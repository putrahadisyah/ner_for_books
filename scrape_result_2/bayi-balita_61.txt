{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/kumcer-islami-pilihan",
	"title": "Kumcer Islami Pilihan",
	"author": "Anisa Widiyarti",
	"Jumlah Halaman": "96",
	"Tanggal Terbit": "1 Sep 2017",
	"ISBN": "9786023663262",
	"Bahasa": "Indonesia",
	"Penerbit": "Tiga Serangkai",
	"Berat": "0.18 kg",
	"Lebar": "19 cm",
	"Panjang": "24 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/disney-princess-snow-white-tiga-raksasa-snow-white-and-the-three-giants",
	"title": "Disney Princess: Snow White & Tiga Raksasa (Snow White And The Three Giants)",
	"author": "Disney",
	"Jumlah Halaman": "24",
	"Tanggal Terbit": "25 Jul 2016",
	"ISBN": "9786020332581",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.1000 kg",
	"Lebar": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/disney-first-colors-shapes-numbers-hc",
	"title": "Disney First Colors Shapes Numbers",
	"author": "Disney Book Group",
	"Jumlah Halaman": "24.0",
	"Tanggal Terbit": "1 Nov 2018",
	"ISBN": "9781368037020",
	"Penerbit": "Disney Press",
	"Berat": "0.45 kg",
	"Lebar": "22.0 cm",
	"Panjang": "23.0 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/uwais-al-qarni-anak-yang-berbakti-pada-ibunya",
	"title": "Uwais Al Qarni Anak Yang Berbakti Pada Ibunya",
	"author": "Wahyu Annisha",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "6 Mei 2019",
	"ISBN": "9786026647214",
	"Bahasa": "Indonesia",
	"Penerbit": "Buku Kita",
	"Berat": "0.15 kg",
	"Lebar": "19 cm",
	"Panjang": "26 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/lingkungan-sosial-seri-pengenalan",
	"title": "Lingkungan Sosial Seri Pengenalan",
	"author": "Kak Kay",
	"Jumlah Halaman": "64",
	"Tanggal Terbit": "15 Apr 2016",
	"ISBN": "9786023820726",
	"Penerbit": "Yrama Widya",
	"Berat": "0.1200 kg",
	"Lebar": "17.5 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/aku-sayang-adik-scb",
	"title": "Aku Sayang Adik : Scb",
	"author": "Eka Wardhana",
	"Jumlah Halaman": "20",
	"Tanggal Terbit": "27 Jul 2015",
	"ISBN": "9786022427544",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Publishing",
	"Berat": "0.4000 kg",
	"Lebar": "18 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/smart-fun-membaca-menulis-berhitung-mewarnai-usia-3-6-tahu",
	"title": "Smart & Fun Membaca Menulis Berhitung Mewarnai Usia 3-6 Tahun",
	"author": "Amazing Kids",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "12 Mar 2019",
	"ISBN": "9786239005719",
	"Bahasa": "Indonesia",
	"Penerbit": "Kata Buku",
	"Berat": "0.29 kg",
	"Lebar": "19 cm",
	"Panjang": "26 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-berhitung-menulis-angka-untuk-paud-tk-tema-buah2an",
	"title": "SERI BERHITUNG & MENULIS ANGKA UNTUK PAUD DAN TK; TEMA BUAH-BUAHAN",
	"author": "Ratna Agustina",
	"Jumlah Halaman": "84",
	"Tanggal Terbit": "22 Nov 2016",
	"ISBN": "9786023912827",
	"Bahasa": "Indonesia",
	"Penerbit": "Diva Press",
	"Berat": "0.3000 kg",
	"Lebar": "20.5 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-anak-hebat-aku-bilang-astagfirullah-board-book",
	"title": "Seri Anak Hebat: Aku Bilang Astagfirullah (Board Book)",
	"author": "Lina Sellin",
	"Jumlah Halaman": "20",
	"Tanggal Terbit": "8 Mei 2018",
	"ISBN": "9786023852901",
	"Bahasa": "Indonesia",
	"Penerbit": "Noura Publishing",
	"Berat": "0.40 kg",
	"Lebar": "18 cm",
	"Panjang": "18 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/little-bebi-bilingual-di-mana-robotku",
	"title": "Little Bebi Bilingual - Di Mana Robotku?",
	"author": "Tim Eldi",
	"Jumlah Halaman": "24",
	"Tanggal Terbit": "10 Des 2016",
	"ISBN": "9786020297309",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.2 kg",
	"Lebar": "13 cm",
	"Panjang": "15 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/islamic-worldview",
	"title": "Islamic Worldview",
	"author": "Dr. Abas Mansur Tamam",
	"Jumlah Halaman": "179",
	"Tanggal Terbit": "17 Nov 2017",
	"ISBN": "9786021988268",
	"Penerbit": "Spirit Media",
	"Berat": "0.2 kg",
	"Lebar": "15 cm",
	"Panjang": "22 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/macan-kumbang-yang-adil",
	"title": "Macan Kumbang yang Adil",
	"author": "Nelfi Syafrina",
	"Jumlah Halaman": "35",
	"Tanggal Terbit": "15 Mei 2018",
	"ISBN": "9786023664443",
	"Bahasa": "Indonesia",
	"Penerbit": "Tiga Serangkai",
	"Berat": "0.10 kg",
	"Lebar": "20 cm",
	"Panjang": "19 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/justice-league-unlimited-coloring-book-wb-cvan36227",
	"title": "Justice League Unlimited, Coloring Book (Wb)-Cvan36227",
	"author": "Warner Bros",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "13 Sep 2016",
	"ISBN": "9789792958676",
	"Bahasa": "Indonesia",
	"Penerbit": "Andi Offset",
	"Berat": "0.5000 kg",
	"Lebar": "28 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/conf-nabi-zakaria-as-nabi-yang-sabar-menanti-keturunan",
	"title": "Nabi Zakaria AS: Nabi Yang Sabar Menanti Keturunan",
	"author": "Yugha E",
	"Jumlah Halaman": "52",
	"Tanggal Terbit": "25 Feb 2016",
	"ISBN": "9786022527220",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Erlangga",
	"Berat": "0.15 kg",
	"Lebar": "21 cm",
	"Panjang": "28 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/priddy-lift-the-flap-farm-hc",
	"title": "Priddy Lift The Flap: Farm",
	"author": "Roger Priddy",
	"Jumlah Halaman": "16",
	"Tanggal Terbit": "20 Feb 2005",
	"ISBN": "9780312528133",
	"Bahasa": "English",
	"Penerbit": "Priddy Books",
	"Berat": "0.14 kg",
	"Lebar": "10 cm",
	"Panjang": "23 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-fun-cican-kring-kring-cican-naik-sepeda-board-book",
	"title": "SERI FUN CICAN: KRING-KRING! CICAN NAIK SEPEDA (BOARD BOOK)",
	"author": "Wahyu Aditya",
	"Jumlah Halaman": "20",
	"Tanggal Terbit": "22 Mei 2018",
	"ISBN": "9786023853960",
	"Bahasa": "Indonesia",
	"Penerbit": "Noura Publishing",
	"Berat": "0.34 kg",
	"Lebar": "18 cm",
	"Panjang": "17 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-fun-cican-waktunya-tidur-cican-boardbook",
	"title": "Seri Fun Cican: Waktunya Tidur, Cican! (BB)",
	"author": "Tria Ayu K",
	"Jumlah Halaman": "20.0",
	"Tanggal Terbit": "28 Nov 2018",
	"ISBN": "9786024304379",
	"Penerbit": "Bentang Pustaka",
	"Berat": "0.38 kg",
	"Lebar": "18.0 cm",
	"Panjang": "18.0 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/disney-puppy-dog-pals-happy-birth-day-puppy-pals-sc",
	"title": "Disney Puppy Dog Pals: Happy Birthday Puppy Pals",
	"author": "Disney Book Group",
	"Jumlah Halaman": "24",
	"Tanggal Terbit": "27 Feb 2018",
	"ISBN": "9781368010863",
	"Bahasa": "English",
	"Penerbit": "Disney Press",
	"Berat": "0.91 kg",
	"Lebar": "19 cm",
	"Panjang": "20 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/dinosaurus-terdahsyat-super-seru-sticker-book",
	"title": "Dinosaurus Terdahsyat : Super Seru!!! Sticker Book",
	"author": "Tim Redaksi",
	"Jumlah Halaman": "27",
	"Tanggal Terbit": "10 Mar 2016",
	"ISBN": "9786023172306",
	"Bahasa": "Indonesia",
	"Penerbit": "Solusi Distribusi",
	"Berat": "0.1000 kg",
	"Lebar": "19 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/why-heredity-type-of-blood",
	"title": "Why? Heredity & Type Of Blood",
	"author": "Yearimdang",
	"Jumlah Halaman": "159",
	"Tanggal Terbit": "31 Mei 2017",
	"ISBN": "9786020002705",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.40 kg",
	"Lebar": "18 cm",
	"Panjang": "24 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/paw-patrol-stories-with-figures-bb",
	"title": "Paw Patrol: Stories With Figures (BB)",
	"author": "Disney",
	"Jumlah Halaman": "7",
	"Tanggal Terbit": "31 Okt 2017",
	"ISBN": "9781772383553",
	"Bahasa": "Indonesia",
	"Penerbit": "Novelty Ca",
	"Berat": "0.88 kg",
	"Lebar": "22.5 cm",
	"Panjang": "30 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-satwa-indonesia-luna-si-lutung-jawa",
	"title": "Seri Satwa Indonesia: Luna Si Lutung Jawa",
	"author": "Rahayu Oktaviani",
	"Jumlah Halaman": "24",
	"Tanggal Terbit": "30 Mei 2019",
	"ISBN": "9786024672607",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Erlangga",
	"Berat": "0.06 kg",
	"Lebar": "20 cm",
	"Panjang": "20 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/jejak-langkah-sang-sufi-jalaluddin-rumi",
	"title": "Jejak Langkah Sang Sufi Jalaluddin Rumi",
	"author": "Chindi Andriyani",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "7 Agt 2017",
	"ISBN": "9786026595799",
	"Penerbit": "Mueeza",
	"Berat": "0.155 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-anak-cerdas-belajar-huruf-membaca",
	"title": "Seri Anak Cerdas: Belajar Huruf & Membaca",
	"author": "Diyah P",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "22 Jan 2019",
	"ISBN": "9786023782413",
	"Bahasa": "Indonesia",
	"Penerbit": "Wahyumedia Wacana",
	"Berat": "0.12 kg",
	"Lebar": "19 cm",
	"Panjang": "26 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/hello-kitty-menggambar-bentuk-bersama-hello-kitty",
	"title": "Hello Kitty : Menggambar Bentuk Bersama Hello Kitty",
	"author": "Tim Efk",
	"Jumlah Halaman": "16",
	"Tanggal Terbit": "1 Des 2016",
	"ISBN": "9786022527985",
	"Bahasa": "Indonesia",
	"Penerbit": "Erlangga For Kids",
	"Berat": "0.1000 kg",
	"Lebar": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/ketua-osis-koplak",
	"title": "Ketua Osis Koplak",
	"author": "Wisnu Maulana",
	"Jumlah Halaman": "352",
	"Tanggal Terbit": "16 Nov 2017",
	"ISBN": "9786026716125",
	"Penerbit": "Mizan Publishing",
	"Berat": "0.3 kg",
	"Lebar": "14 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/sofia-the-first-sofia-tersesat",
	"title": "Sofia The First: Sofia Tersesat",
	"author": "Tim Efk",
	"Jumlah Halaman": "15",
	"Tanggal Terbit": "28 Des 2016",
	"ISBN": "9786022527725",
	"Bahasa": "Indonesia",
	"Penerbit": "Erlangga For Kids",
	"Berat": "0.07 kg",
	"Lebar": "21 cm",
	"Panjang": "28 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/dongeng-princes-princess-plus-doa-sehari-hari-untuk-muslim-cilik",
	"title": "Dongeng Princes & Princess Plus Doa Sehari-Hari Untuk Muslim Cilik",
	"author": "Mulasih Tary",
	"Jumlah Halaman": "152",
	"Tanggal Terbit": "19 Agt 2016",
	"ISBN": "9786026763372",
	"Bahasa": "Indonesia",
	"Penerbit": "Charissa Publisher",
	"Berat": "1.0000 kg",
	"Lebar": "26 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/kartu-peraga-tematik",
	"title": "Kartu Peraga Tematik",
	"author": "Lisa Tiyani",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "5 Apr 2018",
	"ISBN": "9786023781966",
	"Bahasa": "Indonesia",
	"Penerbit": "Wahyu Media",
	"Berat": "0.25 kg",
	"Lebar": "12 cm",
	"Panjang": "17 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/paper-puppet-boboiboy-vol-2",
	"title": "Paper Puppet Boboiboy Vol. 2",
	"author": "Tim Eldi",
	"Jumlah Halaman": "6",
	"Tanggal Terbit": "20 Apr 2016",
	"ISBN": "9786020282602",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/because-of-you",
	"title": "Because of You",
	"author": "Xi Zhi",
	"Jumlah Halaman": "224",
	"Tanggal Terbit": "20 Jul 2017",
	"ISBN": "9786026383020",
	"Penerbit": "Penerbit Haru",
	"Berat": "0.165 kg",
	"Lebar": "13 cm",
	"Panjang": "19 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/first-100-words",
	"title": "First 100 Words",
	"author": "Make Believe Ideas",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "1 Agt 2014",
	"ISBN": "9781783932085",
	"Bahasa": "Indonesia",
	"Penerbit": "Make Believe Ideas",
	"Berat": "0.35 kg",
	"Lebar": "18 cm",
	"Panjang": "18 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/membaca-menulis-berhitung-mewarnai-smart-kids-activities",
	"title": "Membaca Menulis Berhitung Mewarnai : Smart Kids Activities",
	"author": "Lilik Puji Rahayu",
	"Jumlah Halaman": "116",
	"Tanggal Terbit": "13 Apr 2016",
	"ISBN": "9786026991652",
	"Bahasa": "Indonesia",
	"Penerbit": "Genta Group",
	"Berat": "0.2800 kg",
	"Lebar": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-buah-sayur-fun-kids-activity-usia-4-6-tahun",
	"title": "Seri Buah & Sayur : Fun Kids Activity Usia 4-6 Tahun",
	"author": "Kak Caca",
	"Jumlah Halaman": "76",
	"Tanggal Terbit": "1 Mar 2018",
	"ISBN": "9786025480317",
	"Bahasa": "Indonesia",
	"Penerbit": "Solusi Distribusi Buku Cv",
	"Berat": "0.160 kg",
	"Lebar": "18 cm",
	"Panjang": "24 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-cerdas-kreatif-mewarnai-keluarga-profesi",
	"title": "Seri Cerdas Kreatif Mewarnai Keluarga & Profesi",
	"author": "Novi Kurnia",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "12 Jun 2019",
	"ISBN": "9786239043988",
	"Bahasa": "Indonesia",
	"Penerbit": "Sarang Baca",
	"Berat": "0.250 kg",
	"Lebar": "19 cm",
	"Panjang": "23 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/petualangan-lu-si-ulat-bulu",
	"title": "Petualangan Lu Si Ulat Bulu",
	"author": "Norrattri",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "2 Okt 2017",
	"ISBN": "9786024285975",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "20 cm",
	"Panjang": "24 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/balita-hebat-cepat-kuasai-tulis-hitung",
	"title": "Balita Hebat: Cepat Kuasai Baca, Tulis, dan Hitung",
	"author": "Kak Rini",
	"Jumlah Halaman": "80",
	"Tanggal Terbit": "9 Nov 2018",
	"ISBN": "9786024074111",
	"Bahasa": "Indonesia",
	"Penerbit": "Laksana",
	"Berat": "0.31 kg",
	"Lebar": "20 cm",
	"Panjang": "27 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-anak-hebat-aku-mau-makan-sendiri-board-book",
	"title": "Seri Anak Hebat: Aku Mau Makan Sendiri (Board Book)",
	"author": "Richanadia",
	"Jumlah Halaman": "20",
	"Tanggal Terbit": "30 Okt 2017",
	"ISBN": "9786023853038",
	"Bahasa": "Indonesia",
	"Penerbit": "Noura Publishing",
	"Berat": "0.35 kg",
	"Lebar": "17.5 cm",
	"Panjang": "17.5 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-dongeng-animasi-3d-kisah-ajaib-tuan-scrooge",
	"title": "Seri Dongeng Animasi 3D : Kisah Ajaib Tuan Scrooge",
	"author": "Charles Dickens/kyowon",
	"Jumlah Halaman": "56",
	"Tanggal Terbit": "13 Des 2016",
	"ISBN": "9786023943838",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.1500 kg",
	"Lebar": "20 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/manis-legit-dodol-garut",
	"title": "Manis Legit Dodol Garut",
	"author": "FiFadila",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "26 Mar 2019",
	"ISBN": "9786232060241",
	"Bahasa": "Indonesia",
	"Penerbit": "Tiga Serangkai",
	"Berat": "0.100 kg",
	"Lebar": "20 cm",
	"Panjang": "19 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/ibnu-rusyd-tokoh-filsafat-islam",
	"title": "Ibnu Rusyd Tokoh Filsafat Islam",
	"author": "Yoli Hemdi",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "1 Jan 2019",
	"ISBN": "9786022684497",
	"Bahasa": "Indonesia",
	"Penerbit": "Luxima Metro Media",
	"Berat": "0.155 kg"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-dongeng-animasi-3d-gadis-angsa",
	"title": "Seri Dongeng Animasi 3D : Gadis Angsa",
	"author": "Kyowon",
	"Jumlah Halaman": "156",
	"Tanggal Terbit": "28 Nov 2016",
	"ISBN": "9786023943401",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.3200 kg",
	"Lebar": "20 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/disney-moana-discoveries-and-dreams-sc",
	"title": "Disney Moana: Discoveries And Dreams",
	"author": "Parragon Books Ltd",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "2 Okt 2017",
	"ISBN": "9781474852739",
	"Bahasa": "English",
	"Penerbit": "Sinar Star Book",
	"Berat": "0.40 kg",
	"Lebar": "20 cm",
	"Panjang": "28 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-yuk-mewarnai-bunga-indonesia",
	"title": "Seri Yuk Mewarnai Bunga Indonesia",
	"author": "Tim Kreatif Rainbow",
	"Jumlah Halaman": "30",
	"Tanggal Terbit": "28 Des 2018",
	"ISBN": "9789792970777",
	"Bahasa": "Indonesia",
	"Penerbit": "Andi Publisher",
	"Berat": "0.130 kg",
	"Lebar": "21 cm",
	"Panjang": "30 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/twinkle-twinkle-unicorn-bb",
	"title": "Twinkle Twinkle Unicorn (Boardbook)",
	"author": "Jeffrey Burton",
	"Jumlah Halaman": "16",
	"Tanggal Terbit": "20 Mar 2019",
	"ISBN": "9781534439733",
	"Bahasa": "English",
	"Penerbit": "Simon & Schuster",
	"Berat": "0.25 kg",
	"Lebar": "14 cm",
	"Panjang": "14 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/dora-the-explorer-sehari-bersama-dora",
	"title": "Dora The Explorer : Sehari Bersama Dora",
	"author": "Eric Weiner",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "14 Nov 2016",
	"ISBN": "9786022528036",
	"Bahasa": "Indonesia",
	"Penerbit": "Erlangga",
	"Berat": "0.1000 kg",
	"Lebar": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/super-activity-book-robot",
	"title": "Super Activity Book: Robot",
	"author": "Kak Dony",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "21 Apr 2017",
	"ISBN": "9786021576847",
	"Bahasa": "Indonesia",
	"Penerbit": "Ruang Kata",
	"Berat": "0.103 kg",
	"Lebar": "20 cm",
	"Panjang": "20 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-anak-hebat-aku-senang-membaca-board-book",
	"title": "Seri Anak Hebat: Aku Senang Membaca (Board Book)",
	"author": "Dian Rusdi",
	"Jumlah Halaman": "20",
	"Tanggal Terbit": "20 Des 2017",
	"ISBN": "9786023853618",
	"Bahasa": "Indonesia",
	"Penerbit": "Noura Publishing",
	"Berat": "0.370 kg",
	"Lebar": "17.5 cm",
	"Panjang": "17.5 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/yuk-belajar-menulis-huruf-tegak-bersambung",
	"title": "Yuk, Belajar Menulis Huruf Tegak Bersambung",
	"author": "Sri Nur Hidayati",
	"Jumlah Halaman": "52",
	"Tanggal Terbit": "12 Sep 2017",
	"ISBN": "9789797753023",
	"Bahasa": "Indonesia",
	"Penerbit": "Indonesia Tera",
	"Berat": "0.17 kg",
	"Lebar": "19 cm",
	"Panjang": "26 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/mewarnai-hewan-laut",
	"title": "Mewarnai Hewan Laut",
	"author": "Iqbal Sudirja",
	"Jumlah Halaman": "40",
	"Tanggal Terbit": "21 Jun 2017",
	"ISBN": "9786023880430",
	"Bahasa": "Indonesia",
	"Penerbit": "Happy Holy Kids",
	"Berat": "0.1250 kg",
	"Lebar": "19 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/push-pull-slide-busy-town-bb",
	"title": "Push Pull Slide: Busy Town (Bb)",
	"author": "Rebecca Finn",
	"Jumlah Halaman": "20.0",
	"Tanggal Terbit": "23 Des 2019",
	"ISBN": "9781447257615",
	"Bahasa": "English",
	"Penerbit": "Sinar Star Book",
	"Berat": "0.352 kg",
	"Lebar": "18.0 cm",
	"Panjang": "18.0 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/aku-sayang-teman-temanku",
	"title": "Aku Sayang Teman-Temanku",
	"author": "Stella Ernes",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "3 Jul 2017",
	"ISBN": "9789792151718",
	"Bahasa": "Indonesia",
	"Penerbit": "Pt Kanisius",
	"Berat": "0.11 kg",
	"Lebar": "21 cm",
	"Panjang": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/seri-dongeng-animasi-3d-si-pohon-cemara",
	"title": "Seri Dongeng Animasi 3D : Si Pohon Cemara",
	"author": "Kyowon",
	"Jumlah Halaman": "56",
	"Tanggal Terbit": "28 Agt 2017",
	"ISBN": "9786023949076",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.1500 kg",
	"Lebar": "20 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/tan-memancing",
	"title": "Tan: Memancing",
	"author": "Lia Loeferns",
	"Jumlah Halaman": "23",
	"Tanggal Terbit": "4 Des 2017",
	"ISBN": "9786020379609",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.10 kg",
	"Lebar": "21 cm",
	"Panjang": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/hello-kitty-mencari-jalan",
	"title": "Hello Kitty : Mencari Jalan",
	"author": "Tim Efk",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "11 Jul 2016",
	"ISBN": "9786022527633",
	"Bahasa": "Indonesia",
	"Penerbit": "Erlangga For Kids",
	"Berat": "0.79 kg",
	"Lebar": "21 cm",
	"Panjang": "28 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/siap-dan-mandiri-masuk-sd-cars",
	"title": "Siap Dan Mandiri Masuk Sd Cars",
	"author": "Disney",
	"Jumlah Halaman": "64",
	"Tanggal Terbit": "11 Mei 2016",
	"ISBN": "9786020285283",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "21 cm"
},
{
	"page": 61,
	"kategori 1": "buku-anak",
	"kategori 2": "bayi-balita",
	"book_url": "https://www.gramedia.com/products/yuk-mengenal-20-binatang-predator-yang-unik-menakjubkan-sstd",
	"title": "Yuk Mengenal 20 Binatang Predator Yang Unik & Menakjubkan: Sstd",
	"author": "Jenar",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "22 Mar 2016",
	"ISBN": "9786023002191",
	"Penerbit": "Araska",
	"Berat": "0.1000 kg",
	"Lebar": "20 cm"
},
