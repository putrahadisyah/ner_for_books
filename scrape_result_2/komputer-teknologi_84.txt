{
	"page": 84,
	"kategori 1": "non-fiksi",
	"kategori 2": "komputer-teknologi",
	"book_url": "https://www.gramedia.com/products/passive-income-dari-youtube",
	"title": "Passive Income dari YouTube",
	"author": "Jefferly Helianthusonfri",
	"Jumlah Halaman": "224",
	"Tanggal Terbit": "5 Agt 2019",
	"ISBN": "9786230003356",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.270 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 84,
	"kategori 1": "non-fiksi",
	"kategori 2": "komputer-teknologi",
	"book_url": "https://www.gramedia.com/products/panduan-lengkap-mengelola-data-excel-2016",
	"title": "Panduan Lengkap Mengelola Data Excel 2016",
	"author": "Yudhy Wicaksono",
	"Jumlah Halaman": "384",
	"Tanggal Terbit": "22 Apr 2019",
	"ISBN": "9786020497358",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.460 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 84,
	"kategori 1": "non-fiksi",
	"kategori 2": "komputer-teknologi",
	"book_url": "https://www.gramedia.com/products/panduan-praktis-microsoft-windows-server-2012",
	"title": "Panduan Praktis Microsoft Windows Server 2012",
	"author": "Andik Susilo",
},
{
	"page": 84,
	"kategori 1": "non-fiksi",
	"kategori 2": "komputer-teknologi",
	"book_url": "https://www.gramedia.com/products/autocad-2013-untuk-pemula",
	"title": "Autocad 2013 Untuk Pemula",
	"author": "Jasmadi",
},
{
	"page": 84,
	"kategori 1": "non-fiksi",
	"kategori 2": "komputer-teknologi",
	"book_url": "https://www.gramedia.com/products/mahir-otodidak-microsoft-excel-2007",
	"title": "Mahir Otodidak Microsoft Excel 2007",
	"author": "Hamdan Lugina Jaya",
},
{
	"page": 84,
	"kategori 1": "non-fiksi",
	"kategori 2": "komputer-teknologi",
	"book_url": "https://www.gramedia.com/products/panduan-praktis-ngeblog-di-kompasiana",
	"title": "Panduan Praktis Ngeblog Di Kompasiana",
	"author": "Jubilee Enterprise",
},
{
	"page": 84,
	"kategori 1": "non-fiksi",
	"kategori 2": "komputer-teknologi",
	"book_url": "https://www.gramedia.com/products/solusi-tepat-menjadi-pakar-adobe-dreamweaver-cs6",
	"title": "Solusi Tepat Menjadi Pakar Adobe Dreamweaver CS6",
	"author": "Ruko Mandar",
	"Tanggal Terbit": "2 Mei 2017",
	"ISBN": "9786020420967",
	"Penerbit": "Elex Media Komputindo"
},
