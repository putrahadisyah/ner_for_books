{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/kumpulan-cerita-terbaik-miiko-1",
	"title": "Kumpulan Cerita Terbaik Miiko! 1",
	"author": "Ono Eriko",
	"Jumlah Halaman": "182",
	"Tanggal Terbit": "26 Agt 2009",
	"ISBN": "9789792335149",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.10 kg",
	"Lebar": "11 cm",
	"Panjang": "18 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/king-of-idol-2",
	"title": "King of Idol 2",
	"author": "Wakaki Tamiki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "26 Des 2018",
	"ISBN": "9786020487816",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.096 kg",
	"Lebar": "17.2 cm",
	"Panjang": "11.4 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/naruto-16",
	"title": "Naruto 16",
	"author": "Kishimoto, Masashi",
	"Jumlah Halaman": "169",
	"Tanggal Terbit": "24 Jan 2017",
	"ISBN": "9789792074192",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.10 kg",
	"Lebar": "11 cm",
	"Panjang": "17 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/prince-hero-04",
	"title": "Prince & Hero 04",
	"author": "Daisy Yamada",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "12 Des 2018",
	"ISBN": "9786020486086",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.082 kg",
	"Lebar": "17.2 cm",
	"Panjang": "11.4 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/ten-ten-mimpi-manis",
	"title": "Ten Ten: Mimpi Manis",
	"author": "Glsongi (via Carrot Korean Agency)",
	"Jumlah Halaman": "193",
	"Tanggal Terbit": "3 Sep 2018",
	"ISBN": "9786020477787",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.3 kg",
	"Lebar": "15 cm",
	"Panjang": "22 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/komik-doa-anak-muslim",
	"title": "Komik Doa Anak Muslim",
	"author": "Dian K & Aan W",
	"Jumlah Halaman": "168.0",
	"Tanggal Terbit": "29 Apr 2019",
	"ISBN": "9786232161566",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.25 kg",
	"Lebar": "18.0 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/ekstrak-komik-si-abi",
	"title": "Ekstrak Komik Si Abi",
	"author": "Abidin Ma`Aruf",
	"Jumlah Halaman": "120",
	"Tanggal Terbit": "16 Nov 2017",
	"ISBN": "9786021951392",
	"Bahasa": "Indonesia",
	"Penerbit": "Visi Mandiri",
	"Berat": "0.105 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/orang-orangan-sawah",
	"title": "Orang Orangan Sawah",
	"author": "Rendra M. Ridwan",
	"Jumlah Halaman": "96",
	"Tanggal Terbit": "22 Jul 2017",
	"ISBN": "9786023673933",
	"Penerbit": "Muffin Graphics",
	"Berat": "0.85 kg",
	"Lebar": "14.5 cm",
	"Panjang": "21 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/may-as-well-die",
	"title": "May As Well Die",
	"author": "Gold Kiwi Bird",
	"Jumlah Halaman": "324",
	"Tanggal Terbit": "7 Nov 2018",
	"ISBN": "9786025254765",
	"Bahasa": "Indonesia",
	"Penerbit": "Haru",
	"Berat": "0.30 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/komik-kkpk-pendongeng-cilik",
	"title": "Komik Kkpk Pendongeng Cilik",
	"author": "Najla Nurdiany Putri",
	"Jumlah Halaman": "88",
	"Tanggal Terbit": "7 Mei 2019",
	"ISBN": "9786024207953",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Media Utama",
	"Berat": "0.1 kg",
	"Lebar": "15 cm",
	"Panjang": "21 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-police-girl-maruka-5",
	"title": "LC: Police Girl Maruka 5",
	"author": "Takashi Nagasaki & Seimu Yoshizaki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "5 Des 2018",
	"ISBN": "9786020485614",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.15 kg",
	"Lebar": "18 cm",
	"Panjang": "13 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/ultimate-comix-4",
	"title": "Ultimate Comix 4",
	"author": "-",
	"Jumlah Halaman": "272",
	"Tanggal Terbit": "11 Jul 2017",
	"ISBN": "9789792372748",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbitan Sarana Bobo",
	"Berat": "0.45 kg",
	"Lebar": "17 cm",
	"Panjang": "24 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lucky-luke-kereta-kuda-lc",
	"title": "Lc: Lucky Luke - Kereta Kuda",
	"author": "Morris, Goscinny",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "13 Apr 2016",
	"ISBN": "9786020283890",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1500 kg",
	"Lebar": "18 cm",
	"Panjang": "24 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/ace-of-diamond-21",
	"title": "Ace of Diamond 21",
	"author": "Yuji Terajima",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "8 Nov 2017",
	"ISBN": "9786020447582",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.13 kg",
	"Lebar": "11.2 cm",
	"Panjang": "17.4 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/the-real-winner-vol-10",
	"title": "The Real Winner Vol. 10",
	"author": "Putri",
	"Jumlah Halaman": "88",
	"Tanggal Terbit": "14 Jul 2017",
	"ISBN": "9786024204129",
	"Penerbit": "Dar Mizan",
	"Berat": "0.15 kg",
	"Lebar": "14.5 cm",
	"Panjang": "21 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/debora-perempuan-pemberani",
	"title": "Debora Perempuan Pemberani",
	"author": "Ervina",
	"Jumlah Halaman": "78",
	"Tanggal Terbit": "20 Des 2018",
	"ISBN": "9786025067730",
	"Bahasa": "Indonesia",
	"Penerbit": "Yayasan Lentera Kasih Agape",
	"Berat": "0.135 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/love-blue-between-twin-glasses-02",
	"title": "LOVE BLUE Between Twin & Glasses 02",
	"author": "Daisy Yamada",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "30 Jan 2019",
	"ISBN": "9786020488912",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1 kg",
	"Lebar": "17.2 cm",
	"Panjang": "11.4 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/seri-detektif-clifton-04-alias-lord-x",
	"title": "Seri Detektif Clifton 04: Alias Lord X",
	"author": "Turk De Groot",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "25 Feb 2019",
	"ISBN": "9786024835729",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.1 kg",
	"Lebar": "15.5 cm",
	"Panjang": "22 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/komik-next-g-enggak-usah-buru-buru",
	"title": "Komik Next G Enggak Usah Buru-Buru",
	"author": "Griselda Ayrin Dianny Bachtiar, Dkk",
	"Jumlah Halaman": "100",
	"Tanggal Terbit": "1 Agt 2019",
	"ISBN": "9786023677320",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Media Utama",
	"Berat": "0.11 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/puasa-itu-ibadah",
	"title": "Puasa Itu Ibadah",
	"author": "Miki Nomala",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "4 Jun 2018",
	"ISBN": "9786020461601",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.25 kg",
	"Lebar": "15 cm",
	"Panjang": "22 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/boy-and-girl-1",
	"title": "ILL Boy & ILL Girl 01",
	"author": "AKATSUKI AKIRA & NISIOISIN",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "22 Mei 2019",
	"ISBN": "9786020496665",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Kompetindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/ace-of-diamond-24",
	"title": "Ace Of Diamond 24",
	"author": "Yuji Terajima",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "24 Jan 2018",
	"ISBN": "9786020453101",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/first-love-double-edge-07",
	"title": "First Love Double Edge 07",
	"author": "Mizue Odawara",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "15 Jul 2019",
	"ISBN": "9786024805531",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.085 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/night-patrol-teacher-05",
	"title": "Night Patrol Teacher 05",
	"author": "Seiki Tsuchida",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "8 Mei 2019",
	"ISBN": "9786024804671",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.118 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/komik-next-g-my-friends-is-the-best",
	"title": "Komik Next G My Friends Is The Best",
	"author": "Faisal Dkk",
	"Jumlah Halaman": "95",
	"Tanggal Terbit": "4 Mei 2018",
	"ISBN": "9786023675173",
	"Bahasa": "Indonesia",
	"Penerbit": "Muffin Graphics",
	"Berat": "0.10 kg",
	"Lebar": "15 cm",
	"Panjang": "21 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/sofia-fashion-detective-4",
	"title": "Sofia Fashion Investigations 4",
	"author": "Bonni Rambatan, Aisah Puspa Lestari",
	"Jumlah Halaman": "136",
	"Tanggal Terbit": "16 Jan 2019",
	"ISBN": "9786020488653",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/inuyasha-premium-29",
	"title": "Inuyasha Premium 29",
	"author": "Takahashi Rumiko",
	"Jumlah Halaman": "360",
	"Tanggal Terbit": "24 Jul 2019",
	"ISBN": "9786230002700",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-s-the-last-policeman-17",
	"title": "LC : S - The Last Policeman 17",
	"author": "Komori Yoichi,toudou Yutaka",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "11 Okt 2017",
	"ISBN": "9786020439495",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "13 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/say-hello-to-black-jack-03",
	"title": "Say Hello to Black Jack 03",
	"author": "Syuho Sato",
	"Jumlah Halaman": "216",
	"Tanggal Terbit": "16 Agt 2017",
	"ISBN": "9786024283018",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1300 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/gal-sensation",
	"title": "Gal Sensation",
	"author": "Akira Wao",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "3 Okt 2018",
	"ISBN": "9786024800932",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.11 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/okitenemuru-the-awaken-07",
	"title": "Okitenemuru - The Awaken 07",
	"author": "Hitori Renda",
	"Jumlah Halaman": "176.0",
	"Tanggal Terbit": "18 Des 2019",
	"ISBN": "9786024808372",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.093 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/a-chef-of-nobunaga-12",
	"title": "A Chef of Nobunaga 12",
	"author": "Takurou Kajikawa / Mitsuru Nishimura",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "9 Agt 2017",
	"ISBN": "9786024284671",
	"Penerbit": "m&c!",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "16.8 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-joker-the-god-01",
	"title": "LC: Joker, The God 01",
	"author": "Michiharu Kusunoki , Mizu Sahara",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "16 Mei 2018",
	"ISBN": "9786020457994",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.12 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/my-angelic-devilish-boyfriend",
	"title": "My Angelic, Devilish Boyfriend",
	"author": "Chihiro Komori",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "31 Jul 2019",
	"ISBN": "9786024806408",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.093 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/flowering-heart-advertising-account-executive",
	"title": "Flowering Heart : Advertising Account Executive",
	"author": "Iconix / Jang Yeseo",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "26 Feb 2018",
	"ISBN": "9786024286477",
	"Penerbit": "m&c!",
	"Berat": "0.4850 kg",
	"Lebar": "15 cm",
	"Panjang": "22 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-to-the-abandoned-sacred-beast-5",
	"title": "LC: To The Abandoned Sacred Beast 5",
	"author": "Maybe",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "30 Jan 2019",
	"ISBN": "9786020489292",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1 kg",
	"Lebar": "12 cm",
	"Panjang": "18 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/sdmi-kl3-4-5-6-buku-pintar-sks-ips-super-lengkap",
	"title": "Sd/Mi Kl.3 4 5 6 Buku Pintar Sks Ips Super Lengkap",
	"author": "T. Judani Astuti, Dkk",
	"Jumlah Halaman": "444.0",
	"Tanggal Terbit": "19 Agt 2019",
	"ISBN": "9786239087173",
	"Bahasa": "Indonesia",
	"Penerbit": "Oxygen",
	"Berat": "0.345 kg",
	"Lebar": "14.0 cm",
	"Panjang": "20.0 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/no-exit-13",
	"title": "No Exit 13",
	"author": "Haruhi Seta",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "31 Jul 2019",
	"ISBN": "9786230002670",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.093 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/ghost-school-days-rock-star-wannabe",
	"title": "Ghost School Days: Rock Star Wannabe",
	"author": "Assyifa S. Arum",
	"Jumlah Halaman": "83",
	"Tanggal Terbit": "6 Apr 2017",
	"ISBN": "9786023673230",
	"Bahasa": "Indonesia",
	"Penerbit": "Muffin Graphics",
	"Berat": "0.10 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/please-be-mine",
	"title": "Please Be Mine",
	"author": "Eugenia Rakhma",
	"Jumlah Halaman": "182",
	"Tanggal Terbit": "9 Apr 2018",
	"ISBN": "9786024554668",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.150 kg",
	"Lebar": "13 cm",
	"Panjang": "19 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/raise-the-demon-05",
	"title": "Raise The Demon 05",
	"author": "Keito Yoshikawa",
	"Jumlah Halaman": "216.0",
	"Tanggal Terbit": "6 Nov 2019",
	"ISBN": "9786230009020",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.12 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/yowamushi-pedal-go-04",
	"title": "Yowamushi Pedal, Go! 04",
	"author": "Wataru Watanabe",
	"Jumlah Halaman": "392",
	"Tanggal Terbit": "22 Nov 2017",
	"ISBN": "9786024286057",
	"Penerbit": "m&c!",
	"Berat": "0.200 kg",
	"Lebar": "11.2 cm",
	"Panjang": "17.6 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/rahasia-anak-sd-first-kiss",
	"title": "Rahasia Anak SD - First Kiss",
	"author": "Mikayo Nakae",
	"Jumlah Halaman": "160",
	"Tanggal Terbit": "26 Jun 2019",
	"ISBN": "9786230000171",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.118 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/penaklukan-muslim-di-mata-bangsa-taklukan",
	"title": "Penaklukan Muslim Di Mata Bangsa Taklukan",
	"author": "HUSSAM `ITANI",
	"Jumlah Halaman": "336.0",
	"Tanggal Terbit": "8 Okt 2019",
	"ISBN": "9786232200678",
	"Bahasa": "Indonesia",
	"Penerbit": "Alvabet",
	"Berat": "0.36 kg",
	"Lebar": "15.0 cm",
	"Panjang": "23.0 cm"
},
{
	"page": 68,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-giant-killing-47",
	"title": "LC: Giant Killing 47",
	"author": "Tsujitomo, Masaya Tsunamoto",
	"Jumlah Halaman": "232",
	"Tanggal Terbit": "13 Mar 2019",
	"ISBN": "9786020491288",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.150 kg",
	"Lebar": "18 cm",
	"Panjang": "13 cm"
},
