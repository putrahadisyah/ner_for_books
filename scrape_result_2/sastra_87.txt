{
	"page": 87,
	"kategori 1": "fiksi-sastra",
	"kategori 2": "sastra",
	"book_url": "https://www.gramedia.com/products/resonansi-tiga-hati",
	"title": "Resonansi Tiga Hati",
	"author": "Yeni Fatmawati, Lies Wijayanti, Donny",
	"Tanggal Terbit": "21 Apr 2016",
	"ISBN": "9786023754021",
	"Penerbit": "Gramedia Widiasarana Indonesia"
},
{
	"page": 87,
	"kategori 1": "fiksi-sastra",
	"kategori 2": "sastra",
	"book_url": "https://www.gramedia.com/products/inspirasi-cinta",
	"title": "Inspirasi Cinta",
	"author": "Kinoysan",
	"Tanggal Terbit": "27 Jul 2015",
	"ISBN": "9786023750467",
	"Penerbit": "Gramedia Widiasarana Indonesia"
},
{
	"page": 87,
	"kategori 1": "fiksi-sastra",
	"kategori 2": "sastra",
	"book_url": "https://www.gramedia.com/products/teenlit-touche-1-touche",
	"title": "TeenLit: Touche#1: Touche",
	"author": "Windhy Puspitadewi",
	"Tanggal Terbit": "30 Mei 2017",
	"ISBN": "9786020303635",
	"Penerbit": "Gramedia Pustaka Utama"
},
