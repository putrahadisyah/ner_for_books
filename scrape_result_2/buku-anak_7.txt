{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/my-little-pony-friends-forever-3",
	"title": "My Little Pony: Friends Forever 3",
	"author": "Hasbro",
	"Jumlah Halaman": "76",
	"Tanggal Terbit": "20 Nov 2017",
	"ISBN": "9786020377346",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.2 kg",
	"Lebar": "20 cm",
	"Panjang": "29 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/lc-parasyte-10",
	"title": "LC: Parasyte 10",
	"author": "Hitoshi Iwaaki",
	"Jumlah Halaman": "232",
	"Tanggal Terbit": "19 Apr 2017",
	"ISBN": "9786020413259",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1350 kg",
	"Lebar": "12 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/winnie-the-pooh-a-boy-a-bear-a-ballon-bocah-beruang-balon",
	"title": "Winnie The Pooh : A Boy, A Bear, A Ballon (Bocah, Beruang, Balon)",
	"author": "Disney",
	"Jumlah Halaman": "40",
	"Tanggal Terbit": "30 Jul 2018",
	"ISBN": "9786020395159",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.15 kg",
	"Lebar": "21 cm",
	"Panjang": "28 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/seri-i-love-big-cats-1",
	"title": "Seri I Love : Big Cats",
	"author": "Steve Parker",
	"Jumlah Halaman": "24.0",
	"Tanggal Terbit": "26 Agt 2019",
	"ISBN": "9786232164222",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.25 kg",
	"Lebar": "24.0 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/si-cacing",
	"title": "Si Cacing",
	"author": "Ajahn Brahm",
	"Jumlah Halaman": "312",
	"Tanggal Terbit": "18 Mar 2017",
	"ISBN": "9786028194310",
	"Bahasa": "Indonesia",
	"Penerbit": "Awareness Publication",
	"Berat": "0.37 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/lima-sekawan-nyaris-terjebak",
	"title": "Lima Sekawan: Nyaris Terjebak (Cetak Ulang 2018)",
	"author": "Enid Blyton",
	"Jumlah Halaman": "248",
	"Tanggal Terbit": "22 Jan 2018",
	"ISBN": "9786020321318",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.120 kg",
	"Lebar": "11 cm",
	"Panjang": "18 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/conan-movie-the-darkest-nightmare-last",
	"title": "Conan Movie : The Darkest Nightmare last",
	"author": "Aoyama Gosho",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "30 Mei 2018",
	"ISBN": "9786020457918",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1 kg",
	"Lebar": "12 cm",
	"Panjang": "18 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/ensiklopedia-dunia-satwa-dinosaurus",
	"title": "Ensiklopedia Dunia Satwa: Dinosaurus",
	"author": "John Woodward",
	"Jumlah Halaman": "80",
	"Tanggal Terbit": "4 Okt 2015",
	"ISBN": "9786022499039",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.24 kg",
	"Lebar": "22 cm",
	"Panjang": "28 cm"
},
{
	"page": 7,
	"kategori 1": "buku-anak",
	"book_url": "https://www.gramedia.com/products/petualangan-tintin-laut-merah",
	"title": "Petualangan Tintin: Laut Merah",
	"author": "Herge",
	"Jumlah Halaman": "62",
	"Tanggal Terbit": "9 Nov 2015",
	"ISBN": "9786020316840",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.2500 kg",
	"Lebar": "21.5 cm"
},
