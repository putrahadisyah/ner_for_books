{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/yes-success-is-my-right-tips-trik-cespleng-100-hari-men",
	"title": "Yes!! Success Is My Right!!! Tips Trik Cespleng 100 Hari Men",
	"author": "AYU WENING",
	"Jumlah Halaman": "248",
	"Tanggal Terbit": "29 Nov 2018",
	"ISBN": "9786025945069",
	"Bahasa": "Indonesia",
	"Penerbit": "Scritto Books",
	"Berat": "0.250 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/pernah-singgah",
	"title": "Pernah Singgah",
	"author": "Khasan Ashari",
	"Jumlah Halaman": "280.0",
	"Tanggal Terbit": "14 Okt 2019",
	"ISBN": "9786230007552",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.38 kg",
	"Lebar": "14.0 cm",
	"Panjang": "21.0 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/dear-millennials",
	"title": "Dear Millennials",
	"author": "Sudirman Said",
	"Jumlah Halaman": "146",
	"Tanggal Terbit": "20 Mar 2018",
	"ISBN": "9786023854226",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Publishing",
	"Berat": "0.20 kg",
	"Lebar": "12 cm",
	"Panjang": "19 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/rahasia-manusia-bagian-dari-tuhan",
	"title": "Rahasia Manusia Bagian Dari Tuhan",
	"author": "Zufri. AR",
	"Jumlah Halaman": "358.0",
	"Tanggal Terbit": "20 Nov 2019",
	"ISBN": "9789791836036",
	"Bahasa": "Indonesia",
	"Penerbit": "CIPROMEDIA",
	"Berat": "0.47 kg",
	"Lebar": "15.0 cm",
	"Panjang": "23.0 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/senja-adalah-jeda",
	"title": "Senja Adalah Jeda",
	"author": "FADILA HANUM, IRRA FACHRIYANTHI DKK",
	"Jumlah Halaman": "200.0",
	"Tanggal Terbit": "14 Okt 2019",
	"ISBN": "9786025701009",
	"Bahasa": "Indonesia",
	"Penerbit": "Indiva Media Kreasi",
	"Berat": "0.153 kg",
	"Lebar": "13.0 cm",
	"Panjang": "19.0 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/jump",
	"title": "Jump!",
	"author": "Augie Fantinus",
	"Jumlah Halaman": "256",
	"Tanggal Terbit": "1 Agt 2017",
	"ISBN": "9786024260576",
	"Penerbit": "Bentang Pustaka",
	"Berat": "0.21 kg",
	"Lebar": "13 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/conf-go-for-gold",
	"title": "Go For Gold",
	"author": "John C. Maxwell",
	"Jumlah Halaman": "378",
	"Tanggal Terbit": "14 Sep 2015",
	"ISBN": "9786020956053",
	"Bahasa": "Indonesia",
	"Penerbit": "Menuju Insan Cemerlang",
	"Berat": "0.4500 kg",
	"Lebar": "13 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/sukses-mulia-is-my-life-solusi-nyata-mewujudkan",
	"title": "Sukses Mulia is My Life : Solusi Nyata Mewujudkan",
	"author": "33 Trainer Tempa",
	"Jumlah Halaman": "584",
	"Tanggal Terbit": "29 Jun 2015",
	"ISBN": "9786023420278",
	"Bahasa": "Indonesia",
	"Penerbit": "Zikrul Hakim",
	"Berat": "0.60 kg",
	"Lebar": "15 cm",
	"Panjang": "23 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/money-master-the-game-platinum-edition",
	"title": "Money Master The Game Platinum Edition",
	"author": "Anthony Robbins",
	"Jumlah Halaman": "830",
	"Tanggal Terbit": "19 Mar 2019",
	"ISBN": "9786025406959",
	"Bahasa": "Indonesia",
	"Penerbit": "Phoenix",
	"Berat": "0.876 kg",
	"Lebar": "15 cm",
	"Panjang": "24 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/cakap-berbicara-ubah-gaya-bicaramu-layaknya-seorang-intelek",
	"title": "Cakap Berbicara: Ubah Gaya Bicaramu Layaknya Seorang Intelek",
	"author": "Wendy Adelia",
	"Jumlah Halaman": "346.0",
	"Tanggal Terbit": "30 Des 2019",
	"ISBN": "9786237324997",
	"Bahasa": "Indonesia",
	"Penerbit": "Psikologi Corner",
	"Berat": "0.272 kg",
	"Lebar": "14.0 cm",
	"Panjang": "20.0 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/the-power-of-berpikir-positif-selalu-ada-hikmah-dalam-setia",
	"title": "The Power Of Berpikir Positif: Selalu Ada Hikmah Dalam Setia",
	"author": "NUFI WIBISANA",
	"Jumlah Halaman": "200.0",
	"Tanggal Terbit": "31 Agt 2019",
	"ISBN": "9786237210849",
	"Penerbit": "Psikologi Corner",
	"Berat": "0.2 kg",
	"Lebar": "14.0 cm",
	"Panjang": "20.0 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/kapan-kamu-nikah",
	"title": "Kapan Kamu Nikah?",
	"author": "Sophia Mega",
	"Jumlah Halaman": "196",
	"Tanggal Terbit": "1 Des 2017",
	"ISBN": "9786026328403",
	"Bahasa": "Indonesia",
	"Penerbit": "Tiga Serangkai",
	"Berat": "0.261 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/dream-big-make-an-impact",
	"title": "Dream Big Make An Impact",
	"author": "Nadia Permatasari Wijaya, Oda Sekar Ayu, Rizkya Dian Maharani",
	"Jumlah Halaman": "174",
	"Tanggal Terbit": "23 Nov 2017",
	"ISBN": "9786024123079",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Buku Kompas",
	"Berat": "0.2 kg",
	"Lebar": "13 cm",
	"Panjang": "21 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/ayat-ayat-motivasi-penggugah-hati",
	"title": "Ayat-Ayat Motivasi Penggugah Hati",
	"author": "Mohammad A Syuropati",
	"Jumlah Halaman": "240",
	"Tanggal Terbit": "13 Jun 2016",
	"ISBN": "9786021038314",
	"Bahasa": "Indonesia",
	"Penerbit": "Kauna Pustaka",
	"Berat": "1.0000 kg",
	"Lebar": "14 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/tajir-melintir",
	"title": "Tajir Melintir",
	"author": "Mardigu Wp",
	"Jumlah Halaman": "250",
	"Tanggal Terbit": "10 Okt 2017",
	"ISBN": "9786021036655",
	"Bahasa": "Indonesia",
	"Penerbit": "Transmedia",
	"Berat": "0.25 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/99u-series-make-your-mark-panduan-kreati-membangun",
	"title": "99U Series : Make Your Mark Panduan Kreatif Membangun Bisnis Yang Berpengaruh",
	"author": "Jocelyn K. Glei",
	"Jumlah Halaman": "260",
	"Tanggal Terbit": "16 Nov 2016",
	"ISBN": "9786023850778",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1900 kg",
	"Lebar": "18 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/7-langkah-dahsyat-menjadi-pembelajar-hebat",
	"title": "7 Langkah Dahsyat Menjadi Pembelajar Hebat",
	"author": "Tumpak Pia Silitonga",
	"Jumlah Halaman": "272",
	"Tanggal Terbit": "12 Jun 2017",
	"ISBN": "9786020338187",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.3500 kg",
	"Lebar": "14 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/kuliah-kerja-kaya",
	"title": "Kuliah, Kerja, Kaya!",
	"author": "Radis Bastian",
	"Jumlah Halaman": "212.0",
	"Tanggal Terbit": "20 Mei 2019",
	"ISBN": "9786024075583",
	"Penerbit": "Laksana",
	"Berat": "0.18 kg",
	"Lebar": "14.0 cm",
	"Panjang": "20.0 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/kuliah-sukses-ip-4-0-gampaaang",
	"title": "Kuliah Sukses? Ip 4.0?Gampaaang!",
	"author": "Yuan Acitra",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "30 Jul 2015",
	"ISBN": "9786027701779",
	"Bahasa": "Indonesia",
	"Penerbit": "Solusi Distribusi",
	"Berat": "0.1500 kg",
	"Lebar": "14 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/menjadi-pribadi-memikat-berpengaruh-dan-percaya-diri-di-segala-situasi",
	"title": "Menjadi Pribadi Memikat, Berpengaruh dan Percaya Diri Di Segala Situasi",
	"author": "Hari Laksana",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "19 Des 2017",
	"ISBN": "9786023004355",
	"Bahasa": "Indonesia",
	"Penerbit": "Araska Publisher",
	"Berat": "0.165 kg",
	"Lebar": "14 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/the-ancient-chinese-wisdom-sc-cover-baru-isbn-lama",
	"title": "The Ancient Chinese Wisdom (Sc) Cover Baru Isbn Lama",
	"author": "Andri Wang",
	"Jumlah Halaman": "260.0",
	"Tanggal Terbit": "13 Jan 2020",
	"ISBN": "9789792291827",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.5 kg",
	"Lebar": "15.0 cm",
	"Panjang": "23.0 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/cara-bersikap-tegas-dalam-setiap-situasi-edisi-revisi",
	"title": "Cara Bersikap Tegas Dalam Setiap Situasi (Edisi Revisi)",
	"author": "Sue Hadfield",
	"Jumlah Halaman": "308",
	"Tanggal Terbit": "13 Mei 2019",
	"ISBN": "9786232161825",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.400 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/stop-stressing-start-living",
	"title": "Stop Stressing Start Living",
	"author": "Elisabeth Murni",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "20 Apr 2018",
	"ISBN": "9786024073688",
	"Bahasa": "Indonesia",
	"Penerbit": "Laksana",
	"Berat": "0.2 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/the-5-essential-people-skills-cover-baru",
	"title": "The 5 Essential People Skills - Cover Baru",
	"author": "Dale Carnegie Training",
	"Jumlah Halaman": "256",
	"Tanggal Terbit": "12 Nov 2018",
	"ISBN": "9789792251285",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.270 kg",
	"Lebar": "15 cm",
	"Panjang": "23 cm"
},
{
	"page": 80,
	"kategori 1": "non-fiksi",
	"kategori 2": "pengembangan-diri",
	"book_url": "https://www.gramedia.com/products/developing-the-leaders-around-you",
	"title": "Developing The Leaders Around You",
	"author": "John C. Maxwell",
	"Jumlah Halaman": "243",
	"Tanggal Terbit": "30 Jul 2013",
	"ISBN": "9786028482660",
	"Bahasa": "Indonesia",
	"Penerbit": "Mic Publishing",
	"Berat": "0.470 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
