{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/seri-aku-sayang-binatang-cici-ingin-terbang",
	"title": "Seri Aku Sayang Binatang : Cici Ingin Terbang",
	"author": "Wulan Mulya Pratiwi.",
	"Jumlah Halaman": "32.0",
	"Tanggal Terbit": "16 Des 2019",
	"ISBN": "9786024808938",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.12 kg",
	"Lebar": "20.0 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/mengapa-kau-menangis",
	"title": "Mengapa Kau Menangis?",
	"author": "Veronica Sri Utami",
	"Jumlah Halaman": "0.0",
	"Tanggal Terbit": "8 Agt 2019",
	"ISBN": "9786022315452",
	"Bahasa": "Indonesia",
	"Penerbit": "BPKGM",
	"Berat": "100.0 kg",
	"Lebar": "21.0 cm",
	"Panjang": "1.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/ayo-bersikap-baik-dodo-yang-rendah-hati",
	"title": "Ayo Bersikap Baik: Dodo yang Rendah Hati",
	"author": "Watiek Ideo",
	"Jumlah Halaman": "28",
	"Tanggal Terbit": "8 Mar 2019",
	"ISBN": "9786020628110",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.1 kg",
	"Lebar": "15 cm",
	"Panjang": "23 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/fun-math-4-berburu-harta-karun",
	"title": "Fun Math 4 : Berburu Harta Karun",
	"author": "May Belle",
	"Jumlah Halaman": "64.0",
	"Tanggal Terbit": "19 Agt 2019",
	"ISBN": "9786232164000",
	"Penerbit": "BHUANA ILMU POPULER",
	"Berat": "0.1 kg",
	"Lebar": "18.0 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/opredo-smart-arab-coloring-book-50-benda",
	"title": "Opredo Smart Arab Coloring Book: 50 Benda",
	"author": "Tim Oopredoo",
	"Jumlah Halaman": "50.0",
	"Tanggal Terbit": "25 Nov 2019",
	"ISBN": "9786230009839",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.14 kg",
	"Lebar": "14.0 cm",
	"Panjang": "19.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/my-little-pony-kekuatan-persahabatan-boardbook",
	"title": "My Little Pony: Kekuatan Persahabatan (Boardbook)",
	"author": "Olivia London",
	"Jumlah Halaman": "20.0",
	"Tanggal Terbit": "22 Okt 2019",
	"ISBN": "9786023677603",
	"Bahasa": "Indonesia",
	"Penerbit": "Curhat Anak Bangsa",
	"Berat": "0.3 kg",
	"Lebar": "17.0 cm",
	"Panjang": "17.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/petualangan-emily-dan-emmet-ajari-aku-membuat-macaroon",
	"title": "Petualangan Emily dan Emmet-Ajari Aku Membuat Macaroon",
	"author": "Arie Komalasari",
	"Jumlah Halaman": "48.0",
	"Tanggal Terbit": "4 Nov 2019",
	"ISBN": "9786230007484",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.15 kg",
	"Lebar": "19.0 cm",
	"Panjang": "19.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/simple-and-easy-chinese",
	"title": "Simple And Easy Chinese",
	"author": "Agustian & Kelly Rosalin",
	"Jumlah Halaman": "24",
	"Tanggal Terbit": "4 Des 2017",
	"ISBN": "9786024524197",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Widiasarana Indonesia",
	"Berat": "0.100 kg",
	"Lebar": "18 cm",
	"Panjang": "15 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/seri-dongeng-populer-dunia-anak-katak-bersuara-merdu",
	"title": "Seri Dongeng Populer Dunia : Anak Katak Bersuara Merdu",
	"author": "Lee Hyang Ahn",
	"Jumlah Halaman": "56",
	"Tanggal Terbit": "18 Mar 2019",
	"ISBN": "9786232160194",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.120 kg",
	"Lebar": "24 cm",
	"Panjang": "18 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/qanza-seri-hasan-anisa-idul-fitri-tiba",
	"title": "Qanza - Seri Hasan & Anisa : Idul Fitri Tiba!",
	"author": "Yasmeen Rahim",
	"Jumlah Halaman": "24",
	"Tanggal Terbit": "27 Mei 2019",
	"ISBN": "9786024805579",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1 kg",
	"Lebar": "20 cm",
	"Panjang": "17.5 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/mix-match-book-binatang-1",
	"title": "Mix & Match Book: Binatang 1",
	"author": "Efik Sukamto",
	"Jumlah Halaman": "14.0",
	"Tanggal Terbit": "4 Nov 2019",
	"ISBN": "9786230000294",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.12 kg",
	"Lebar": "15.0 cm",
	"Panjang": "15.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/qanza-seri-aku-bisa-aku-memakai-hijab-di-mana-saja",
	"title": "QANZA - Seri Aku Bisa : Aku Memakai Hijab di Mana Saja",
	"author": "Yasmin Rahim",
	"Jumlah Halaman": "16.0",
	"Tanggal Terbit": "18 Nov 2019",
	"ISBN": "9786024808488",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.15 kg",
	"Lebar": "20.0 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/opredo-tall-fold-book-colors",
	"title": "Opredo Tall Fold Book : Colors",
	"author": "Tim Oopredoo",
	"Jumlah Halaman": "10.0",
	"Tanggal Terbit": "6 Jan 2020",
	"ISBN": "719121593",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.12 kg",
	"Lebar": "12.0 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/seri-edukasi-britannica-tumbuhan",
	"title": "Seri Edukasi Britannica: Tumbuhan",
	"author": "Bombom Story",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "25 Feb 2019",
	"ISBN": "9786024833015",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.250 kg",
	"Lebar": "18 cm",
	"Panjang": "24 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/aku-ingin-cepat-besar",
	"title": "Aku Ingin Cepat Besar",
	"author": "Hong Hong, Da Q",
	"Jumlah Halaman": "32.0",
	"Tanggal Terbit": "19 Agt 2019",
	"ISBN": "9786024806903",
	"Penerbit": "m&c!",
	"Berat": "0.25 kg",
	"Lebar": "28.0 cm",
	"Panjang": "21.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/sticker-colour-create-farmyard-mighty-machines-sc",
	"title": "Sticker Colour Create: Farmyard Mighty Machines",
	"author": "Make Beleive",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "1 Feb 2019",
	"ISBN": "9781788437134",
	"Bahasa": "Indonesia",
	"Penerbit": "Make Believe",
	"Berat": "0.260 kg",
	"Lebar": "28 cm",
	"Panjang": "21.5 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/opredo-3-in-1-balita-hebat-pertamaku-1-non-pkp",
	"title": "Opredo 3 In 1 Balita Hebat Pertamaku 1 (Non Pkp)",
	"author": "A. Sitaresmi",
	"Jumlah Halaman": "44",
	"Tanggal Terbit": "5 Agt 2019",
	"ISBN": "9786020498478",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.252 kg",
	"Lebar": "7 cm",
	"Panjang": "7 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/menulis-mewarnai-huruf-hijaiyah-paket-buku-hijaiyah-seri",
	"title": "Menulis & Mewarnai Huruf Hijaiyah Paket Buku Hijaiyah (Seri 1, Seri 2, Seri 3)",
	"author": "Kak Elly",
	"Jumlah Halaman": "300.0",
	"Tanggal Terbit": "20 Agt 2023",
	"ISBN": "9786029252712",
	"Penerbit": "Firdauss Pressindo",
	"Berat": "0.215 kg",
	"Lebar": "18.0 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/seri-kimi-dan-kino-menghormati-orang-lain",
	"title": "Seri Kimi Dan Kino: Menghormati Orang Lain",
	"author": "Watiek Ideo",
	"Jumlah Halaman": "14.0",
	"Tanggal Terbit": "23 Sep 2019",
	"ISBN": "9786230004742",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.45 kg",
	"Lebar": "15.0 cm",
	"Panjang": "15.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/shimmer-shine-dinosaurus-di-halaman-rumah",
	"title": "Shimmer & Shine : Dinosaurus di Halaman Rumah",
	"author": "Nickelodeon",
	"Jumlah Halaman": "24.0",
	"Tanggal Terbit": "6 Jan 2020",
	"ISBN": "9786024808440",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.12 kg",
	"Lebar": "23.0 cm",
	"Panjang": "23.0 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/seri-dongeng-3d-nusantara-legenda-pulau-kemaro",
	"title": "Seri Dongeng 3D Nusantara: Legenda Pulau Kemaro",
	"author": "Lilis Hu Dan Lia Sidik",
	"Jumlah Halaman": "56",
	"Tanggal Terbit": "18 Des 2017",
	"ISBN": "9786024552725",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.100 kg",
	"Lebar": "20.5 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/seri-kalimat-thayyibah-ayo-ucapkan-insya-allah",
	"title": "Seri Kalimat Thayyibah : Ayo Ucapkan Insya Allah",
	"author": "Dian Kristiani",
	"Jumlah Halaman": "14",
	"Tanggal Terbit": "17 Jun 2019",
	"ISBN": "9786232162006",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.120 kg",
	"Lebar": "14 cm",
	"Panjang": "18 cm"
},
{
	"page": 51,
	"kategori 1": "buku-anak",
	"kategori 2": "anak-anak",
	"book_url": "https://www.gramedia.com/products/opredo-dot-to-dot-dan-mewarnai-alfabet-pertarungan-seru-di",
	"title": "Opredo Dot To Dot Dan Mewarnai Alfabet : Pertarungan Seru di Luar Angkasa",
	"author": "Animonsta Studio Sdn. Bhd",
	"Jumlah Halaman": "33.0",
	"Tanggal Terbit": "19 Agt 2019",
	"ISBN": "9786020483436",
	"Penerbit": "ELEX MEDIA KOMPUTINDO",
	"Berat": "0.067 kg",
	"Lebar": "21.0 cm",
	"Panjang": "27.0 cm"
},
