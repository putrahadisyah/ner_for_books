{
	"page": 26,
	"kategori 1": "non-fiksi",
	"kategori 2": "sosial",
	"book_url": "https://www.gramedia.com/products/filsafat-komunikasi",
	"title": "Filsafat Komunikasi",
	"author": "H.Aang Ridwan,M.Ag.",
	"Jumlah Halaman": "264.0",
	"Tanggal Terbit": "11 Okt 2019",
	"ISBN": "9789790763104",
	"Bahasa": "Indonesia",
	"Penerbit": "Pustaka Setia",
	"Berat": "0.315 kg",
	"Lebar": "15.5 cm",
	"Panjang": "23.5 cm"
},
{
	"page": 26,
	"kategori 1": "non-fiksi",
	"kategori 2": "sosial",
	"book_url": "https://www.gramedia.com/products/hitam-putih-kesultanan-demak",
	"title": "Hitam Putih Kesultanan Demak",
	"author": "Fery Taufiq",
	"Jumlah Halaman": "292.0",
	"Tanggal Terbit": "28 Agt 2019",
	"ISBN": "9786237145516",
	"Bahasa": "Indonesia",
	"Penerbit": "Araska Publisher",
	"Berat": "0.45 kg",
	"Lebar": "14.0 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 26,
	"kategori 1": "non-fiksi",
	"kategori 2": "sosial",
	"book_url": "https://www.gramedia.com/products/filsafat-di-indonesia-manusia-dan-budaya",
	"title": "Filsafat [di Indonesia] Manusia dan Budaya",
	"author": "A.setyo Wibowo",
	"Jumlah Halaman": "212.0",
	"Tanggal Terbit": "18 Nov 2019",
	"ISBN": "9786232410633",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Buku Kompas",
	"Berat": "0.25 kg",
	"Lebar": "15.0 cm",
	"Panjang": "23.0 cm"
},
