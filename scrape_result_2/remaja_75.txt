{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/save-the-queen-princess-academy",
	"title": "Save The Queen : Princess Academy",
	"author": "Romy Hernadi & Assyifa S. Arum",
	"Jumlah Halaman": "92",
	"Tanggal Terbit": "8 Mar 2016",
	"ISBN": "9786023671359",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "14 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/happy-prince-the",
	"title": "The Happy Prince",
	"author": "Akira Hagio",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "30 Mar 2016",
	"ISBN": "9786023398331",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/suvenir-dari-eropa-ghost-school-days",
	"title": "Suvenir Dari Eropa : Ghost School Days",
	"author": "Romy Hernadi & Assyifa S. Arum",
	"Jumlah Halaman": "92",
	"Tanggal Terbit": "8 Mar 2016",
	"ISBN": "9786023671397",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "14 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/nanamis-secret-love",
	"title": "Nanamis Secret Love",
	"author": "Ryo Misaki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "18 Jun 2015",
	"ISBN": "9786021122822",
	"Bahasa": "Indonesia",
	"Penerbit": "Tiga Lancar",
	"Berat": "0.2000 kg",
	"Lebar": "12 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/monster-mitos-legenda-naga-ular",
	"title": "Monster, Mitos & Legenda: Naga & Ular",
	"author": "Amber Books",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "9 Nov 2015",
	"ISBN": "9786020276083",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.2000 kg",
	"Lebar": "21.5 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/oppa-the-life-of-a-fangirl",
	"title": "Oppa! The Life of a Fangirl",
	"author": "Canti Clarinta",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "16 Nov 2015",
	"ISBN": "9786020322773",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.3000 kg",
	"Lebar": "15 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/runaway-comic-edition",
	"title": "Runaway : Comic Edition",
	"author": "Alim Sudio",
	"Jumlah Halaman": "78",
	"Tanggal Terbit": "25 Jun 2015",
	"ISBN": "9786027126589",
	"Bahasa": "Indonesia",
	"Penerbit": "M Books",
	"Berat": "0.2000 kg",
	"Lebar": "16 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/aishiteiru",
	"title": "Aishiteiru",
	"author": "Erin Kijima",
	"Tanggal Terbit": "20 Sep 2013",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1500 kg",
	"Lebar": "0 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/saint-seiya-episode-g-13-lc",
	"title": "Saint Seiya Episode G 13: Lc",
	"author": "Masami Kurumada & Megumu Okada",
	"Tanggal Terbit": "20 Okt 2014",
	"ISBN": "9786020250472",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.3000 kg",
	"Lebar": "0 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/conf-fairy-tail-13",
	"title": "Fairy Tail 13",
	"author": "Kodansha - Hiro Mashima",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "3 Des 2014",
	"ISBN": "978602002500",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.12 kg",
	"Lebar": "11.36 cm",
	"Panjang": "17.25 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/nurse-aoi-31-lc",
	"title": "Nurse Aoi 31: Lc",
	"author": "Ryo Koshino",
	"Tanggal Terbit": "13 Okt 2014",
	"ISBN": "9786020250793",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.3000 kg",
	"Lebar": "0 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/beelzebub-07",
	"title": "Beelzebub 07",
	"author": "Ryuhei Tamura",
	"Tanggal Terbit": "8 Nov 2013",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1500 kg",
	"Lebar": "0 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/blade-of-immortal-30-lc",
	"title": "Blade Of Immortal 30: Lc",
	"author": "Hiroaki Samura",
	"Tanggal Terbit": "27 Okt 2014",
	"ISBN": "9786020250465",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.3000 kg",
	"Lebar": "13 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/hunter-x-hunter-04",
	"title": "Hunter X Hunter 04 ( Print On Demand )",
	"author": "Yoshihiro Togashi",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "25 Okt 2013",
	"ISBN": "978602022515",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.11 kg",
	"Lebar": "11.0 cm",
	"Panjang": "17.0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/please-make-me-the-first",
	"title": "Please Make Me The First",
	"author": "Sumire",
	"Tanggal Terbit": "10 Nov 2014",
	"ISBN": "9786021122433",
	"Penerbit": "Tiga Lancar",
	"Berat": "0.1500 kg",
	"Lebar": "0 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/key-of-love-01",
	"title": "Key Of Love 01",
	"author": "Keiko Notoyama",
	"Tanggal Terbit": "",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1500 kg",
	"Lebar": "0 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/scarlet-fan-the-tale-of-eternal-love-10",
	"title": "Scarlet Fan - The Tale Of Eternal Love - 10",
	"author": "Kumagai Kyoko",
	"Jumlah Halaman": "183",
	"Tanggal Terbit": "13 Okt 2014",
	"ISBN": "9786022667728",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/k-on-highschool",
	"title": "K-On Highschool",
	"author": "Kakifly",
	"Tanggal Terbit": "29 Agt 2014",
	"ISBN": "9786020245898",
	"Penerbit": "Hobunsha",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/conf-lc-airgear-29",
	"title": "LC: Airgear 29",
	"author": "Kodansha - Oh!great",
	"Jumlah Halaman": "188",
	"Tanggal Terbit": "29 Okt 2014",
	"ISBN": "978602002759",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.12 kg",
	"Lebar": "12.0 cm",
	"Panjang": "18.0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/bermain-sambil-belajar-membaca-menulis-menghitung-mewarnai",
	"title": "Bermain Sambil Belajar Membaca, Menulis, Menghitung, Mewarnai",
	"author": "Veronica Dan Yan Seran",
	"Jumlah Halaman": "232",
	"Tanggal Terbit": "3 Okt 2016",
	"ISBN": "9786023757039",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Widiasarana Indonesia",
	"Berat": "0.4000 kg",
	"Lebar": "20.5 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/dooly-chinese-character-3-kemenangan-dalam-turnamen-dunia-barat",
	"title": "Dooly Chinese Character 3 - Kemenangan Dalam Turnamen Dunia Barat",
	"author": "Kim Soo Jung",
	"Jumlah Halaman": "212",
	"Tanggal Terbit": "11 Feb 2013",
	"ISBN": "9786020203621",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.3000 kg",
	"Lebar": "18 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/love-button-11",
	"title": "Love Button 11",
	"author": "Usami Maki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "30 Des 2014",
	"ISBN": "9786022669012",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/breaker-new-waves-the-09",
	"title": "Breaker New Waves,The 09",
	"author": "Park Jin-hwan",
	"Jumlah Halaman": "205",
	"Tanggal Terbit": "7 Nov 2014",
	"ISBN": "9786022668046",
	"Penerbit": "m&c!",
	"Berat": "0.3000 kg",
	"Lebar": "13 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/hana-kun-the-one-i-love-09",
	"title": "Hana-kun, The One I Love 9",
	"author": "Fuyu Kumaoka",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "20 Apr 2016",
	"ISBN": "9786023398843",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11.2 cm",
	"Panjang": "17.6 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/hayate-the-combat-butler-40",
	"title": "Hayate The Combat Butler 40",
	"author": "Hata Kenjiro",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "17 Agt 2016",
	"ISBN": "9786020291383",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11.5 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/dangu-05-lc",
	"title": "Lc: Dangu 05",
	"author": "Park Joong-ki",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "7 Okt 2015",
	"ISBN": "9786020273723",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.2000 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/i-got-the-girl-terbit-ulang",
	"title": "I Got The Girl (Terbit Ulang)",
	"author": "Shogakukan",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "8 Feb 2017",
	"ISBN": "9786024282677",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1250 kg",
	"Lebar": "11.2 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/chocolat-02",
	"title": "Chocolat 02",
	"author": "Shin Ji-sang & Geo",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "4 Mar 2015",
	"ISBN": "9786020259420",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.110 kg",
	"Lebar": "11 cm",
	"Panjang": "18 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/karneval-01",
	"title": "Karneval 01",
	"author": "Touya Mikanagi",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "30 Sep 2015",
	"ISBN": "9786023395064",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/shy-girl-in-love-06",
	"title": "Shy Girl In Love 06",
	"author": "Moe Yukimaru",
	"Jumlah Halaman": "183",
	"Tanggal Terbit": "6 Des 2014",
	"ISBN": "9786022668503",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm",
	"Panjang": "0 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/yamada-the-7-witches-vol-03",
	"title": "Yamada & The 7 Witches Vol. 03",
	"author": "Miki Yoshikawa",
	"Jumlah Halaman": "-",
	"Tanggal Terbit": "26 Agt 2015",
	"ISBN": "9786020271163",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/venus-versus-virus-4",
	"title": "Venus Versus Virus 4",
	"author": "Suzumi Atsushi",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "8 Apr 2015",
	"ISBN": "9786020261911",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/world-god-only-knows-the-14",
	"title": "The World God Only Knows 14",
	"author": "Wakaki Tamiki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "24 Feb 2016",
	"ISBN": "9786020280943",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/ghost-school-days-mix-ed-temen-misterius",
	"title": "Ghost School Days Mix Ed : Temen Misterius",
	"author": "Fayanna Ailisha Davianny",
	"Jumlah Halaman": "96",
	"Tanggal Terbit": "17 Nov 2015",
	"ISBN": "9786023670956",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Publishing",
	"Berat": "0.1500 kg",
	"Lebar": "16 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/lc-karakuri-circus-29",
	"title": "LC: Karakuri Circus 29",
	"author": "Fujita Kazuhiro",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "26 Jul 2017",
	"ISBN": "9786020436142",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "12 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/demon-king-43",
	"title": "Demon King 43",
	"author": "Ra In-soo, Kim Jae-hwan",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "4 Mar 2015",
	"ISBN": "9786020259413",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/cindaku-komik-fantasteen-28",
	"title": "Cindaku : Komik Fantasteen #28",
	"author": "Nelfi Syafrina Dkk",
	"Jumlah Halaman": "116",
	"Tanggal Terbit": "13 Okt 2015",
	"ISBN": "9786022428275",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Media Utama",
	"Berat": "0.1000 kg",
	"Lebar": "13 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/magic-weapon-genesis-15",
	"title": "Magic Weapon Genesis 15",
	"author": "Tony Wong, Cao Zhihao",
	"Jumlah Halaman": "160",
	"Tanggal Terbit": "14 Sep 2016",
	"ISBN": "9786024280901",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.2000 kg",
	"Lebar": "17 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/indecent-tropical-fish-2",
	"title": "Indecent Tropical Fish 2",
	"author": "Miyuki Kitagawa",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "12 Okt 2015",
	"ISBN": "9786021122990",
	"Bahasa": "Indonesia",
	"Penerbit": "Tiga Lancar",
	"Berat": "0.2000 kg",
	"Lebar": "12 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/love-deadline",
	"title": "Love Deadline",
	"author": "Aisahpl",
	"Jumlah Halaman": "124",
	"Tanggal Terbit": "10 Mar 2016",
	"ISBN": "9786021383759",
	"Bahasa": "Indonesia",
	"Penerbit": "Bentang Komik",
	"Berat": "0.1000 kg",
	"Lebar": "14 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/orange-marmalade-01",
	"title": "Orange Marmalade 01",
	"author": "Seok Woo",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "17 Agt 2016",
	"ISBN": "9786023399239",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.2000 kg",
	"Lebar": "14 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/komik-love-mate-series-1-kalau-monyet-jatuh-cinta",
	"title": "Komik Love Mate Series #1: Kalau Monyet Jatuh Cinta",
	"author": "Sweta Kartika",
	"Jumlah Halaman": "104",
	"Tanggal Terbit": "15 Okt 2015",
	"ISBN": "9786020851297",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "13 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/hayate-the-combat-butler-39",
	"title": "Hayate The Combat Butler 39",
	"author": "Hata Kenjiro",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "20 Jul 2016",
	"ISBN": "9786020289564",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/jaka-dan-pohon-kacang-ajaib-ed-revisi",
	"title": "Jaka Dan Pohon Kacang Ajaib Ed Revisi",
	"author": "Suroso&suhartanto",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "27 Mei 2016",
	"ISBN": "9789792145502",
	"Bahasa": "Indonesia",
	"Penerbit": "Kanisius",
	"Berat": "0.1000 kg",
	"Lebar": "20 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/komik-fantasteen-24-double-walker",
	"title": "Komik Fantasteen 24: Double Walker",
	"author": "Kessy Vanessa Dkk",
	"Jumlah Halaman": "124",
	"Tanggal Terbit": "27 Mei 2015",
	"ISBN": "9786023670260",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Publishing",
	"Berat": "0.2000 kg",
	"Lebar": "13 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/zara-pandai-bersyukur",
	"title": "Zara Pandai Bersyukur",
	"author": "Fayanna Ailisha Davianny",
	"Jumlah Halaman": "144",
	"Tanggal Terbit": "25 Apr 2016",
	"ISBN": "9786021614938",
	"Bahasa": "Indonesia",
	"Penerbit": "Indiva Media Kreasi",
	"Berat": "0.2000 kg",
	"Lebar": "14 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/countdown-7-days-03",
	"title": "Countdown 7 Days 03",
	"author": "Karakara Kemuri",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "9 Des 2015",
	"ISBN": "9786023396313",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/rinne-19",
	"title": "Rinne 19",
	"author": "Takahashi Rumiko",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "17 Feb 2016",
	"ISBN": "9786020280714",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/cantik-itu-dari-hati-peci-penulis-cilik-indonesia",
	"title": "Cantik Itu Dari Hati: Peci Penulis Cilik Indonesia",
	"author": "Meika Hapsari",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "14 Jan 2016",
	"ISBN": "9786021614839",
	"Bahasa": "Indonesia",
	"Penerbit": "Indiva Media Kreasi",
	"Berat": "0.1500 kg",
	"Lebar": "14 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/teen-house-01",
	"title": "Teen House 01",
	"author": "Yuu Yoshinaga",
	"Jumlah Halaman": "196",
	"Tanggal Terbit": "3 Jun 2015",
	"ISBN": "9786023392285",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/chocotan-chocolate-tan-02",
	"title": "Chocotan! - Chocolate & Tan- 02",
	"author": "Kozue Takeuchi",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "10 Agt 2016",
	"ISBN": "9786024280437",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/noragami-03",
	"title": "Noragami 03",
	"author": "Adachitoka",
	"Jumlah Halaman": "196",
	"Tanggal Terbit": "22 Apr 2015",
	"ISBN": "9786020263120",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/preparation-for-love-01-of-04",
	"title": "Preparation For Love 01 Of 04",
	"author": "Takako Yamazaki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "24 Feb 2016",
	"ISBN": "9786023397815",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/new-fugitive-lawyer-makoto-narita-the-05-lc",
	"title": "New Fugitive Lawyer Makoto Narita,The 05: Lc",
	"author": "Okamoto Sou",
	"Jumlah Halaman": "216",
	"Tanggal Terbit": "8 Jun 2016",
	"ISBN": "9786020284071",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "13 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/crimson-prince-07",
	"title": "Crimson Prince 07",
	"author": "Souta Kuwahara",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "4 Jan 2017",
	"ISBN": "9786020299037",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/larva-wow-sains-1-planet-bumi",
	"title": "Larva Wow Sains 1 : Planet Bumi",
	"author": "Storybus",
	"Jumlah Halaman": "196",
	"Tanggal Terbit": "13 Jun 2016",
	"ISBN": "9786023941520",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.3000 kg",
	"Lebar": "19 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/petit-flower",
	"title": "Petit Flower",
	"author": "Yasuko",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "24 Mei 2017",
	"ISBN": "9786024283681",
	"Penerbit": "m&c!",
	"Berat": "0.1290 kg",
	"Lebar": "11.2 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/demonic-detective-neuro-20",
	"title": "Demonic Detective Neuro 20",
	"author": "Yusei Matsui",
	"Jumlah Halaman": "-",
	"Tanggal Terbit": "16 Sep 2015",
	"ISBN": "9786020272573",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/first-love-dandelion",
	"title": "First Love Dandelion",
	"author": "Kayoru",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "3 Feb 2016",
	"ISBN": "9786023397235",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/scary-lessons-17",
	"title": "Scary Lessons 17",
	"author": "Emi Ishikawa",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "19 Apr 2017",
	"ISBN": "9786020413600",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/ghost-school-days-mix-edit-detektif-javania",
	"title": "Ghost School Days Mix Edit: Detektif Javania",
	"author": "Ivan Binuki & Assyifa S. Arum",
	"Jumlah Halaman": "96",
	"Tanggal Terbit": "1 Jun 2016",
	"ISBN": "9786023672059",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "23 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/pedora-bigband-01",
	"title": "Pedora Bigband 01",
	"author": "Sadaham",
	"Jumlah Halaman": "-",
	"Tanggal Terbit": "20 Mei 2015",
	"ISBN": "9786020265223",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/tetsugaku-letra-03",
	"title": "Tetsugaku Letra 03",
	"author": "Mizu Sahara",
	"Jumlah Halaman": "188",
	"Tanggal Terbit": "10 Jun 2015",
	"ISBN": "9786020266459",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/mushibugyo-18",
	"title": "Mushibugyo 18",
	"author": "Hiroshi Fukuda",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "16 Agt 2017",
	"ISBN": "9786020427812",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/swordsman-the-08",
	"title": "The Swordsman 08",
	"author": "Lee Jae Heon / Hong Ki Woo",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "6 Apr 2016",
	"ISBN": "9786023398775",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1500 kg",
	"Lebar": "13 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/town-where-you-live-24",
	"title": "Town Where You Live 24",
	"author": "Kouji Seo",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "8 Mar 2017",
	"ISBN": "9786020401669",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/yoimachi-overlord-03",
	"title": "Yoimachi Overlord 03",
	"author": "Ida Tatsuhiko, Asashi Mizutani",
	"Jumlah Halaman": "216",
	"Tanggal Terbit": "3 Mei 2017",
	"ISBN": "9786020418551",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/red-raven-08",
	"title": "Red Raven 08",
	"author": "Shinta Fujimoto",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "8 Mar 2017",
	"ISBN": "9786020401454",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/kkpk-lollipop-friendship",
	"title": "Kkpk: Lollipop Friendship",
	"author": "Ahdra Tsaqofa Hidayat",
	"Jumlah Halaman": "104",
	"Tanggal Terbit": "13 Jan 2016",
	"ISBN": "9786022428039",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1500 kg",
	"Lebar": "15 cm"
},
{
	"page": 75,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/yotsuba-07-terbit-ulang",
	"title": "Yotsuba&! 07 (Terbit Ulang)",
	"author": "Kiyohiko Azuma",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "2 Sep 2015",
	"ISBN": "9786020271590",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
