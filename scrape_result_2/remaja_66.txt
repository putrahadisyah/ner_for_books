{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/seki-kun-8",
	"title": "Seki-Kun 8",
	"author": "Morishige Takuma",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "16 Agt 2017",
	"ISBN": "9786020444567",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1250 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/lc-the-drops-of-god-36",
	"title": "LC: The Drops of God 36",
	"author": "Agi Tadashi,shu Okimoto",
	"Jumlah Halaman": "224",
	"Tanggal Terbit": "3 Jan 2017",
	"ISBN": "9786020298900",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1400 kg",
	"Lebar": "13 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/codename-sailor-v-2",
	"title": "Codename Sailor V 2",
	"author": "Naoko Takeuchi",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "15 Feb 2017",
	"ISBN": "9786020400440",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/chasing-love-terbit-ulang",
	"title": "Chasing Love (Terbit Ulang)",
	"author": "Ai Mizutani",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "18 Jan 2017",
	"ISBN": "9786024282271",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.2 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/valentine-4",
	"title": "Valentine # 4",
	"author": "Aswin Mc Siregar",
	"Jumlah Halaman": "28",
	"Tanggal Terbit": "19 Jan 2016",
	"ISBN": "9786026917119",
	"Bahasa": "Indonesia",
	"Penerbit": "Skylar Book",
	"Berat": "0.1000 kg",
	"Lebar": "16.5 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/komik-next-g-no-bullying",
	"title": "Komik Next G No Bullying",
	"author": "Fayza Valdaisyifa R & Hendhy, Dkk",
	"Jumlah Halaman": "104",
	"Tanggal Terbit": "19 Okt 2016",
	"ISBN": "9786023672479",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "21 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/zodiac-tour-for-a-lively-girl",
	"title": "Zodiac Tour For A Lively Girl",
	"author": "An Seong Hee / Choi Bong Seon",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "25 Apr 2016",
	"ISBN": "9786023398812",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1000 kg",
	"Lebar": "15 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/misteri-piala-sekolah-ghost-school-days-mix-edit",
	"title": "Misteri Piala Sekolah : Ghost School Days Mix Edit",
	"author": "Qs. Emmus & Citra Mustikawati, Dkk",
	"Jumlah Halaman": "96",
	"Tanggal Terbit": "3 Okt 2016",
	"ISBN": "9786023672387",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1200 kg",
	"Lebar": "23.5 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/star-darlings-vega-rahasia-sang-desainer-vega-and-the-fashion-disaster",
	"title": "Star Darlings#4: Vega And The Fashion Disaster (Vega & Rahasia Sang Desainer)",
	"author": "Disney",
	"Jumlah Halaman": "216",
	"Tanggal Terbit": "16 Jan 2017",
	"ISBN": "9786020336688",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.1600 kg",
	"Lebar": "13.5 cm",
	"Panjang": "20 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/apa-yang-terjadi-pada-dinosaurusku",
	"title": "Apa Yang Terjadi Pada Dinosaurusku?",
	"author": "Quentin Greban",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "5 Jun 2017",
	"ISBN": "9786023759514",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Widiasarana Indonesia",
	"Berat": "0.1000 kg",
	"Lebar": "24 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/kkpk-together-forever",
	"title": "KKPK.TOGETHER FOREVER",
	"author": "Dheanata Annoraputri (dinda)",
	"Jumlah Halaman": "108",
	"Tanggal Terbit": "4 Nov 2016",
	"ISBN": "9786024201500",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1200 kg",
	"Lebar": "14.5 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/princess-academy-opera-dongeng-kanaya-putri",
	"title": "Princess Academy Opera Dongeng: Kanaya Putri",
	"author": "Oyasujiwo",
	"Jumlah Halaman": "24",
	"Tanggal Terbit": "2 Mei 2016",
	"ISBN": "9786023671618",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.2000 kg",
	"Lebar": "21 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/psychic-detective-yakumo-the-alternate-story-1",
	"title": "Psychic Detective Yakumo - The Alternate Story 1",
	"author": "Manabu Kaminaga / Suzuka Oda",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "7 Jun 2017",
	"ISBN": "9786022104476",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1280 kg",
	"Lebar": "13 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/enid-blyton-findouters-mystery-of-missing-necklace-sc",
	"title": "Enid Blyton Findouters: Mystery Of Missing Necklace",
	"author": "Enid Blyton",
	"Jumlah Halaman": "224",
	"Tanggal Terbit": "1 Jan 2019",
	"ISBN": "9781444930818",
	"Bahasa": "Indonesia",
	"Penerbit": "Mitra Pelita Internusa",
	"Berat": "0.18 kg",
	"Lebar": "12 cm",
	"Panjang": "17 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/next-g-dunia-tersembunyi",
	"title": "Komik Next G Dunia Tersembunyi",
	"author": "Aisya Tsabita Azfatunnisa & Indha, Dkk",
	"Jumlah Halaman": "104",
	"Tanggal Terbit": "18 Jul 2016",
	"ISBN": "9786023672042",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "21 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/tiap-detik",
	"title": "Tiap Detik",
	"author": "Renato Reimundo Jr.",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "13 Mei 2015",
	"ISBN": "9786023392834",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.15 kg",
	"Lebar": "13 cm",
	"Panjang": "20 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/rebutan-hp-papa-kkpk-next-g",
	"title": "Rebutan Hp Papa : Kkpk.Next G",
	"author": "Darrel Raff Laksana & Elsy Dkk",
	"Jumlah Halaman": "104",
	"Tanggal Terbit": "19 Sep 2016",
	"ISBN": "9786023672363",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "15 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/angel-trumpet-08",
	"title": "Angel Trumpet 08",
	"author": "Michiyo Akaishi",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "19 Apr 2017",
	"ISBN": "9786024282790",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1280 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/beast-switch-2",
	"title": "Beast Switch 2",
	"author": "Shin Yumachi",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "8 Agt 2016",
	"ISBN": "9786023950447",
	"Bahasa": "Indonesia",
	"Penerbit": "Tiga Lancar",
	"Berat": "0.1000 kg",
	"Lebar": "12 cm",
	"Panjang": "18 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/misteri-tengkorak-purbakala-kkjd",
	"title": "Misteri Tengkorak Purbakala : Kkjd",
	"author": "Nyoman B S A Purushotama (komang)",
	"Jumlah Halaman": "108",
	"Tanggal Terbit": "18 Mei 2016",
	"ISBN": "9786024200107",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1000 kg",
	"Lebar": "14 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/larva-komik-dengkuran",
	"title": "Larva Komik : Dengkuran",
	"author": "Tuban Co.ltd",
	"Jumlah Halaman": "213",
	"Tanggal Terbit": "26 Sep 2016",
	"ISBN": "9786023942664",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.2800 kg",
	"Lebar": "18 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/k-fantasteen-47-penghuni-rumah-kayu",
	"title": "K. Fantasteen#47:Penghuni Rumah Kayu",
	"author": "Ciko Satrio, Dkk",
	"Jumlah Halaman": "124",
	"Tanggal Terbit": "6 Apr 2017",
	"ISBN": "9786024203580",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Publishing",
	"Berat": "0.1000 kg",
	"Lebar": "13 cm",
	"Panjang": "19 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/komik-10-pahlawan-islam",
	"title": "Komik :10 Pahlawan Islam",
	"author": "Izzah Annisa",
	"Jumlah Halaman": "116",
	"Tanggal Terbit": "29 Jan 2016",
	"ISBN": "9786021694336",
	"Bahasa": "Indonesia",
	"Penerbit": "Al Kautsar Kids",
	"Berat": "0.3000 kg",
	"Lebar": "20 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/pengembangan-emosi-ketika-aku-diadopsi",
	"title": "Pengembangan Emosi Ketika Aku Diadopsi",
	"author": "Nouf Zahrah Anastasi",
	"Jumlah Halaman": "32",
	"Tanggal Terbit": "19 Nov 2018",
	"ISBN": "9786024671013",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Erlangga",
	"Berat": "0.10 kg",
	"Lebar": "20 cm",
	"Panjang": "20 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/semua-sayang-gissele-kkpk",
	"title": "Semua Sayang Gissele : Kkpk",
	"author": "Ananda Shaafiyah (afia)",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "7 Sep 2016",
	"ISBN": "9786022429845",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.1500 kg",
	"Lebar": "15 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/14-th-guardian-fantasteen-numbers",
	"title": "14 Th Guardian : Fantasteen Numbers",
	"author": "Dwi Pratiwi, Dkk",
	"Jumlah Halaman": "100",
	"Tanggal Terbit": "20 Jun 2016",
	"ISBN": "9786024200466",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan",
	"Berat": "0.0800 kg",
	"Lebar": "13 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/montage-11-lc",
	"title": "LC: Montage 11",
	"author": "Jun Watanabe",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "21 Des 2016",
	"ISBN": "9786020298245",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1200 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/minions-minion-evil-lc",
	"title": "Lc: Minions - Minion Evil",
	"author": "Renaud Collin - Didier Ah-koon",
	"Jumlah Halaman": "48",
	"Tanggal Terbit": "28 Sep 2016",
	"ISBN": "9786020294247",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "18 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/komik-next-g-harus-bilang-mama",
	"title": "Komik Next G Harus Bilang Mama",
	"author": "Fidelia Amaryl Ghaisani, dkk",
	"Jumlah Halaman": "100.0",
	"Tanggal Terbit": "15 Okt 2019",
	"ISBN": "9786023677498",
	"Bahasa": "Indonesia",
	"Penerbit": "Curhat Anak Bangsa",
	"Berat": "0.1 kg",
	"Lebar": "15.0 cm",
	"Panjang": "21.0 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/fantasteen-deadly-call",
	"title": "Fantasteen Deadly Call",
	"author": "Sarah Ann",
	"Jumlah Halaman": "188",
	"Tanggal Terbit": "11 Jul 2019",
	"ISBN": "9786024208301",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Media Utama",
	"Berat": "0.16 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 66,
	"kategori 1": "buku-anak",
	"kategori 2": "remaja",
	"book_url": "https://www.gramedia.com/products/blue-giant-03",
	"title": "Blue Giant 03",
	"author": "Shinichi Ishizuka",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "6 Jun 2018",
	"ISBN": "9786024289089",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.130 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
