{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/new-dorabase-02",
	"title": "New Dorabase 02",
	"author": "Shintaro Mugiwara",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "24 Feb 2016",
	"ISBN": "9786020280387",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1000 kg",
	"Lebar": "11 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/secret-idol-02",
	"title": "Secret Idol 02",
	"author": "Chitose Yagami",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "27 Des 2017",
	"ISBN": "9786024287344",
	"Penerbit": "m&c!",
	"Berat": "0.115 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/tokyo-ghoul-re-05",
	"title": "Tokyo Ghoul: Re 05",
	"author": "Sui Ishida",
	"Jumlah Halaman": "220",
	"Tanggal Terbit": "8 Mei 2019",
	"ISBN": "9786024802912",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.125 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/brave-10-s-vol-2-lc",
	"title": "Lc: Brave 10 S Vol. 2",
	"author": "Kairi Shimotsuki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "29 Agt 2016",
	"ISBN": "9786020292243",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/a-hadgehog-in-love-04",
	"title": "A Hadgehog In Love 04",
	"author": "Nao Hinachi",
	"Jumlah Halaman": "192.0",
	"Tanggal Terbit": "27 Nov 2019",
	"ISBN": "9786230010149",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.093 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/yu-gi-oh-premium-01",
	"title": "Yu-Gi-Oh (Premium) 01",
	"author": "Kazuki Takahashi",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "2 Jun 2015",
	"ISBN": "9786020252445",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/komik-next-g-aku-sayang-ibu",
	"title": "Komik Next G Aku Sayang Ibu",
	"author": "Dkk",
	"Jumlah Halaman": "100",
	"Tanggal Terbit": "4 Jan 2019",
	"ISBN": "9786023675982",
	"Bahasa": "Indonesia",
	"Penerbit": "Muffin Graphics",
	"Berat": "0.15 kg",
	"Lebar": "14.5 cm",
	"Panjang": "21 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/there-are-walls-between-us-03",
	"title": "There Are Walls Between Us 03",
	"author": "Haru Tsukishima",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "7 Des 2016",
	"ISBN": "9786024281724",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.125 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/flowery-wedding",
	"title": "Flowery Wedding",
	"author": "Mashiro Ooki",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "14 Mar 2018",
	"ISBN": "9786024288303",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.125 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-war-of-roses-20",
	"title": "LC: War of Roses 20",
	"author": "Yukari Koyama",
	"Jumlah Halaman": "184.0",
	"Tanggal Terbit": "28 Nov 2018",
	"ISBN": "9786020483887",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.13 kg",
	"Lebar": "13.0 cm",
	"Panjang": "18.0 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/i-wont-ever-like-you-my-senpai-04",
	"title": "I Wont Ever Like You, My Senpai 04",
	"author": "Hatsuharu",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "27 Sep 2017",
	"ISBN": "9786024285630",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1200 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/kariage-kun-56",
	"title": "Kariage kun 56",
	"author": "Masashi Ueda",
	"Jumlah Halaman": "127",
	"Tanggal Terbit": "27 Jun 2018",
	"ISBN": "9786020461458",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.10 kg",
	"Lebar": "11 cm",
	"Panjang": "17 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/kariage-kun-26-terbit-ulang",
	"title": "Kariage Kun 26 (Terbit Ulang)",
	"author": "Masashi Ueda",
	"Jumlah Halaman": "132",
	"Tanggal Terbit": "9 Nov 2016",
	"ISBN": "9786020296272",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1200 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/captain-tsubasa-premium-14",
	"title": "Captain Tsubasa (Premium) 14",
	"author": "Yoichi Takahasi",
	"Jumlah Halaman": "336",
	"Tanggal Terbit": "23 Agt 2017",
	"ISBN": "9786020431246",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1320 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/one-winged-shuttle-4",
	"title": "One Winged Shuttle 4",
	"author": "Aguri Kurita",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "10 Okt 2018",
	"ISBN": "9786020483818",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.11 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/dragon-ball-vol-26",
	"title": "Dragon Ball Vol. 26",
	"author": "Akira Toriyama",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "7 Mar 2018",
	"ISBN": "9786020455969",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/sket-dance-28",
	"title": "Sket Dance 28",
	"author": "Kenta Shinohara",
	"Jumlah Halaman": "192.0",
	"Tanggal Terbit": "20 Nov 2019",
	"ISBN": "9786230007613",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.093 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-attack-on-titan-before-the-fall-15",
	"title": "LC: Attack on Titan Before The Fall 15",
	"author": "Hajime Isayama & Shiki Satoshi",
	"Jumlah Halaman": "224.0",
	"Tanggal Terbit": "23 Des 2019",
	"ISBN": "9786230010538",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.15 kg",
	"Lebar": "13.0 cm",
	"Panjang": "18.0 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/nyan-communication-1",
	"title": "Nyan Communication 1",
	"author": "Kiduki Kako",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "6 Sep 2017",
	"ISBN": "9786020444888",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lovely-idol",
	"title": "Lovely Idol",
	"author": "Yukichi Nakamura",
	"Jumlah Halaman": "182",
	"Tanggal Terbit": "1 Mei 2019",
	"ISBN": "9786024805142",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.110 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/aquanus-vol-1-sang-pewaris",
	"title": "Aquanus Vol. 1 : Sang Pewaris",
	"author": "Galang Tirtakusuma",
	"Jumlah Halaman": "160",
	"Tanggal Terbit": "27 Feb 2019",
	"ISBN": "9786024804060",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.180 kg",
	"Lebar": "21 cm",
	"Panjang": "14.8 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/citylite-too-good-for-you",
	"title": "CITYLITE: Too Good for You",
	"author": "Adeliany Azfar",
	"Jumlah Halaman": "336",
	"Tanggal Terbit": "21 Mei 2018",
	"ISBN": "9786020460796",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.220 kg",
	"Lebar": "12.5 cm",
	"Panjang": "19.5 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/did-i-mention-i-miss-you",
	"title": "Did I Mention I Miss You?",
	"author": "Estelle Maskame",
	"Jumlah Halaman": "424",
	"Tanggal Terbit": "28 Mei 2018",
	"ISBN": "9786024555900",
	"Bahasa": "Indonesia",
	"Penerbit": "Bhuana Ilmu Populer",
	"Berat": "0.350 kg",
	"Lebar": "13.5 cm",
	"Panjang": "20 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/three-kingdoms-03-sumpah-setia",
	"title": "Three Kingdoms 03 - Sumpah Setia",
	"author": "Wei Dong Chen /  Xiao Long Liang",
	"Jumlah Halaman": "174.0",
	"Tanggal Terbit": "11 Sep 2019",
	"ISBN": "9786024805654",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.25 kg",
	"Lebar": "17.0 cm",
	"Panjang": "24.5 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/tarung-legenda-vol-02",
	"title": "Tarung Legenda Vol 02",
	"author": "Team Reon",
	"Jumlah Halaman": "160",
	"Tanggal Terbit": "27 Nov 2017",
	"ISBN": "9786020892351",
	"Bahasa": "Indonesia",
	"Penerbit": "Team Reon",
	"Berat": "0.15 kg",
	"Lebar": "14 cm",
	"Panjang": "19.5 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/lc-giant-killing-42",
	"title": "LC: Giant Killing 42",
	"author": "Masaya Tsunamoto, Tsujitomo",
	"Jumlah Halaman": "232",
	"Tanggal Terbit": "25 Okt 2017",
	"ISBN": "9786020447575",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1250 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/paket-editor-s-choice-27",
	"title": "Paket Editor`S Choice 27",
	"author": "m&c!",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "30 Nov 2016",
	"ISBN": "531600057",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.3200 kg",
	"Lebar": "11.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/hayate-the-combat-butler-47",
	"title": "Hayate The Combat Butler 47",
	"author": "Hata Kenjiro",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "25 Okt 2017",
	"ISBN": "9786020446028",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1290 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/shaman-king-15",
	"title": "Shaman King 15",
	"author": "Hiroyuki Takei",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "6 Sep 2017",
	"ISBN": "9789792315660",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1300 kg",
	"Lebar": "11.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/gg-stories-of-oedo-5",
	"title": "GG stories of Oedo 5",
	"author": "Ai Takahashi",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "24 Apr 2019",
	"ISBN": "9786020497075",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.110 kg",
	"Lebar": "17.2 cm",
	"Panjang": "11.4 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/the-labours-of-hercules-tugastugas-hercules",
	"title": "The Labours of Hercules (TugasTugas Hercules)",
	"author": "Agatha Christie",
	"Jumlah Halaman": "416",
	"Tanggal Terbit": "21 Mei 2018",
	"ISBN": "9789792235159",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.2 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/koro-sensei-quest-iii",
	"title": "Koro Sensei Quest! III",
	"author": "Kizuku Watanabe, Jo Aoto, Yusei Matsui",
	"Jumlah Halaman": "176.0",
	"Tanggal Terbit": "18 Des 2019",
	"ISBN": "9786230011320",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.085 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/green-blood-03-lc",
	"title": "Lc: Green Blood 03",
	"author": "Kakizaki Masasumi",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "6 Mei 2015",
	"ISBN": "9786020264233",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1500 kg",
	"Lebar": "13 cm",
	"Panjang": "18 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/the-shadow-princess-wedding-02",
	"title": "The Shadow Princess Wedding 02",
	"author": "Kyoumochi Hisa",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "10 Jul 2019",
	"ISBN": "9786230001888",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/kkpk-class-nomik-26-pelangi-di-hatimu-1",
	"title": "KKPK Class Nomik #26: Pelangi Di Hatimu",
	"author": "Meiza Maulida Munawaroh",
	"Jumlah Halaman": "88",
	"Tanggal Terbit": "10 Des 2017",
	"ISBN": "9786024204693",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Publishing",
	"Berat": "0.10 kg",
	"Lebar": "14.5 cm",
	"Panjang": "21 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/chopperman-3",
	"title": "Chopperman 3",
	"author": "Hirofumi Takei",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "15 Agt 2018",
	"ISBN": "9786020477855",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.11 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/dharma-zahra-lovebirds-diary-02",
	"title": "Dharma & Zahra, Lovebirds Diary 02",
	"author": "Sweta Kartika",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "20 Feb 2019",
	"ISBN": "9786024803940",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.150 kg",
	"Lebar": "20 cm",
	"Panjang": "13.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/school-babysitters-13",
	"title": "School Babysitters 13",
	"author": "Hari Tokeino",
	"Jumlah Halaman": "192",
	"Tanggal Terbit": "18 Okt 2017",
	"ISBN": "9786024284817",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.1300 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/bestiarius-5",
	"title": "Bestiarius 5",
	"author": "Kakizaki Masasumi",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "7 Mar 2018",
	"ISBN": "9786020454580",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/my-cheeky-junior-01",
	"title": "My Cheeky Junior 01",
	"author": "Iroha Machino",
	"Jumlah Halaman": "176",
	"Tanggal Terbit": "2 Mei 2018",
	"ISBN": "9786024286743",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.10 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/ms-iroha-aku-02",
	"title": "Ms: Iroha & Aku 02",
	"author": "Ryoko Akisawa",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "10 Apr 2019",
	"ISBN": "9786020496818",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.120 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/garden-of-grimoire-02",
	"title": "Garden of Grimoire 02",
	"author": "Haru Sakurana",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "4 Mar 2019",
	"ISBN": "9786024802868",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.080 kg",
	"Lebar": "17.2 cm",
	"Panjang": "11.4 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/kurenai-10",
	"title": "Kurenai 10",
	"author": "Kentaro Katayama",
	"Jumlah Halaman": "196",
	"Tanggal Terbit": "15 Mar 2017",
	"ISBN": "9786020403250",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.130 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/dong-fang-wu-di-a-long-hu-men-side-story-03",
	"title": "Dong Fang Wu Di : A Long Hu Men Side Story 03",
	"author": "Tony Wong",
	"Jumlah Halaman": "120",
	"Tanggal Terbit": "23 Jan 2019",
	"ISBN": "9786024801861",
	"Bahasa": "Indonesia",
	"Penerbit": "m&c!",
	"Berat": "0.230 kg",
	"Lebar": "24.5 cm",
	"Panjang": "17 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/geronimo-stilton-70-phantom-of-bandit-sc",
	"title": "Geronimo Stilton #7 Phantom Of Bandit (SC)",
	"author": "Geronimo Stilton",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "1 Okt 2018",
	"ISBN": "9781338268539",
	"Bahasa": "English",
	"Penerbit": "Scholastic Us",
	"Berat": "0.20 kg",
	"Lebar": "13 cm",
	"Panjang": "19 cm"
},
{
	"page": 59,
	"kategori 1": "komik",
	"book_url": "https://www.gramedia.com/products/duel-masters-the-legend-of-the-overlord-gachi-5",
	"title": "Duel Masters The Legend Of The Overlord Gachi 5",
	"author": "Fujisaki Masato",
	"Jumlah Halaman": "160.0",
	"Tanggal Terbit": "11 Des 2019",
	"ISBN": "9786230004360",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.07 kg",
	"Lebar": "11.4 cm",
	"Panjang": "17.2 cm"
},
