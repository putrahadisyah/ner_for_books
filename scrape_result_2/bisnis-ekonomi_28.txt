{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/rich-factors-cara-berpikir-bekerja-orang-orang-kaya",
	"title": "Rich Factors: Cara Berpikir & Bekerja Orang-Orang Kaya",
	"author": "Edward Simanjuntak",
	"Jumlah Halaman": "232",
	"Tanggal Terbit": "5 Sep 2018",
	"ISBN": "9786025172526",
	"Bahasa": "Indonesia",
	"Penerbit": "Andaliman Books",
	"Berat": "0.210 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/ekonomi-publik-edisi-2-ekonomi-untuk-kesejahteraan-rakyat",
	"title": "Ekonomi Publik Edisi 2 - Ekonomi untuk Kesejahteraan Rakyat",
	"author": "Henry Faizal Noor",
	"Jumlah Halaman": "320",
	"Tanggal Terbit": "18 Mar 2017",
	"ISBN": "9789790624948",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Indeks",
	"Berat": "0.8 kg",
	"Lebar": "21 cm",
	"Panjang": "28 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/buku-praktis-dasar2-akuntansi",
	"title": "Buku Praktis Dasar2 Akuntansi",
	"author": "Indah Rahmawaty",
	"Jumlah Halaman": "164",
	"Tanggal Terbit": "12 Apr 2014",
	"ISBN": "9786021137123",
	"Bahasa": "Indonesia",
	"Penerbit": "Laskar Aksara",
	"Berat": "0.15 kg",
	"Lebar": "10 cm",
	"Panjang": "21 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/mendirikan-startup-yang-diburu-anggel-investor-big-fund",
	"title": "Mendirikan Startup Yang Diburu Anggel Investor & Big Fund",
	"author": "Hasnul Arifin",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "7 Okt 2016",
	"ISBN": "9789799115911",
	"Bahasa": "Indonesia",
	"Penerbit": "Media Pressindo",
	"Berat": "0.30 kg",
	"Lebar": "18 cm",
	"Panjang": "23 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/teori-praktik-pemodelan-bioekonomi-dalam-pengelolaan-perikanan-tangkap",
	"title": "Teori & Praktik Pemodelan Bioekonomi Dalam Pengelolaan Perikanan Tangkap",
	"author": "Nimmi Zulbainarni",
	"Jumlah Halaman": "338",
	"Tanggal Terbit": "7 Okt 2016",
	"ISBN": "9789794938843",
	"Bahasa": "Indonesia",
	"Penerbit": "Ipb Press",
	"Berat": "0.4000 kg",
	"Lebar": "15 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/aspek2-membuat-dokumen-pengadaan-evaluasi-penawaran",
	"title": "Aspek-aspek Membuat Dokumen Pengadaan & Evaluasi Penawaran",
	"author": "Mudjisantosa",
	"Jumlah Halaman": "200",
	"Tanggal Terbit": "25 Jan 2016",
	"ISBN": "9786027010796",
	"Bahasa": "Indonesia",
	"Penerbit": "Primaprint",
	"Berat": "0.2000 kg",
	"Lebar": "18 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/sukses-masuk-ptn-lewat-jalur-undangan",
	"title": "Sukses Masuk Ptn Lewat Jalur Undangan",
	"author": "Erika Paramitha",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "15 Mar 2016",
	"ISBN": "9786026882011",
	"Bahasa": "Indonesia",
	"Penerbit": "Dafa Publishing",
	"Berat": "0.1900 kg",
	"Lebar": "14 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/membedah-apbd",
	"title": "Membedah Apbd",
	"author": "Marselina Djayasinga",
	"Jumlah Halaman": "106",
	"Tanggal Terbit": "17 Feb 2016",
	"ISBN": "9786022625247",
	"Bahasa": "Indonesia",
	"Penerbit": "Graha Ilmu",
	"Berat": "0.3000 kg",
	"Lebar": "21 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/kinerja-karyawan-tinjauan-dari-dimendi-gaya-kepemimpinan",
	"title": "Kinerja Karyawan: Tinjauan dari Dimensi Gaya Kepemimpinan",
	"author": "Harun Samsudin",
	"Jumlah Halaman": "118",
	"Tanggal Terbit": "26 Okt 2018",
	"ISBN": "9786026417596",
	"Bahasa": "Indonesia",
	"Penerbit": "Indomedia Pustaka",
	"Berat": "0.250 kg",
	"Lebar": "17 cm",
	"Panjang": "24 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/monsta-x-shoot-out-to-stardom",
	"title": "Monsta X : Shoot Out to Stardom",
	"author": "Kang Jiae",
	"Jumlah Halaman": "128",
	"Tanggal Terbit": "25 Feb 2019",
	"ISBN": "9786020517179",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Widiasarana Indonesia",
	"Berat": "0.250 kg",
	"Lebar": "24 cm",
	"Panjang": "17 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/pemasaran-syariah-1",
	"title": "Pemasaran Syariah",
	"author": "Nurul Huda",
	"Jumlah Halaman": "168",
	"Tanggal Terbit": "17 Des 2017",
	"ISBN": "9786024221850",
	"Bahasa": "Indonesia",
	"Penerbit": "Kencana",
	"Berat": "0.17 kg",
	"Lebar": "13.5 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/branchless-banking",
	"title": "Branchless Banking",
	"author": "I.g.n. Alit Asmara Jaya",
	"Jumlah Halaman": "396",
	"Tanggal Terbit": "11 Apr 2017",
	"ISBN": "9786027829411",
	"Bahasa": "Indonesia",
	"Penerbit": "Expose Publika",
	"Berat": "0.4000 kg",
	"Lebar": "15 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/berkaca-dari-kegagalan-liberalisasi-ekonomi",
	"title": "Berkaca dari Kegagalan Liberalisasi Ekonomi",
	"author": "Soekarwo",
	"Jumlah Halaman": "320",
	"Tanggal Terbit": "4 Jun 2018",
	"ISBN": "9786020460369",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.210 kg",
	"Lebar": "13.5 cm",
	"Panjang": "20 cm"
},
{
	"page": 28,
	"kategori 1": "non-fiksi",
	"kategori 2": "bisnis-ekonomi",
	"book_url": "https://www.gramedia.com/products/menyusun-business-planrencana-aksi",
	"title": "Menyusun Business Plan&Rencana Aksi",
	"author": "Muchtar Af",
	"Jumlah Halaman": "240",
	"Tanggal Terbit": "2 Sep 2015",
	"ISBN": "9786022777151",
	"Bahasa": "Indonesia",
	"Penerbit": "Yrama Widya",
	"Berat": "0.25 kg",
	"Lebar": "15.5 cm",
	"Panjang": "23 cm"
},
