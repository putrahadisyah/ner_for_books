{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/alkitab-katolik-deu-sedang-052",
	"title": "Alkitab Katolik Deu Sedang 052",
	"author": "Lai",
	"Jumlah Halaman": "0.0",
	"Tanggal Terbit": "1 Jan 2009",
	"ISBN": "9789794636701",
	"Bahasa": "Indonesia",
	"Penerbit": "Lai",
	"Berat": "0.5 kg",
	"Lebar": "18.0 cm",
	"Panjang": "23.0 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/al-quranul-karim-mushaf-a4-besar-hc-al-kautsar",
	"title": "Al Quranul Karim Mushaf A4 Besar Hc Al Kautsar",
	"author": "Depag",
	"Jumlah Halaman": "632",
	"Tanggal Terbit": "18 Jan 2016",
	"ISBN": "9789795927297",
	"Bahasa": "Indonesia",
	"Penerbit": "Pustaka Al Kautsar",
	"Berat": "1.3 kg",
	"Lebar": "21.5 cm",
	"Panjang": "30 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/conf-the-mystery-of-creator-and-creation",
	"title": "The Mystery of Creator and Creation",
	"author": "Anand Krishna",
	"Jumlah Halaman": "146",
	"Tanggal Terbit": "25 Agt 2015",
	"ISBN": "9786020320168",
	"Bahasa": "Indonesia",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.1580 kg",
	"Lebar": "14 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/berbincang-dengan-rasulullah",
	"title": "Berbincang Dengan Rasulullah",
	"author": "",
	"Jumlah Halaman": "352",
	"Tanggal Terbit": "1 Jan 2001",
	"ISBN": "9786021361443",
	"Bahasa": "Indonesia",
	"Penerbit": "No Publisher",
	"Berat": "0.5000 kg",
	"Lebar": "18 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/majmu-syarif-edisi-ekseklusif-full-color-format-mushaf-al-qu",
	"title": "Majmu Syarif Edisi Eksklusif Full Color Format Mushaf Al Quran",
	"author": "Tim Wali Pustaka",
	"Jumlah Halaman": "320",
	"Tanggal Terbit": "19 Feb 2016",
	"ISBN": "9786027406407",
	"Bahasa": "Indonesia",
	"Penerbit": "Wali Pustaka",
	"Berat": "0.427 kg",
	"Lebar": "14 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/belajar-sendiri-semua-jenis-shalat",
	"title": "Belajar Sendiri Semua Jenis Shalat",
	"author": "Zainal Abidin",
	"Jumlah Halaman": "244",
	"Tanggal Terbit": "6 Jun 2017",
	"ISBN": "9786024071462",
	"Bahasa": "Indonesia",
	"Penerbit": "Laksana",
	"Berat": "0.2600 kg",
	"Lebar": "14 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/hijrahku-pelopor-perubahanku",
	"title": "Hijrahku Pelopor Perubahanku",
	"author": "Wahyu Ardy Purnomo",
	"Jumlah Halaman": "136.0",
	"Tanggal Terbit": "19 Agt 2019",
	"ISBN": "9786230004223",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.25 kg",
	"Lebar": "14.0 cm",
	"Panjang": "21.0 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/pernikahan-bahagia",
	"title": "Pernikahan Bahagia",
	"author": "Jarot Wijanarko",
	"Jumlah Halaman": "205",
	"Tanggal Terbit": "30 Okt 2018",
	"ISBN": "9786023880720",
	"Bahasa": "Indonesia",
	"Penerbit": "Buku Kita",
	"Berat": "0.214 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/jangan-takut-gagal-special-edition",
	"title": "Jangan Takut Gagal (Special Edition)",
	"author": "Aldilla Dharma",
	"Jumlah Halaman": "218",
	"Tanggal Terbit": "8 Nov 2017",
	"ISBN": "9789790173743",
	"Bahasa": "Indonesia",
	"Penerbit": "Qultum Media",
	"Berat": "0.22 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/dear-allah-jadikan-aku-muslimah-salehah-hc",
	"title": "Dear Allah Jadikan Aku Muslimah Salehah-Hc",
	"author": "@nashihatku",
	"Jumlah Halaman": "155",
	"Tanggal Terbit": "11 Apr 2017",
	"ISBN": "9786024180911",
	"Bahasa": "Indonesia",
	"Penerbit": "Salam",
	"Berat": "0.25 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/memahami-perjanjian-baru-tanah-nazaret",
	"title": "Memahami Perjanjian Baru - Tanah Nazaret",
	"author": "Toni Matas",
	"Jumlah Halaman": "64",
	"Tanggal Terbit": "21 Jan 2019",
	"ISBN": "9786020489063",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.1 kg",
	"Lebar": "18 cm",
	"Panjang": "24 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/sejarah-peradaban-islam-4",
	"title": "Sejarah Peradaban Islam",
	"author": "Prof. Dr. H. Syamruddin Nasution, M.Ag.",
	"Jumlah Halaman": "314",
	"Tanggal Terbit": "1 Nov 2018",
	"ISBN": "9786024256562",
	"Bahasa": "Indonesia",
	"Penerbit": "Rajagrafindo Persada",
	"Berat": "0.3 kg"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/7-cara-akselerasi-rezeki",
	"title": "7 Cara Akselerasi Rezeki",
	"author": "Akhmad Muhaimin Azzet",
	"Jumlah Halaman": "240",
	"Tanggal Terbit": "21 Jul 2016",
	"ISBN": "9786023912179",
	"Bahasa": "Indonesia",
	"Penerbit": "Diva Press",
	"Berat": "0.2400 kg",
	"Lebar": "20 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/kaidah-fikih-muamalah",
	"title": "Kaidah Fikih Muamalah",
	"author": "Enang Hidayat, M.Ag.",
	"Jumlah Halaman": "340",
	"Tanggal Terbit": "1 Mar 2019",
	"ISBN": "9786024463175",
	"Bahasa": "Indonesia",
	"Penerbit": "PT REMAJA ROSDAKARYA",
	"Berat": "0.46 kg",
	"Lebar": "15.4 cm",
	"Panjang": "24 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/nikmati-hidupmu-allah-bersamamu",
	"title": "Nikmati Hidupmu ALLAH Bersamamu",
	"author": "Didi Junaedi",
	"Jumlah Halaman": "195",
	"Tanggal Terbit": "3 Mei 2018",
	"ISBN": "9786025547225",
	"Bahasa": "Indonesia",
	"Penerbit": "Qaf Media",
	"Berat": "0.20 kg",
	"Lebar": "13.5 cm",
	"Panjang": "20 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/syaikh-siti-jenar-2-suluk-abdul-jalil-novel-sejarah",
	"title": "Syaikh Siti Jenar 2 : Suluk Abdul Jalil Novel Sejarah",
	"author": "Agus Sunyoto",
	"Jumlah Halaman": "312",
	"Tanggal Terbit": "16 Jun 2016",
	"ISBN": "9789794339619",
	"Bahasa": "Indonesia",
	"Penerbit": "Mizan Media Utama",
	"Berat": "0.2300 kg",
	"Lebar": "13 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/cahaya-di-atas-cahaya",
	"title": "Cahaya Di Atas Cahaya",
	"author": "Imam Al-Ghazali",
	"Jumlah Halaman": "143",
	"Tanggal Terbit": "24 Mei 2017",
	"ISBN": "9786021583418",
	"Bahasa": "Indonesia",
	"Penerbit": "Turos Pustaka",
	"Berat": "0.19 kg",
	"Lebar": "11.5 cm",
	"Panjang": "16.5 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/kisah2-alkitab-berstiker-musa-daud-daniel-cerita-lainnya",
	"title": "Kisah2 Alkitab Berstiker: Musa, Daud, Daniel&Cerita Lainnya",
	"author": "Sally Ann Wright & Moira Maclean",
	"Jumlah Halaman": "52",
	"Tanggal Terbit": "28 Apr 2015",
	"ISBN": "9786022312451",
	"Bahasa": "Indonesia",
	"Penerbit": "Bpk Gunung Mulia",
	"Berat": "0.1500 kg",
	"Lebar": "15 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/kisah-nyata-ridlo-ibu-membawa-berkah-laknat-ibu-membawa-peta",
	"title": "Kisah Nyata: Ridlo Ibu Membawa Berkah Laknat Ibu Membawa Peta",
	"author": "Aqilah Selma Amalia",
	"Jumlah Halaman": "180",
	"Tanggal Terbit": "18 Des 2017",
	"ISBN": "9786026187642",
	"Bahasa": "Indonesia",
	"Penerbit": "Dida Pustaka",
	"Berat": "0.155 kg",
	"Lebar": "14 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/tolonglah-orang-lain-maka-allah-akan-menolongmu",
	"title": "Tolonglah Orang Lain Maka Allah Akan Menolongmu",
	"author": "Dwi Suwiknyo Dan Penulis Lainnya",
	"Jumlah Halaman": "324",
	"Tanggal Terbit": "8 Apr 2018",
	"ISBN": "9786025781001",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Noktah",
	"Berat": "0.2 kg",
	"Lebar": "13 cm",
	"Panjang": "19 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/baguskan-akhlakmu",
	"title": "Baguskan Akhlakmu",
	"author": "Dr. Amr Muhammad Khalid",
	"Jumlah Halaman": "297",
	"Tanggal Terbit": "1 Feb 2018",
	"ISBN": "9786026721112",
	"Bahasa": "Indonesia",
	"Penerbit": "Qalam",
	"Berat": "0.235 kg",
	"Lebar": "13 cm",
	"Panjang": "20 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/dosa-dosa-istri-yang-mengundang-laknat-allah",
	"title": "Dosa-Dosa Istri Yang Mengundang Laknat Allah",
	"author": "Humaira El Rahmi",
	"Jumlah Halaman": "208",
	"Tanggal Terbit": "19 Okt 2017",
	"ISBN": "9786023004379",
	"Bahasa": "Indonesia",
	"Penerbit": "Araska",
	"Berat": "0.19 kg",
	"Lebar": "14 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/permainan-abhijna",
	"title": "Permainan Abhijna",
	"author": "Sheng Yen Lu",
	"Jumlah Halaman": "212.0",
	"Tanggal Terbit": "1 Okt 2019",
	"ISBN": "9786028877299",
	"Bahasa": "Indonesia",
	"Penerbit": "PT.Budaya Daden Indonesia",
	"Berat": "0.208 kg",
	"Lebar": "14.5 cm",
	"Panjang": "21.0 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/al-barjanji-terjemahnya-majmu-atu-al-mawalid-wa-adiyah",
	"title": "Al-Barjanji & Terjemahnya (Majmu Atu Al-Mawalid Wa Adiyah )",
	"author": "Imam Zainal Abidin Ja Far Bin H",
	"Jumlah Halaman": "256",
	"Tanggal Terbit": "28 Des 2015",
	"ISBN": "9786027720435",
	"Bahasa": "Indonesia",
	"Penerbit": "Lentera Hati",
	"Berat": "0.5000 kg",
	"Lebar": "16 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/cerita-cerita-menginspirasi-dari-nabi",
	"title": "Cerita Cerita Menginspirasi Dari Nabi",
	"author": "Utsman Qadri Mukanisi",
	"Jumlah Halaman": "300",
	"Tanggal Terbit": "22 Sep 2016",
	"ISBN": "9786027428096",
	"Bahasa": "Indonesia",
	"Penerbit": "Qalam",
	"Berat": "0.2500 kg",
	"Lebar": "13 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/al-quran-aljamil-kecil-tajwid-warna-terjemah-perkata-inggris",
	"title": "Al Quran Aljamil Kecil Tajwid Warna Terjemah Perkata Inggris",
	"author": "Endang",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "20 Nov 2012",
	"ISBN": "9786027307452",
	"Bahasa": "Indonesia",
	"Penerbit": "Endang",
	"Berat": "0.64 kg",
	"Lebar": "15 cm",
	"Panjang": "22 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/panduan-praktis-memahami-hukum-haidh-nifaz-istihadhoh",
	"title": "Panduan Praktis Memahami Hukum Haidh,Nifaz&Istihadhoh",
	"author": "Abul Fatik Al Asyad",
	"Jumlah Halaman": "127",
	"Tanggal Terbit": "20 Sep 2016",
	"ISBN": "9786026791665",
	"Bahasa": "Indonesia",
	"Penerbit": "Cv Aswaja Pressindo",
	"Berat": "0.2000 kg",
	"Lebar": "15 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/patah-hati-di-tanah-suci",
	"title": "Patah Hati di Tanah Suci",
	"author": "Taufik Saptoto Rohadi",
	"Jumlah Halaman": "340",
	"Tanggal Terbit": "4 Jun 2018",
	"ISBN": "9786022914112",
	"Bahasa": "Indonesia",
	"Penerbit": "Bentang Pustaka",
	"Berat": "0.270 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/juz-amma-terjemah-dan-pelafalan",
	"title": "Juz`Amma Terjemah Dan Pelafalan",
	"author": "-",
	"Tanggal Terbit": "21 Okt 2011",
	"ISBN": "9786029298130",
	"Penerbit": "Ziyad Visi Media",
	"Berat": "0.2000 kg",
	"Lebar": "20.5 cm",
	"Panjang": "0 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/lebih-dari-rindu",
	"title": "Lebih Dari Rindu",
	"author": "Aby A. Izzudin",
	"Jumlah Halaman": "170",
	"Tanggal Terbit": "3 Jan 2018",
	"ISBN": "9786026358387",
	"Bahasa": "Indonesia",
	"Penerbit": "Wahyu Qolbu",
	"Berat": "0.15 kg",
	"Lebar": "13 cm",
	"Panjang": "19 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/the-great-sahaba-hard-cover",
	"title": "The Great Sahaba",
	"author": "Rizem Aizid",
	"Jumlah Halaman": "490",
	"Tanggal Terbit": "25 Jan 2018",
	"ISBN": "9786024073114",
	"Bahasa": "Indonesia",
	"Penerbit": "Laksana",
	"Berat": "0.680 kg",
	"Lebar": "15.5 cm",
	"Panjang": "24 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/tenangkan-pikiran-hatimu-setiap-saat",
	"title": "Tenangkan Pikiran & Hatimu Setiap Saat",
	"author": "Shalih Ahmad Asy-Syami",
	"Jumlah Halaman": "367",
	"Tanggal Terbit": "1 Mar 2018",
	"ISBN": "9786025022616",
	"Bahasa": "Indonesia",
	"Penerbit": "Wali Pustaka",
	"Berat": "0.41 kg",
	"Lebar": "15 cm",
	"Panjang": "23 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/al-quran-mushaf-tajwid-an-naim-jaket-a7pl",
	"title": "Al Quran Mushaf Tajwid An-Na`Im Jaket A7/Pl",
	"author": "+++",
	"Jumlah Halaman": "604",
	"Tanggal Terbit": "31 Mei 2016",
	"ISBN": "9786023172870",
	"Bahasa": "Indonesia",
	"Penerbit": "Lbd",
	"Berat": "0.450 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/ilmu-falak-dari-sejarah-ke-teori-dan-aplikasi",
	"title": "Ilmu Falak Dari Sejarah Ke Teori Dan Aplikasi",
	"author": "Siti Tatmainul Qulub",
	"Jumlah Halaman": "322",
	"Tanggal Terbit": "16 Jul 2017",
	"ISBN": "9786024251550",
	"Penerbit": "Rajagrafindo Persada",
	"Berat": "0.40 kg",
	"Lebar": "15 cm",
	"Panjang": "22.5 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/17-menit-sehari-bisa-hafal-hadis-dan-artinya",
	"title": "17 Menit Sehari bisa Hafal Hadis dan Artinya",
	"author": "ADNAN RAHMADI",
	"Jumlah Halaman": "248",
	"Tanggal Terbit": "18 Feb 2019",
	"ISBN": "9786020490205",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.350 kg",
	"Lebar": "21 cm",
	"Panjang": "14 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/panduan-shalat-doa-dan-amalan-khusus-wanita",
	"title": "Panduan Shalat, Doa, dan Amalan Khusus Wanita",
	"author": "H.M. Amrin Rauf",
	"Jumlah Halaman": "None",
	"Tanggal Terbit": "1 Apr 2019",
	"ISBN": "9786024075156",
	"Bahasa": "Indonesia",
	"Penerbit": "Laksana",
	"Berat": "0.160 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/pandangan-hidup-muslim",
	"title": "Pandangan Hidup Muslim",
	"author": "Abdul Malik Karim Amrullah",
	"Jumlah Halaman": "268",
	"Tanggal Terbit": "11 Mar 2016",
	"ISBN": "9786022502968",
	"Bahasa": "Indonesia",
	"Penerbit": "Gema Insani",
	"Berat": "0.4000 kg",
	"Lebar": "15 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/genggam-waktumu-kesuksesan-akan-menjadi-milikmu",
	"title": "Genggam Waktumu, Kesuksesan akan Menjadi Milikmu",
	"author": "",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "5 Agt 2019",
	"ISBN": "9786230002823",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.280 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/keajaiban-resep-obat-nabi-menurut-sains-klasik-modern",
	"title": "Keajaiban Resep Obat Nabi: Menurut Sains Klasik&Modern",
	"author": "Joko Rinanto",
	"Jumlah Halaman": "354",
	"Tanggal Terbit": "21 Agt 2015",
	"ISBN": "9789791303804",
	"Bahasa": "Indonesia",
	"Penerbit": "Qisthi Press",
	"Berat": "0.4000 kg",
	"Lebar": "13 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/kumpulan-kultum-muslimah-sepanjang-tahun",
	"title": "Kumpulan Kultum Muslimah Sepanjang Tahun",
	"author": "A.Septiyani",
	"Jumlah Halaman": "420",
	"Tanggal Terbit": "10 Apr 2018",
	"ISBN": "9786025638336",
	"Bahasa": "Indonesia",
	"Penerbit": "Mueeza",
	"Berat": "0.40 kg",
	"Lebar": "18 cm",
	"Panjang": "22 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/rediscovering-discipleship-menemukan-ulang-pemuridan",
	"title": "Rediscovering Discipleship (Menemukan Ulang Pemuridan)",
	"author": "Robby Gallaty",
	"Jumlah Halaman": "300",
	"Tanggal Terbit": "5 Mar 2018",
	"ISBN": "9786021302484",
	"Bahasa": "Indonesia",
	"Penerbit": "Literatur Perkantas Jatim",
	"Berat": "0.230 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/satu-hari-bersamamu-for-one-more-day",
	"title": "Satu Hari Bersamamu (For One More Day)",
	"author": "Mitch Albom",
	"Jumlah Halaman": "248.0",
	"Tanggal Terbit": "26 Agt 2019",
	"ISBN": "9786020333403",
	"Penerbit": "Gramedia Pustaka Utama",
	"Berat": "0.18 kg",
	"Lebar": "13.5 cm",
	"Panjang": "20.0 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/ya-allah-maaf-saya-tidak-ada-waktu-untuk-mu",
	"title": "Ya Allah .. Maaf Saya Tidak Ada Waktu Untuk Mu",
	"author": "Dewi A.z",
	"Jumlah Halaman": "288",
	"Tanggal Terbit": "23 Sep 2016",
	"ISBN": "9786027485297",
	"Bahasa": "Indonesia",
	"Penerbit": "Mueeza",
	"Berat": "0.2500 kg",
	"Lebar": "15 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/dear-allah-ternyata-engkau-dekat",
	"title": "Dear Allah, Ternyata Engkau Dekat",
	"author": "@inspirasialquran",
	"Jumlah Halaman": "232",
	"Tanggal Terbit": "2 Mei 2017",
	"ISBN": "9786026358226",
	"Bahasa": "Indonesia",
	"Penerbit": "Wahyu Qolbu",
	"Berat": "0.2320 kg",
	"Lebar": "13 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/trias-muslimatika-edisi-revisi",
	"title": "Trias Muslimatika Edisi Revisi",
	"author": "Dr. Davrina Rianda",
	"Jumlah Halaman": "336.0",
	"Tanggal Terbit": "4 Nov 2019",
	"ISBN": "9786230008818",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.4 kg",
	"Lebar": "14.0 cm",
	"Panjang": "21.0 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/tak-kenal-maka-tak-dakwah",
	"title": "Tak Kenal, Maka Tak Dakwah",
	"author": "Mahestha Rastha Andaara",
	"Jumlah Halaman": "256",
	"Tanggal Terbit": "25 Mar 2019",
	"ISBN": "9786020495835",
	"Bahasa": "Indonesia",
	"Penerbit": "Elex Media Komputindo",
	"Berat": "0.350 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/manajemen-dalam-perspektif-alquran-dan-hadis",
	"title": "Manajemen Dalam Perspektif Alquran Dan Hadis",
	"author": "Dr.Dodo Murtado,M.Si. / Dr.Iis Suhayati, M.Ag / Uay Zoharudin S.Ag.,M.Pd.I",
	"Jumlah Halaman": "248.0",
	"Tanggal Terbit": "1 Okt 2019",
	"ISBN": "9786232050150",
	"Bahasa": "Indonesia",
	"Penerbit": "Yrama Widya",
	"Berat": "0.305 kg",
	"Lebar": "15.5 cm",
	"Panjang": "24.0 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/innallaha-maana",
	"title": "Innallaha Ma`Ana",
	"author": "#Hambaallah",
	"Jumlah Halaman": "124",
	"Tanggal Terbit": "1 Des 2017",
	"ISBN": "9786026744074",
	"Bahasa": "Indonesia",
	"Penerbit": "Magenta Media",
	"Berat": "0.243 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/rahasia-al-fatihah",
	"title": "Rahasia Al-Fatihah",
	"author": "Al-thabari Dkk",
	"Jumlah Halaman": "260",
	"Tanggal Terbit": "25 Mei 2016",
	"ISBN": "9786027428010",
	"Bahasa": "Indonesia",
	"Penerbit": "Qalam",
	"Berat": "0.2000 kg",
	"Lebar": "13 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/christ-in-crisis-menemukan-oasis-di-tengah-krisi-kehidupan",
	"title": "Christ In Crisis: Menemukan Oasis Di Tengah Krisi Kehidupan",
	"author": "Petrus Kwik",
	"Jumlah Halaman": "156",
	"Tanggal Terbit": "4 Mei 2016",
	"ISBN": "9786027238350",
	"Bahasa": "Indonesia",
	"Penerbit": "Spirit Grafindo",
	"Berat": "0.1200 kg",
	"Lebar": "14 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/seri-kisah-alkitab-1",
	"title": "Seri Kisah Alkitab 1",
	"author": "Ben Mboi",
	"Jumlah Halaman": "37",
	"Tanggal Terbit": "24 Mei 2017",
	"ISBN": "9789795657811",
	"Bahasa": "Indonesia",
	"Penerbit": "Obor",
	"Berat": "0.10 kg",
	"Lebar": "20 cm",
	"Panjang": "20 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/tuntunan-generasi-muda",
	"title": "Tuntunan Generasi Muda",
	"author": "Badiuzzaman Said Nursi",
	"Jumlah Halaman": "218",
	"Tanggal Terbit": "12 Mar 2018",
	"ISBN": "9786027381377",
	"Bahasa": "Indonesia",
	"Penerbit": "Risalah Nur Press",
	"Berat": "0.225 kg",
	"Lebar": "13 cm",
	"Panjang": "19 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/unstolen-christmas-natal-yang-tak-dapat-dicuri",
	"title": "Unstolen Christmas : Natal yang Tak Dapat Dicuri",
	"author": "Petruk Kwik",
	"Jumlah Halaman": "152",
	"Tanggal Terbit": "12 Okt 2015",
	"ISBN": "9786027238329",
	"Bahasa": "Indonesia",
	"Penerbit": "Gloria Usaha Mulia",
	"Berat": "0.1500 kg",
	"Lebar": "14 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/misteri-dajjal-misteri-di-balik-angka-666",
	"title": "Misteri Dajjal (Misteri Di Balik Angka 666)",
	"author": "Wiyanto Suud",
	"Jumlah Halaman": "220",
	"Tanggal Terbit": "22 Okt 2013",
	"ISBN": "9786029185218",
	"Bahasa": "Indonesia",
	"Penerbit": "Belanoor",
	"Berat": "0.165 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/muslimah-yang-disenangi-allah-rasulullah-dan-suami",
	"title": "Muslimah Yang Disenangi Allah, Rasulullah, Dan Suami",
	"author": "Ummu Kaysa",
	"Jumlah Halaman": "224",
	"Tanggal Terbit": "2 Agt 2019",
	"ISBN": "9786025781681",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Noktah",
	"Berat": "0.25 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/muhammad-nabi-cinta",
	"title": "Muhammad Nabi Cinta",
	"author": "Craig Considine",
	"Jumlah Halaman": "300",
	"Tanggal Terbit": "19 Sep 2018",
	"ISBN": "9786023855384",
	"Bahasa": "Indonesia",
	"Penerbit": "Noura Books Publishing",
	"Berat": "0.300 kg",
	"Lebar": "14 cm",
	"Panjang": "21 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/dari-kuntum-menjadi-bunga-jilid-2",
	"title": "Dari Kuntum Menjadi Bunga Jilid 2",
	"author": "Ibnu Basyar",
	"Jumlah Halaman": "278",
	"Tanggal Terbit": "17 Nov 2018",
	"ISBN": "9786027769212",
	"Bahasa": "Indonesia",
	"Penerbit": "Al Qalam",
	"Berat": "0.383 kg",
	"Lebar": "23 cm",
	"Panjang": "15 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/ibadah-para-juara",
	"title": "Ibadah Para Juara",
	"author": "Rizem Aizid",
	"Jumlah Halaman": "228",
	"Tanggal Terbit": "19 Des 2016",
	"ISBN": "9786022962199",
	"Bahasa": "Indonesia",
	"Penerbit": "Sabil",
	"Berat": "0.20 kg",
	"Lebar": "14 cm",
	"Panjang": "25 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/metode-asy-syafii-cara-praktis-baca-al-quran",
	"title": "Metode Asy-Syafi`i: Cara Praktis Baca al-Qur`an",
	"author": "Abu Ya'la Kurnaedi",
	"Jumlah Halaman": "72",
	"Tanggal Terbit": "5 Agt 2017",
	"ISBN": "9786028062732",
	"Penerbit": "Pustaka Imam Asy Syafi`I",
	"Berat": "0.16 kg",
	"Lebar": "18 cm",
	"Panjang": "25 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/beginilah-amalan-penghuni-surga-saat-di-dunia",
	"title": "Beginilah Amalan Penghuni Surga Saat Di Dunia",
	"author": "A. Musabbih",
	"Jumlah Halaman": "228.0",
	"Tanggal Terbit": "30 Okt 2019",
	"ISBN": "9786237145912",
	"Bahasa": "Indonesia",
	"Penerbit": "Araska Publisher",
	"Berat": "0.201 kg",
	"Lebar": "14.0 cm",
	"Panjang": "20.5 cm"
},
{
	"page": 97,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/intervensi-orang-tua-dalam-rumah-tangga-anak-dalam-islam-bolehkah",
	"title": "Intervensi Orang Tua Dalam Rumah Tangga Anak, Dalam Islam Bolehkah?",
	"author": "M. Nur Kholis Al Amin",
	"Jumlah Halaman": "100",
	"Tanggal Terbit": "7 Nov 2016",
	"ISBN": "9786023520121",
	"Bahasa": "Indonesia",
	"Penerbit": "Solusi Distribusi",
	"Berat": "1.0000 kg",
	"Lebar": "14 cm"
},
