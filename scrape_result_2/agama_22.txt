{
	"page": 22,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/berdhuhalah-allah-menjaminmu-kaya",
	"title": "Berdhuhalah, Allah Menjaminmu Kaya",
	"author": "Ustadz Iqro’ Al-Firdaus",
	"Jumlah Halaman": "184",
	"Tanggal Terbit": "8 Jan 2019",
	"ISBN": "9786025781421",
	"Bahasa": "Indonesia",
	"Penerbit": "Penerbit Noktah",
	"Berat": "0.2 kg",
	"Lebar": "14 cm",
	"Panjang": "20 cm"
},
{
	"page": 22,
	"kategori 1": "non-fiksi",
	"kategori 2": "agama",
	"book_url": "https://www.gramedia.com/products/komik-gaul-ala-rasul",
	"title": "Komik Gaul Ala Rasul",
	"author": "Fajar Istiqlal",
	"Jumlah Halaman": "104",
	"Tanggal Terbit": "21 Feb 2017",
	"ISBN": "9786022150046",
	"Bahasa": "Indonesia",
	"Penerbit": "Kaysa Media",
	"Berat": "0.125 kg",
	"Lebar": "15.5 cm",
	"Panjang": "23.5 cm"
},
