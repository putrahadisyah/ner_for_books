{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sdmi-fokus-pendalaman-materi-usbn-2019,
	title: SD/MI Fokus Pendalaman Materi USBN 2019,
	author: Tim Tentor Pakar,
	Jumlah Halaman: 608,
	Tanggal Terbit: 1 Agt 2018,
	ISBN: 9786025169151,
	Bahasa: Indonesia,
	Penerbit: Oxygen Media Ilmu,
	Berat: 1.0 kg,
	Lebar: 19 cm,
	Panjang: 26 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/pendidikan-karakter-ki-hadjar-dewantara,
	title: Pendidikan Karakter Ki Hadjar Dewantara,
	author: SITA ACETYLENA,
	Jumlah Halaman: 136,
	Tanggal Terbit: 9 Jul 2018,
	ISBN: 9786020899596,
	Bahasa: Indonesia,
	Penerbit: Madani,
	Berat: 0.165 kg,
	Lebar: 15.5 cm,
	Panjang: 23 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sd-kelas-iv-tematik-kayanya-negeriku-jilid-4i-semester2-kurikulum-2013-revisi-2016,
	title: SD Kelas IV Tematik Kayanya Negeriku Jilid 4I Semester2 Kurikulum 2013 Revisi 2016,
	author: Dhiah Saptorini & Lili Nurlaili,
	Jumlah Halaman: 162,
	Tanggal Terbit: 1 Jul 2017,
	ISBN: 9786022996699,
	Bahasa: Indonesia,
	Penerbit: Pt Yudhistira Ghalia Indonesia,
	Berat: 0.400 kg,
	Lebar: 21.5 cm,
	Panjang: 28.5 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sdmi-kl-3-sakti-ipa-ktsp-kur-2013,
	title: SD/MI Kelas 3 Sakti IPA KTSP Kurikulum 2013,
	author: Irene Mja,
	Jumlah Halaman: 121,
	Tanggal Terbit: 23 Agt 2016,
	ISBN: 9786022983705,
	Bahasa: Indonesia,
	Penerbit: Penerbit Erlangga,
	Berat: 0.300 kg,
	Lebar: 22 cm,
	Panjang: 27 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sma-kelas-xii-pbt-ekonomi-jilid-3b-semester-2-kurikulum-2013-revisi,
	title: SMA Kelas XII PBT Ekonomi Jilid 3B Semester 2 Kurikulum 2013 Revisi,
	author: Eri Kasman,
	Jumlah Halaman: 99,
	Tanggal Terbit: 1 Apr 2018,
	ISBN: 9786024450373,
	Bahasa: Indonesia,
	Penerbit: Pt Yudhistira Ghalia Indonesia,
	Berat: 0.250 kg,
	Lebar: 20 cm,
	Panjang: 27 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sma-kelas-xi-workbook-talk-active-jilid-2b-semester-2-kurikulum-2013-revisi,
	title: SMA Kelas XI Workbook Talk Active Jilid 2B Semester 2 Kurikulum 2013 Revisi,
	author: FX. SUKRISTIONO, NURSAHID, DKK,
	Jumlah Halaman: 62,
	Tanggal Terbit: 1 Apr 2018,
	ISBN: 9786024450632,
	Bahasa: Indonesia,
	Penerbit: Pt Yudhistira Ghalia Indonesia,
	Berat: 0.250 kg,
	Lebar: 20 cm,
	Panjang: 27 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sd-kl3-sasebi-3-ktsp-2006,
	title: SD Kelas 3 SASEBI Saya Senang Berbahasa Indonesia 3 KTSP 2006,
	author: Hanif Nurcholis,
	Jumlah Halaman: 198,
	Tanggal Terbit: 19 Sep 2012,
	ISBN: 9789797819187,
	Bahasa: Indonesia,
	Penerbit: Penerbit Erlangga,
	Berat: 0.35 kg,
	Lebar: 17 cm,
	Panjang: 25 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/the-amazing-book-of-korean-conversation-for-millenials,
	title: The Amazing Book Of Korean Conversation For Millenials,
	author: Daru Purboning Astuti,
	Jumlah Halaman: 236,
	Tanggal Terbit: 28 Des 2018,
	ISBN: 9786020770123,
	Bahasa: Indonesia,
	Penerbit: Pusat Kajian Bahasa,
	Berat: 0.30 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/all-new-tes-cpns-20182019,
	title: All-New Tes CPNS 2018/2019,
	author: Tim Garuda Eduka,
	Jumlah Halaman: 762.0,
	Tanggal Terbit: 20 Feb 2018,
	ISBN: 9786025710018,
	Bahasa: Indonesia,
	Penerbit: C Media,
	Berat: 1.2 kg,
	Lebar: 19.0 cm,
	Panjang: 26.0 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sd-kl-6-seri-tematik-menuju-masyarakat-sehat-6f-k-2013,
	title: SD Kelas 6 Seri Tematik Menuju Masyarakat Sehat 6F Kurikulum 2013,
	author: Dhiah Saptorini & Lili Nurlaili,
	Jumlah Halaman: 121,
	Tanggal Terbit: 4 Jul 2016,
	ISBN: 9789790929098,
	Bahasa: Indonesia,
	Penerbit: Yudhistira Ghalia Indonesia,
	Berat: 0.300 kg,
	Lebar: 20 cm,
	Panjang: 27 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/super-modul-matematika-sma-kelas-x-xi-dan-xii,
	title: Super Modul Matematika SMA Kelas X, XI, dan XII,
	author: Timtentor Edukasi,
	Jumlah Halaman: 384,
	Tanggal Terbit: 15 Okt 2018,
	ISBN: 9786020512280,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.8 kg,
	Lebar: 21 cm,
	Panjang: 28 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/kamus-lengkap-ingg-ind-ind-ingg-tenses-reg-verbs-irreg-ver,
	title: Kamus Lengkap Inggris-Indonesia, Indonesia-Inggris Untuk Pelajar,Mahasiswa, Dan Umum,
	author: Team Pustaka Agung Harapan,
	Jumlah Halaman: 672,
	Tanggal Terbit: 1 Jan 2009,
	ISBN: 9789792685671,
	Bahasa: Indonesia,
	Penerbit: Pustaka Agung Harapan,
	Berat: 0.650 kg,
	Lebar: 14.5 cm,
	Panjang: 21 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sma-kelas-x-pbt-sejarah-indonesia-program-wajib-jilid-1a-semester-1-kurikulum-2013-revisi,
	title: SMA Kelas X PBT Sejarah Indonesia Program Wajib Jilid 1A Semester 1 Kurikulum 2013 Revisi,
	author: Tim Sejarah,
	Jumlah Halaman: 59,
	Tanggal Terbit: 1 Jun 2017,
	ISBN: 9786024450045,
	Bahasa: Indonesia,
	Penerbit: Pt Yudhistira Ghalia Indonesia,
	Berat: 0.250 kg,
	Lebar: 20 cm,
	Panjang: 27 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/riset-akuntansi,
	title: Kajian Riset Akuntansi,
	author: Hery, S.E., M.SI., CRP., RSA., CFRM.,
	Jumlah Halaman: 124,
	Tanggal Terbit: 17 Apr 2017,
	ISBN: 9786023759026,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.220 kg,
	Lebar: 16 cm,
	Panjang: 24 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/trik-smart-kuasai-matematika-sdmi-kelas-456,
	title: Trik Smart Kuasai Matematika SD/MI Kelas 4,5,6,
	author: Tim Alfa Eduka,
	Jumlah Halaman: 212,
	Tanggal Terbit: 8 Agt 2018,
	ISBN: 9786020504629,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.14 kg,
	Lebar: 12.0 cm,
	Panjang: 18.0 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/mi-kl-iv-aku-cinta-bhs-arab-jl-4-pengayaan,
	title: Aku Cinta Bahasa Arab Madrasah Ibtidaiyah untuk Kelas IV Madrasah Ibtidaiyah,
	author: Agus Wahyudi,
	Jumlah Halaman: 116,
	Tanggal Terbit: 1 Jun 2017,
	ISBN: 9786022577430,
	Bahasa: Indonesia,
	Penerbit: Tiga Serangkai,
	Berat: 0.25 kg,
	Lebar: 18 cm,
	Panjang: 25 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/kamus-bahasa-indonesia-sunda-sunda-indonesia,
	title: Kamus Bahasa Indonesia-Sunda, Sunda-Indonesia, Sunda-Sunda,
	author: Asep Kusnandar,
	Jumlah Halaman: 282,
	Tanggal Terbit: 27 Agt 2014,
	ISBN: 9786021465936,
	Bahasa: Indonesia,
	Penerbit: Buku Pintar,
	Berat: 0.250 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/mahir-kimia-smama-ipa,
	title: Mahir Kimia SMA/MA IPA,
	author: Riska Surya Ningrum, S.Si., M.Sc.,
	Jumlah Halaman: 232,
	Tanggal Terbit: 17 Sep 2018,
	ISBN: 9786020511887,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.3 kg,
	Lebar: 17 cm,
	Panjang: 25 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/super-mudahi-pahami-bahasa-indonesia-matematika-ipa-untuk-sdmi-kelas-4,
	title: Super Mudahi Pahami Bahasa Indonesia, Matematika, IPA Untuk SD/MI Kelas 4,
	author: Dewi Paramita Sari, S.Pd.,
	Jumlah Halaman: 224,
	Tanggal Terbit: 3 Sep 2018,
	ISBN: 9786020511634,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.290 kg,
	Lebar: 17 cm,
	Panjang: 25 cm,
},
{
	page: 26,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/ni-hao-mandarin-buku-pintar-baca-tulis-dengar-bicara,
	title: Ni Hao Mandarin ! Buku Pintar Baca Tulis Dengar & Bicara,
	author: Cheng Ting Yao,
	Jumlah Halaman: 308,
	Tanggal Terbit: 3 Apr 2017,
	ISBN: 9789792953312,
	Bahasa: Indonesia,
	Penerbit: Andi Offset,
	Berat: 0.3900 kg,
	Lebar: 15 cm,
	Panjang: 21 cm,
},
