{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/panduan-imunisasi-anak,
	title: Panduan Imunisasi Anak,
	author: Satgas Komunikasi Pp Idai (ikatan Dokter Anak Indonesia),
	Tanggal Terbit: 8 Sep 2014,
	ISBN: 9789800000000,
	Penerbit: Penerbit Buku Kompas,
	Berat: 0.3000 kg,
	Lebar: 0 cm,
	Panjang: 0 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-telomer-membalik-proses-penuaan,
	title: Telomer, Membalik Proses Penuaan,
	author: Prof. Dr. F.g. Winarno, Ir. Wida Winarno, M.si, A. Driando Ahnan-winarno, S.si,
	Jumlah Halaman: 258,
	Tanggal Terbit: 20 Agt 2015,
	ISBN: 9786020316413,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3290 kg,
	Lebar: 15 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/4-tepat-5-sempurna,
	title: 4 Tepat 5 Sempurna,
	author: Agam Ferry Erwana,
	Jumlah Halaman: 88,
	Tanggal Terbit: 26 Agt 2015,
	ISBN: 9789792950168,
	Bahasa: Indonesia,
	Penerbit: Andi Publisher,
	Berat: 0.2000 kg,
	Lebar: 15 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-agar-tulang-sehat,
	title: Agar Tulang Sehat,
	author: Pangkalan Ide,
	Jumlah Halaman: 292,
	Tanggal Terbit: 25 Sep 2013,
	ISBN: 978602002306,
	Penerbit: Elex Media Komputindo,
	Berat: 0.25 kg,
	Lebar: 14.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/hijrah-story-always-fashionable,
	title: Hijrah Story Always Fashionable,
	author: Irna Mutiara,
	Jumlah Halaman: 144,
	Tanggal Terbit: 27 Feb 2015,
	ISBN: 9789797575946,
	Bahasa: Indonesia,
	Penerbit: Kawan Pustaka,
	Berat: 0.3000 kg,
	Lebar: 14 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/asuhan-keperawatan-dengan-gangguan-sistem-muskuloskeletal,
	title: Asuhan Keperawatan Dengan Gangguan Sistem Muskuloskeletal,
	author: Abd Wahid,
	Jumlah Halaman: 84,
	Tanggal Terbit: 24 Nov 2015,
	ISBN: 9786022710042,
	Bahasa: Indonesia,
	Penerbit: Sagung Seto,
	Berat: 0.1500 kg,
	Lebar: 16 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-40-resep-kreatif-olahan-avokad-buah-eksotis-untuk-kesehatan-jantung,
	title: 40 Resep Kreatif Olahan: Avokad, Buah Eksotis untuk Kesehatan Jantung,
	author: Hindah Muaris,
	Jumlah Halaman: 82,
	Tanggal Terbit: 29 Mei 2013,
	ISBN: 9789792294576,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.2300 kg,
	Lebar: 23 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/step-by-step-50-resep-masakan-praktis-paling-laris-untuk-usaha-boga,
	title: Step By Step 50 Resep Masakan Praktis Paling Laris Untuk Usaha Boga,
	author: Chendawati,
	Jumlah Halaman: 108,
	Tanggal Terbit: 20 Apr 2015,
	ISBN: 9786020314419,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.1860 kg,
	Lebar: 18 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/menu-sebulan-anggaran-dibawah-45000-hari,
	title: Menu Sebulan Anggaran Dibawah 45000/Hari,
	author: Kunthi Sri,
	Jumlah Halaman: 100,
	Tanggal Terbit: 3 Agt 2015,
	ISBN: 9786030000000,
	Bahasa: Indonesia,
	Penerbit: Cable Book,
	Berat: 0.1730 kg,
	Lebar: 18 cm,
	Panjang: 0 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-225-resep-jus-super-peningkat-stamina-daya-tahan,
	title: 225 Resep Jus Super Peningkat Stamina & Daya Tahan,
	author: Tim Sarasvati,
	Jumlah Halaman: 289,
	Tanggal Terbit: 13 Agt 2014,
	ISBN: 9786020307251,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3000 kg,
	Lebar: 18 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/diet-sehat-golongan-darah-b-ed-revisi,
	title: Diet Sehat Golongan Darah B Ed.revisi,
	author: Peter J. Dadamo,
	Jumlah Halaman: 97,
	Tanggal Terbit: 19 Jun 2006,
	ISBN: 9789797982263,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.1500 kg,
	Lebar: 18 cm,
	Panjang: 0 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/diet-sehat-gol-darah-ab-ed-revisi,
	title: Diet Sehat Gol Darah Ab Ed.revisi,
	author: Adamo,
	Jumlah Halaman: 80,
	Tanggal Terbit: 12 Jul 2006,
	ISBN: 9789797982287,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.1500 kg,
	Lebar: 18 cm,
	Panjang: 0 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/50-yummy-mug-cake,
	title: 50 Yummy Mug Cake,
	author: Indrastuti Sindhoputro,
	Jumlah Halaman: 126,
	Tanggal Terbit: 5 Jun 2017,
	ISBN: 9786020356563,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3100 kg,
	Lebar: 15 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/handmade-wadah-antaran,
	title: Handmade Wadah Antaran,
	author: Andie Ran,
	Jumlah Halaman: 82,
	Tanggal Terbit: 30 Sep 2015,
	ISBN: XXX,
	Bahasa: Indonesia,
	Penerbit: Pt.magenta,
	Berat: 0.3000 kg,
	Lebar: 28 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-hidangan-lezat-nusantara,
	title: Best Collection - Hidangan Lezat: Nusantara,
	author: Tim Dapur Esensi,
	Jumlah Halaman: 472,
	Tanggal Terbit: 9 Feb 2016,
	ISBN: 9786027596733,
	Bahasa: Indonesia,
	Penerbit: Penerbit Erlangga,
	Berat: 0.3000 kg,
	Lebar: 19 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-obat-obat-penting-edisi-ke-enam,
	title: Obat-Obat Penting Edisi Ke Enam,
	author: Tan Hoan Tjay & Kirana Rahardja,
	Jumlah Halaman: 992,
	Tanggal Terbit: 11 Feb 2013,
	ISBN: 9789792719130,
	Penerbit: Elex Media Komputindo,
	Berat: 0.96 kg,
	Lebar: 15.0 cm,
	Panjang: 23.0 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-menghindari-dan-mengatasi-torch-toksoplasma-rubella-cmv-herpes,
	title: Menghindari dan Mengatasi TORCH (Toksoplasma Rubella CMV Herpes),
	author: Dr.angela Nusatia-abidin, Mars, Sp.mk,
	Jumlah Halaman: 112,
	Tanggal Terbit: 3 Feb 2014,
	ISBN: 978979910676,
	Penerbit: Kepustakaan Populer Gramedia,
	Berat: 0.11 kg,
	Lebar: 15.0 cm,
	Panjang: 20.0 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-penderita-diabetes-boleh-makan-apa-saja,
	title: Penderita Diabetes Boleh Makan Apa Saja,
	author: Hans Tandra,
	Jumlah Halaman: 168,
	Tanggal Terbit: 31 Jul 2013,
	ISBN: 9789792287066,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3000 kg,
	Lebar: 20 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-bahaya-terlambat-atasi-mikropenis-akan-menderita-seumur-hidup,
	title: Bahaya, Terlambat Atasi MIKROPENIS akan Menderita Seumur Hidup,
	author: Dr. Naek L Tobing,
	Jumlah Halaman: 232,
	Tanggal Terbit: 10 Feb 2016,
	ISBN: 9786020277851,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.3500 kg,
	Lebar: 14 cm,
},
{
	page: 34,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/conf-cookies-decorating-50-desain-cookies-natal,
	title: Cookies Decorating 50 DESAIN COOKIES NATAL,
	author: Etha Margaretha Trezise,
	Jumlah Halaman: 160,
	Tanggal Terbit: 13 Des 2016,
	ISBN: 9786020297620,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2100 kg,
	Lebar: 17 cm,
	Panjang: 22 cm,
},
