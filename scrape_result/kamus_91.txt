{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/kemurahan-hati,
	title: Kemurahan Hati,
	author: T. Krispurwana Cahyadi, Sj,
	Jumlah Halaman: 184,
	Tanggal Terbit: 27 Mei 2016,
	ISBN: 9789792146820,
	Bahasa: Indonesia,
	Penerbit: Kanisius,
	Berat: 0.1100 kg,
	Lebar: 12.5 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/english-dictionary-english-indo-indo-english,
	title: English Dictionary English-Indo-Indo English,
	author: Drs. Rudy Hariyono & Kemal,
	Jumlah Halaman: 576,
	Tanggal Terbit: 30 Jun 2015,
	ISBN: 9786021438299,
	Bahasa: Indonesia,
	Penerbit: Sinarsindo Utama,
	Berat: 0.7000 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/peruntungan-di-tahun-babi-tanah-imlek-2570-5-februari-2019-24-januari-2020,
	title: Peruntungan Di Tahun Babi Tanah Imlek 2570 5 Februari 2019 - 24 Januari 2020,
	author: Kang Hong Kian,
	Jumlah Halaman: 451,
	Tanggal Terbit: 10 Jan 2019,
	ISBN: 9786025849473,
	Bahasa: Indonesia,
	Penerbit: BUKU PINTAR INDONESIA,
	Berat: 0.596 kg,
	Lebar: 24 cm,
	Panjang: 19 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/metode-mudah-membaca-al-quran-iqro,
	title: Metode Mudah Membaca Al-Quran : Iqro,
	author: Tardmidzi Abdulrahhman,
	Jumlah Halaman: 132,
	Tanggal Terbit: 4 Mar 2016,
	ISBN: 9786023080366,
	Bahasa: Indonesia,
	Penerbit: Serambi,
	Berat: 0.1400 kg,
	Lebar: 15 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/slang-bahasa-inggris-di-dunia-maya,
	title: Slang Bahasa Inggris Di Dunia Maya,
	author: Latif Amrullah,
	Jumlah Halaman: 129,
	Tanggal Terbit: 22 Jan 2018,
	ISBN: 9789794209370,
	Bahasa: Indonesia,
	Penerbit: Gadjah Mada University Press,
	Berat: 0.200 kg,
	Lebar: 16 cm,
	Panjang: 23 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/asyiknya-belajar-grammar-dari-kisah-kisah-jenaka,
	title: Asyiknya Belajar Grammar Dari Kisah-Kisah Jenaka,
	author: Diyan Yulianto,
	Jumlah Halaman: 133,
	Tanggal Terbit: 25 Des 2017,
	ISBN: 9786023914913,
	Bahasa: Indonesia,
	Penerbit: Divapress,
	Berat: 0.175 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/kumpulan-kisah-tak-lazim,
	title: Kumpulan Kisah Tak Lazim,
	author: Sheng Yen Lu,
	Jumlah Halaman: 152,
	Tanggal Terbit: 21 Jul 2016,
	ISBN: 9786028877138,
	Bahasa: Indonesia,
	Penerbit: Budaya Daden Indonesia,
	Berat: 0.2000 kg,
	Lebar: 15 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/the-book-of-english-sructure-from-a-to-z,
	title: The Book of English Sructure from A to Z,
	author: Efendi Rahmat,
	Jumlah Halaman: 392,
	Tanggal Terbit: 10 Jul 2017,
	ISBN: 9786023759965,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.6000 kg,
	Lebar: 15 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/5-cahaya,
	title: 5 Cahaya,
	author: Irja Nasrullah,
	Jumlah Halaman: 220,
	Tanggal Terbit: 11 Agt 2016,
	ISBN: 9786020894324,
	Bahasa: Indonesia,
	Penerbit: Tinta Medina,
	Berat: 0.2000 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/jagad-batin-ibnu-arabi-menuju-manusia-dan-kewalian-paripurna,
	title: Jagad Batin Ibnu Arabi, Menuju Manusia Dan Kewalian Paripurna,
	author: Mahmud Mahmud Al-ghurab Dan Abu Abdullah Muhammad Bin Ali,
	Jumlah Halaman: 226,
	Tanggal Terbit: 31 Okt 2016,
	ISBN: 9786027481640,
	Bahasa: Indonesia,
	Penerbit: Solusi Distribusi,
	Berat: 1.0000 kg,
	Lebar: 15 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/inilah-sebabnya-wanita-banyak-masuk-neraka,
	title: Inilah Sebabnya Wanita Banyak Masuk Neraka,
	author: Khalifi Elyas Bahar,
	Jumlah Halaman: 188,
	Tanggal Terbit: 16 Des 2015,
	ISBN: 9786020806747,
	Bahasa: Indonesia,
	Penerbit: Buku Kita,
	Berat: 0.2000 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/soul-keeping-menjaga-jiwa,
	title: Soul Keeping ( Menjaga Jiwa ),
	author: Dian Pradana,
	Jumlah Halaman: 220,
	Tanggal Terbit: 28 Jan 2016,
	ISBN: 9786021302217,
	Bahasa: Indonesia,
	Penerbit: Spirit Grafindo,
	Berat: 0.2000 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/anda-luar-biasa,
	title: Anda Luar Biasa,
	author: Xavier Quentin Pranata,
	Jumlah Halaman: 212,
	Tanggal Terbit: 4 Mei 2016,
	ISBN: 9786027238367,
	Bahasa: Indonesia,
	Penerbit: Spirit Grafindo,
	Berat: 0.1800 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/sembuh-total-dengan-wirid-asmaul-husna,
	title: Sembuh Total Dengan Wirid Asmaul Husna,
	author: Rizem Aizid,
	Jumlah Halaman: 212,
	Tanggal Terbit: 26 Jul 2016,
	ISBN: 9786020806976,
	Bahasa: Indonesia,
	Penerbit: Sabil,
	Berat: 0.2500 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/tafsir-keluarga-menjadi-bahagia-di-dunia-di-surga,
	title: Tafsir Keluarga : Menjadi Bahagia di Dunia & di Surga,
	author: Ahmad Kusyairi Suhail,
	Jumlah Halaman: 276,
	Tanggal Terbit: 21 Jul 2016,
	ISBN: 9786028399326,
	Bahasa: Indonesia,
	Penerbit: Suka Buku,
	Berat: 0.2500 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/air-mata-muslimah,
	title: Air Mata Muslimah,
	author: Nur Zaim,
	Jumlah Halaman: 220,
	Tanggal Terbit: 12 Jan 2018,
	ISBN: 9786024073060,
	Bahasa: Indonesia,
	Penerbit: Laksana,
	Berat: 0.120 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/kamus-bahasa-indonesia,
	title: Kamus Bahasa Indonesia,
	author: A.a. Waskito,
	Jumlah Halaman: 594,
	Tanggal Terbit: 23 Sep 2016,
	ISBN: 9786023780969,
	Bahasa: Indonesia,
	Penerbit: Wahyu Media,
	Berat: 0.3500 kg,
	Lebar: 11 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/conf-lancar-berbahasa-korea-dengan-101-kata-kunci,
	title: Lancar Berbahasa Korea dengan 101 Kata Kunci,
	author: Euis Sulastri,
	Jumlah Halaman: 288,
	Tanggal Terbit: 8 Mei 2017,
	ISBN: 9786023758982,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.2000 kg,
	Lebar: 13.5 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/percakapan-dasar-bahasa-jepang-sehari-hari-super-mudah,
	title: Percakapan Dasar Bahasa Jepang Sehari-Hari Super Mudah,
	author: Doni Judian,
	Jumlah Halaman: 236,
	Tanggal Terbit: 1 Jan 2001,
	ISBN: 9786027485235,
	Bahasa: Indonesia,
	Penerbit: Pusat Kajian Bahasa,
	Berat: 0.2 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/my-trip-in-holy-land,
	title: My Trip In Holy Land,
	author: Christina Wirasasti,
	Jumlah Halaman: 338,
	Tanggal Terbit: 20 Apr 2016,
	ISBN: 9789792956726,
	Bahasa: Indonesia,
	Penerbit: Andi Offset,
	Berat: 0.3000 kg,
	Lebar: 15 cm,
},
