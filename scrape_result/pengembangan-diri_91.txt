{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/super-trik-sukses-dan-kaya-raya-ala-jack-ma,
	title: Super Trik Sukses Dan Kaya Raya Ala Jack Ma,
	author: Lauma Kiwe,
	Jumlah Halaman: 148.0,
	Tanggal Terbit: 12 Des 2019,
	ISBN: 9786025945335,
	Bahasa: Indonesia,
	Penerbit: Scritto Books,
	Berat: 0.162 kg,
	Lebar: 14.0 cm,
	Panjang: 20.0 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/bekerja-dengan-cinta-uang-pun-ikut-serta,
	title: Bekerja Dengan Cinta, Uang Pun Ikut Serta,
	author: John Afifi,
	Jumlah Halaman: 172,
	Tanggal Terbit: 11 Feb 2016,
	ISBN: 9786022961888,
	Bahasa: Indonesia,
	Penerbit: Laksana,
	Berat: 0.2000 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/secercah-cahaya-di-langit-sukamiskin,
	title: Secercah Cahaya Di Langit Sukamiskin,
	author: Tim Scls  Warna Sukamiskin,
	Jumlah Halaman: 440,
	Tanggal Terbit: 1 Jul 2017,
	ISBN: 9789790558304,
	Bahasa: Indonesia,
	Penerbit: Sygma Creative Media Corp. (Syaamil Books),
	Berat: 0.37 kg,
	Lebar: 13.5 cm,
	Panjang: 20.5 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/seni-memengaruhi-orang,
	title: Seni Memengaruhi Orang,
	author: Brian Tracy,
	Jumlah Halaman: 440,
	Tanggal Terbit: 24 Jun 2019,
	ISBN: 9786232161245,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.250 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/fight-like-a-tiger-win-like-achampion-8-kekuatan,
	title: Fight Like A Tiger Win Like Achampion: 8 Kekuatan,
	author: Darmadi Darmawan,
	Tanggal Terbit: 1 Jan 2006,
	ISBN: 9789792092196,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.3000 kg,
	Lebar: 23 cm,
	Panjang: 0 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/conf-secangkir-kopi-pahit,
	title: Secangkir Kopi Pahit,
	author: Dra.v. Dwiyani,
	Jumlah Halaman: 128,
	Tanggal Terbit: 25 Jan 2016,
	ISBN: 9786020279169,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.3000 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/bahagia-dalam-tiada-hidup-bersama-anak-anak-yatim,
	title: Bahagia dalam Tiada: Hidup Bersama Anak-Anak Yatim,
	author: Jonih Rahmat,
	Jumlah Halaman: 256,
	Tanggal Terbit: 21 Agt 2017,
	ISBN: 9786020346465,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.1800 kg,
	Lebar: 13.5 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/semua-bisa-bahagia-1,
	title: Semua Bisa Bahagia,
	author: Mahdi Elmosawi,
	Jumlah Halaman: 228.0,
	Tanggal Terbit: 16 Jan 2020,
	ISBN: 9786025547676,
	Bahasa: Indonesia,
	Penerbit: Qaf Media Kreativa,
	Berat: 0.196 kg,
	Lebar: 13.0 cm,
	Panjang: 20.5 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/kisah-inspiratif-orang2-sukses-melawan-keterbatasan,
	title: Kisah Inspiratif Orang2 Sukses Melawan Keterbatasan,
	author: Riskiana Sulaiman,
	Jumlah Halaman: 0,
	Tanggal Terbit: 24 Jul 2015,
	ISBN: 9786022882046,
	Bahasa: Indonesia,
	Penerbit: Notebook,
	Berat: 0.3000 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/the-untold-story-kick-andy,
	title: Rahasia Di Balik Layar Kick Andy,
	author: Kick Andy,
	Jumlah Halaman: 426,
	Tanggal Terbit: 27 Jul 2017,
	ISBN: 9786021036587,
	Bahasa: Indonesia,
	Penerbit: Transmedia,
	Berat: 0.37 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/mimpi-usaha-berbuah-manis,
	title: Mimpi & Usaha Berbuah Manis,
	author: Sally Goivanny,
	Jumlah Halaman: 300,
	Tanggal Terbit: 24 Mei 2016,
	ISBN: 9786027409057,
	Bahasa: Indonesia,
	Penerbit: Tim Jurnalis,
	Berat: 0.2800 kg,
	Lebar: 14 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/super-trik-sukses-kaya-ala-mark-zuckerberg,
	title: Super Trik Sukses & Kaya Ala Mark Zuckerberg,
	author: Desi Setianingsih,
	Jumlah Halaman: 284.0,
	Tanggal Terbit: 12 Des 2019,
	ISBN: 9786025945359,
	Bahasa: Indonesia,
	Penerbit: Scritto Books,
	Berat: 0.243 kg,
	Lebar: 14.0 cm,
	Panjang: 20.0 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/conf-mandor-model-kepemimpinan-tradisional-jawa-pada-proyek-konstruksi-era-modern,
	title: Mandor, Model Kepemimpinan Tradisional Jawa Pada Proyek Konstruksi Era Modern,
	author: Dr. Kukuh Lukiyanto, S.t., M.m., M.t.,
	Jumlah Halaman: 190,
	Tanggal Terbit: 6 Jan 2017,
	ISBN: 9786020326634,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.5400 kg,
	Lebar: 15 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/jiwa-jiwa-bermesin,
	title: Jiwa-Jiwa Bermesin,
	author: PETRUS HARIYANTO,
	Jumlah Halaman: 186.0,
	Tanggal Terbit: 8 Nov 2019,
	ISBN: 9786020763460,
	Bahasa: Indonesia,
	Penerbit: Istana Media,
	Berat: 0.173 kg,
	Lebar: 20.0 cm,
	Panjang: 14.0 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/little-book-of-bad-moods-sc,
	title: Little Book Of Bad Moods,
	author: Lotta Sonninen,
	Jumlah Halaman: 112,
	Tanggal Terbit: 15 Nov 2018,
	ISBN: 9781526609892,
	Bahasa: English,
	Penerbit: Bloomsbury,
	Berat: 0.12 kg,
	Lebar: 13 cm,
	Panjang: 18 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/rahasia-sukses-sepanjang-zaman,
	title: Rahasia Sukses Sepanjang Zaman,
	author: -,
	Jumlah Halaman: 484,
	Tanggal Terbit: 15 Feb 2018,
	ISBN: 9786026117342,
	Bahasa: Indonesia,
	Penerbit: Alvabet,
	Berat: 0.39 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/25-lessons-in-25-years,
	title: 25 Lessons in 25 Years,
	author: Agnes Lina Purwaningsih,
	Jumlah Halaman: 168,
	Tanggal Terbit: 1 Okt 2018,
	ISBN: 9786020614113,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.2 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/aherlicious,
	title: Aherlicious,
	author: Indraku Dkk,
	Jumlah Halaman: 190,
	Tanggal Terbit: 13 Okt 2015,
	ISBN: 9786027216396,
	Bahasa: Indonesia,
	Penerbit: Duta Media Tama,
	Berat: 0.2100 kg,
	Lebar: 15 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/10-ways-to-stay-positive,
	title: 10 Ways To Stay Positive,
	author: Alviana Cahyanti,
	Jumlah Halaman: 162,
	Tanggal Terbit: 7 Okt 2017,
	ISBN: 9786025448034,
	Bahasa: Indonesia,
	Penerbit: Cemerlang Publishing,
	Berat: 0.195 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 91,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/mi-saja-yang-instan-usaha-jangan,
	title: Mi Saja Yang Instan, Usaha Jangan,
	author: De Simanjuntak,
	Jumlah Halaman: 240.0,
	Tanggal Terbit: 12 Des 2019,
	ISBN: 9786237345107,
	Bahasa: Indonesia,
	Penerbit: Andaliman Books,
	Berat: 0.193 kg,
	Lebar: 14.0 cm,
	Panjang: 20.0 cm,
},
