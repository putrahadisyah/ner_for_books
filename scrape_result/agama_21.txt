{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/tuhan-maaf-kami-sedang-sibuk-edisi-new-cover,
	title: Tuhan, Maaf Kami Sedang Sibuk,
	author: Ahmad Rifai Rifan,
	Jumlah Halaman: 384,
	Tanggal Terbit: 26 Jan 2015,
	ISBN: 9786020256665,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.4810 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/maryam-wanita-suci-yang-berdiam-di-surga,
	title: MARYAM Wanita Suci Yang Berdiam Di Surga,
	author: Immawati Fitri Lestari,
	Jumlah Halaman: 136,
	Tanggal Terbit: 12 Feb 2019,
	ISBN: 9786025713590,
	Bahasa: Indonesia,
	Penerbit: Risalah Zaman,
	Berat: 0.2 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/kitab-tuntunan-shalat-lengkap-wajib-sunnah,
	title: Kitab Tuntunan Shalat Lengkap Wajib & Sunnah,
	author: Khalifa Zain Nasrullah,
	Jumlah Halaman: 372,
	Tanggal Terbit: 29 Nov 2017,
	ISBN: 9789798783760,
	Bahasa: Indonesia,
	Penerbit: Mutiara Media,
	Berat: 0.31 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/fiqih-for-kids-gampang-dimengerti-mudah-dijalankan,
	title: Fiqih For Kids: Gampang Dimengerti Mudah Dijalankan,
	author: Meti Herawati,
	Jumlah Halaman: 176,
	Tanggal Terbit: 5 Des 2016,
	ISBN: 9786020333434,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.2800 kg,
	Lebar: 15 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/kitab-komik-filsuf-muslim,
	title: Kitab Komik Filsuf Muslim,
	author: Ibod,
	Jumlah Halaman: 160,
	Tanggal Terbit: 23 Apr 2018,
	ISBN: 9786024248567,
	Bahasa: Indonesia,
	Penerbit: Kepustakaan Populer Gramedia,
	Berat: 0.200 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/la-tahzan-untuk-bunda-yang-bekerja,
	title: La Tahzan Untuk Bunda Yang Bekerja,
	author: Riyadhus Shalihin Emka,
	Jumlah Halaman: 192,
	Tanggal Terbit: 30 Apr 2018,
	ISBN: 9786025805004,
	Bahasa: Indonesia,
	Penerbit: Araska Publisher,
	Berat: 0.20 kg,
	Lebar: 14 cm,
	Panjang: 20.5 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/alquran-cordoba-a5-al-haramain-new-edition,
	title: Alquran Cordoba A5 Al Haramain (New Edition),
	author: Cordoba International Indonesia,
	Jumlah Halaman: 629,
	Tanggal Terbit: 23 Apr 2018,
	ISBN: 9786027272118,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.559 kg,
	Lebar: 15 cm,
	Panjang: 21.5 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/madrasah-nabawiyah-mengikuti-cara-rasulullah-saw-membesarkan,
	title: Madrasah Nabawiyah,
	author: Iqro Al Firdaus,
	Jumlah Halaman: 198,
	Tanggal Terbit: 11 Apr 2019,
	ISBN: 9786025781209,
	Bahasa: Indonesia,
	Penerbit: Penerbit Noktah,
	Berat: 0.2 kg,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/ada-surga-di-dekatmu,
	title: Ada Surga Di Dekatmu,
	author: Ust Saiful Hadi El-sutha,
	Jumlah Halaman: 291,
	Tanggal Terbit: 9 Feb 2018,
	ISBN: 9786026358097,
	Bahasa: Indonesia,
	Penerbit: Wahyu Qolbu,
	Berat: 0.26 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/islam-berdialog-dengan-zaman,
	title: Islam Berdialog dengan Zaman,
	author: Ardiyansyah,
	Jumlah Halaman: 264,
	Tanggal Terbit: 9 Jul 2018,
	ISBN: 9786020476292,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.35 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/1500-hadis-dan-sunah-pilihan,
	title: 1500++ Hadis Dan Sunah Pilihan,
	author: Syamsul Rizal Hamid,
	Jumlah Halaman: 536,
	Tanggal Terbit: 31 Jan 2017,
	ISBN: 9786022150060,
	Bahasa: Indonesia,
	Penerbit: Kaysa Media,
	Berat: 0.5840 kg,
	Lebar: 14 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/cara-menjadi-kekasih-allah-dan-rasulnya,
	title: Cara Menjadi Kekasih Allah Dan Rasulnya,
	author: Syekh Nawawi Al Bantani,
	Jumlah Halaman: 428,
	Tanggal Terbit: 16 Jan 2018,
	ISBN: 9786026673367,
	Bahasa: Indonesia,
	Penerbit: Mueeza,
	Berat: 0.445 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/paris-a-love-journey,
	title: Paris a Love Journey,
	author: Susanti Dominguez,
	Jumlah Halaman: 356,
	Tanggal Terbit: 25 Mei 2018,
	ISBN: 9786029251425,
	Bahasa: Indonesia,
	Penerbit: Tiga Serangkai,
	Berat: 0.30 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/dasar-dasar-hermeneutik-metode-penafsiran-alkitab-yang-muda,
	title: Dasar-Dasar Hermeneutik, Metode Penafsiran Alkitab Yang Mudah dan Tepat,
	author: Kresbinol Labobar,
	Jumlah Halaman: 124,
	Tanggal Terbit: 22 Nov 2017,
	ISBN: 9789792964257,
	Penerbit: Andi Offset,
	Berat: 0.155 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/66-doa-anak-muslim,
	title: 66 Doa Anak Muslim,
	author: Kak Hasna & Kak Abdul,
	Jumlah Halaman: 66,
	Tanggal Terbit: 26 Des 2018,
	ISBN: 9786026673190,
	Bahasa: Indonesia,
	Penerbit: Playground,
	Berat: 0.230 kg,
	Lebar: 12 cm,
	Panjang: 18 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/biografi-60-sahabat-rasulullah,
	title: Biografi 60 Sahabat Rasulullah,
	author: Khalid Muhammad Khalid,
	Jumlah Halaman: 424,
	Tanggal Terbit: 24 Mar 2015,
	ISBN: 9789791303736,
	Bahasa: Indonesia,
	Penerbit: Qisthi Press,
	Berat: 0.3 kg,
	Lebar: 15.5 cm,
	Panjang: 24 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/be-moslem-scientist-juz-2,
	title: Be Moslem Scientist - Juz 2,
	author: MUSLIM IQBAL ROMADHONI, M. PD & IIS HARYATI, M.PD,
	Jumlah Halaman: 192,
	Tanggal Terbit: 26 Des 2018,
	ISBN: 9786020485607,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.29 kg,
	Lebar: 21 cm,
	Panjang: 14 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/yahudi-tuhan-dan-sejarah,
	title: Yahudi Tuhan Dan Sejarah,
	author: Max Isaac Dimont,
	Jumlah Halaman: 612,
	Tanggal Terbit: 22 Feb 2018,
	ISBN: 9786027696419,
	Bahasa: Indonesia,
	Penerbit: Ircisod,
	Berat: 0.81 kg,
	Lebar: 16 cm,
	Panjang: 24 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/10-tema-fenomenal-dalam-ilmu-al-quran,
	title: 10 Tema Fenomenal dalam Ilmu Al-Qur'an,
	author: Mochammad Arifin,
	Jumlah Halaman: 504,
	Tanggal Terbit: 1 Apr 2019,
	ISBN: 9786020495590,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.8 kg,
	Lebar: 23 cm,
	Panjang: 15 cm,
},
{
	page: 21,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/sempurna-separuh,
	title: Sempurna Separuh,
	author: Rizqi Awal,
	Jumlah Halaman: 138,
	Tanggal Terbit: 1 Jul 2018,
	ISBN: 9786026358523,
	Bahasa: Indonesia,
	Penerbit: Wahyuqolbu,
	Berat: 0.200 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
