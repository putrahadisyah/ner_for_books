{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/sainspedia,
	title: Sainspedia,
	author: Patricia Daniels Dkk,
	Jumlah Halaman: 304,
	Tanggal Terbit: 12 Jun 2017,
	ISBN: 9786024243722,
	Bahasa: Indonesia,
	Penerbit: Kepustakaan Populer Gramedia,
	Berat: 0.450 kg,
	Lebar: 18 cm,
	Panjang: 24 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/everyday-english-edisi-revisi-1,
	title: Everyday English Edisi Revisi,
	author: Evelyn Rientje,
	Jumlah Halaman: 360,
	Tanggal Terbit: 29 Nov 2013,
	ISBN: 100025317,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.25 kg,
	Lebar: 11 cm,
	Panjang: 14.5 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/bahasa-rusia-sehari-hari,
	title: Bahasa Rusia Sehari-Hari,
	author: Nanang S Fadillah,
	Tanggal Terbit: 18 Jul 2013,
	Penerbit: Kesaint Blanc,
	Berat: 0.2000 kg,
	Lebar: 20 cm,
	Panjang: 0 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/conf-melihat-indonesia,
	title: Melihat Indonesia,
	author: Kompas,
	Tanggal Terbit: 12 Des 2015,
	ISBN: 9789797097868,
	Penerbit: Kompas Penerbit Buku,
	Berat: 0.3000 kg,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/kamus-bahasa-inggris-superlengkap-inggris-indonesia-indonesia-inggris,
	title: Kamus Bahasa Inggris Superlengkap Inggris-Indonesia Indonesia-Inggris,
	author: Ireneus Leo Arswendo,
	Jumlah Halaman: 516,
	Tanggal Terbit: 3 Okt 2017,
	ISBN: 9786026178008,
	Penerbit: Scritto Books Publisher,
	Berat: 0.3 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/wanita-wanita-teladan-di-zaman-rasulullah,
	title: Wanita-Wanita Teladan Di Zaman Rasulullah,
	author: Desita Ulla R,
	Jumlah Halaman: 264,
	Tanggal Terbit: 30 Jan 2018,
	ISBN: 9786025469190,
	Bahasa: Indonesia,
	Penerbit: Mueeza,
	Berat: 0.270 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/kamus-bahasa-jerman-excellent-dictionary,
	title: Kamus Bahasa Jerman: Excellent Dictionary,
	author: Erward Pamungkas,
	Jumlah Halaman: 448,
	Tanggal Terbit: 6 Des 2017,
	ISBN: 9786025006944,
	Penerbit: Cemerlang Publishing,
	Berat: 0.3 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/kamus-arab-indonesia-indonesia-arab,
	title: Kamus Arab - Indonesia:Indonesia - Arab,
	author: Wahab Hasbullah,
	Jumlah Halaman: 580,
	Tanggal Terbit: 16 Des 2016,
	ISBN: 9789797752958,
	Bahasa: Indonesia,
	Penerbit: Indonesia Tera,
	Berat: 0.30 kg,
	Lebar: 10 cm,
	Panjang: 17 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/simple-master-crucial-words-in-english-language,
	title: Simple Master Crucial Words In English Language,
	author: Cecilia Larasati,
	Jumlah Halaman: 528,
	Tanggal Terbit: 1 Jan 2018,
	ISBN: 9786025134784,
	Bahasa: Indonesia,
	Penerbit: Scritto Books,
	Berat: 0.3 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/bahasa-prancis-sisteem-52m-vol-1-cd,
	title: Bahasa Prancis Sisteem 52m Vol 1+Cd,
	author: Emy Dwi Lestari,
	Jumlah Halaman: 248,
	Tanggal Terbit: 18 Nov 2014,
	ISBN: 9789795936992,
	Bahasa: Indonesia,
	Penerbit: Kesaint Blanc,
	Berat: 0.32 kg,
	Lebar: 12 cm,
	Panjang: 20 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/master-ebi-ejaan-bahasa-indonesia-dilengkapi-dgn-homofon,
	title: Master EBI Ejaan Bahasa Indonesia: Dilengkapi dengan Homofon, Homograf dan Homonim,
	author: Weni Rahayu,
	Jumlah Halaman: 152,
	Tanggal Terbit: 24 Jan 2018,
	ISBN: 9786025067433,
	Bahasa: Indonesia,
	Penerbit: Syalmahat Publishing,
	Berat: 0.18 kg,
	Lebar: 14 cm,
	Panjang: 20.5 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/super-fun-cross-words-30-hari-jago-bahasa-inggris-dengan-m,
	title: Super Fun Cross Words : 30 Hari Jago Bahasa Inggris,
	author: Ardika Riski Rahmawan,
	Jumlah Halaman: 144,
	Tanggal Terbit: 1 Okt 2017,
	ISBN: 9786021659793,
	Bahasa: Indonesia,
	Penerbit: Scritto Books Publisher,
	Berat: 0.2 kg,
	Lebar: 17 cm,
	Panjang: 25 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/percakapan-bahasa-korea-simple-fun-and-easy,
	title: Percakapan Bahasa Korea: Simple Fun and Easy,
	author: Bright Learning Center,
	Jumlah Halaman: 290,
	Tanggal Terbit: 17 Jan 2018,
	ISBN: 9786026657695,
	Bahasa: Indonesia,
	Penerbit: Bright Publisher,
	Berat: 0.212 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/journey-to-andalusia-jelajah-3-daulah,
	title: Journey To Andalusia : Jelajah 3 Daulah,
	author: Marfuah Panji Astuti,
	Jumlah Halaman: 192.0,
	Tanggal Terbit: 9 Jan 2017,
	ISBN: 9786023943913,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.13 kg,
	Lebar: 13.0 cm,
	Panjang: 20.0 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/conf-sejarah-dan-teori-sosial,
	title: Sejarah dan Teori Sosial,
	author: Peter Burke,
	Jumlah Halaman: 326,
	Tanggal Terbit: 14 Jun 2016,
	ISBN: 9789794619827,
	Bahasa: Indonesia,
	Penerbit: Yayasan Pustaka Obor Indonesia,
	Berat: 0.3500 kg,
	Lebar: 15 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/1-jam-kuasai-16-tenses-bahasa-inggris,
	title: 1 Jam Kuasai 16 Tenses Bahasa Inggris,
	author: Mustika Putri,
	Jumlah Halaman: 320,
	Tanggal Terbit: 24 Okt 2017,
	ISBN: 9786025469428,
	Bahasa: Indonesia,
	Penerbit: Pusat Kajian Bahasa,
	Berat: 0.3 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/kitab-puebi-pedoman-umum-ejaan-bahasa-indonesia,
	title: Kitab Puebi : Pedoman Umum Ejaan Bahasa Indonesia,
	author: Eko Sugiarto,
	Jumlah Halaman: 422,
	Tanggal Terbit: 6 Mei 2017,
	ISBN: 9789792961409,
	Bahasa: Indonesia,
	Penerbit: Andi Publisher,
	Berat: 0.56 kg,
	Lebar: 16 cm,
	Panjang: 23 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/praktis-bahasa-inggris-pintar-percakapan-materi-lkp-eng-conv,
	title: Praktis Bahasa Inggris Pintar Percakapan Materi Lengkap English Conversation,
	author: Dita Pertiwi,
	Jumlah Halaman: 382,
	Tanggal Terbit: 1 Jan 2009,
	ISBN: 9786026237149,
	Bahasa: Indonesia,
	Penerbit: Kartika,
	Berat: 0.185 kg,
	Lebar: 11.7 cm,
	Panjang: 15.2 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/student-pocket-dictionary-inggris-indonesia-super-lengkap,
	title: Student Pocket Dictionary Inggris-Indonesia Super Lengkap,
	author: Yukha Minna,
	Jumlah Halaman: 429,
	Tanggal Terbit: 27 Apr 2017,
	ISBN: 9786027470057,
	Bahasa: Indonesia,
	Penerbit: Bmedia,
	Berat: 0.2450 kg,
	Lebar: 12 cm,
	Panjang: 18 cm,
},
{
	page: 23,
	kategori 1: non-fiksi,
	kategori 2: kamus,
	book_url: https://www.gramedia.com/products/maariful-auliya,
	title: Ma'Ariful Auliya,
	author: Muhammad Khalid Tsabit,
	Jumlah Halaman: 388,
	Tanggal Terbit: 1 Feb 2018,
	ISBN: 9786025547126,
	Bahasa: Indonesia,
	Penerbit: Qaf Media Kreativa,
	Berat: 0.405 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
