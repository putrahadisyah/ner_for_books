{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/30-renungan-tentang-hubungan-dengan-tuhan,
	title: 30 Renungan Tentang Hubungan Dengan Tuhan,
	author: Imelda Saputra,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/do-it-now-kenalilah-dirimu-galilah-potensimu-dan-bangunlah-prestasimu,
	title: DO IT NOW!!!! Kenalilah Dirimu, Galilah Potensimu, dan Bangunlah Prestasimu,
	author: Dr. Sopan Adrianto, SE, M.Pd.,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/pembaruan-hukum-islam-di-indonesia-telaah-kompilasi-hukum,
	title: PEMBARUAN HUKUM ISLAM DI INDONESIA: Telaah Kompilasi Hukum,
	author: Dr. H.a. Malthuf Siroj, M. Ag,
},
