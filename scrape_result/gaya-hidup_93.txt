{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/kreasi-janur-cantik,
	title: Kreasi Janur Cantik,
	author: Diah Iswari,
	Jumlah Halaman: 79,
	Tanggal Terbit: 23 Jan 2018,
	ISBN: 9786022130543,
	Bahasa: Indonesia,
	Penerbit: Kriya Pustaka,
	Berat: 0.14 kg,
	Lebar: 19 cm,
	Panjang: 20 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/sim-salabim-dari-tepung-magic-tercipta-25-sajian-lezat,
	title: Sim Salabim! Dari Tepung Magic Tercipta 25 Sajian Lezat,
	author: Kamikoki,
	Jumlah Halaman: 64,
	Tanggal Terbit: 10 Jul 2017,
	ISBN: 9786020361291,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.2800 kg,
	Lebar: 15 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/keajaiban-ikan-gabus,
	title: Keajaiban Ikan Gabus,
	author: Nurpudji Astuti Daud,
	Jumlah Halaman: 160,
	Tanggal Terbit: 1 Mar 2019,
	ISBN: 9786025192432,
	Bahasa: Indonesia,
	Penerbit: Kamboja Kelopak Enam,
	Berat: 0.3 kg,
	Lebar: 5 cm,
	Panjang: 23 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/wanita-hormon,
	title: Wanita & Hormon,
	author: Ym Dato Seri Paduka Prof. Dr. Noor Zaman Khan MD.Ph.D.,
	Jumlah Halaman: 186.0,
	Tanggal Terbit: 18 Nov 2019,
	ISBN: 9786237239413,
	Bahasa: Indonesia,
	Penerbit: Andi Offset,
	Berat: 0.271 kg,
	Lebar: 16.0 cm,
	Panjang: 23.0 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/tanaman-ajaib-basmi-penyakit-dengan-toga-tanaman-obat-keluarga,
	title: Tanaman Ajaib! Basmi Penyakit Dengan Toga (Tanaman Obat Keluarga),
	author: Astrid Savitri,
	Jumlah Halaman: 192,
	Tanggal Terbit: 18 Feb 2016,
	ISBN: 9786026805232,
	Bahasa: Indonesia,
	Penerbit: Bibit Publisher,
	Berat: 0.3500 kg,
	Lebar: 18 cm,
	Panjang: 24 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/pastry-preneur,
	title: Pastry Preneur,
	author: M Husin Syarbini,
	Jumlah Halaman: 110,
	Tanggal Terbit: 21 Nov 2016,
	ISBN: 9786026328151,
	Bahasa: Indonesia,
	Penerbit: Tiga Serangkai,
	Berat: 0.2000 kg,
	Lebar: 20 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/30-desain-rumah-2-lantai-masa-kini,
	title: 30 Desain Rumah 2 Lantai Masa Kini,
	author: Djoko Wiratno,
	Jumlah Halaman: 140,
	Tanggal Terbit: 2 Sep 2015,
	ISBN: 9786023170289,
	Bahasa: Indonesia,
	Penerbit: Visi Mandiri,
	Berat: 0.3 kg,
	Lebar: 18 cm,
	Panjang: 22 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/kue-kering-hari-raya-65-resep-kukis-istimewa,
	title: Kue Kering Hari Raya: 65 Resep Kukis Istimewa,
	author: Veronica Dhani,
	Jumlah Halaman: 148,
	Tanggal Terbit: 20 Mei 2019,
	ISBN: 9786020630182,
	Bahasa: Indonesia,
	Penerbit: GRAMEDIA PUSTAKA UTAMA,
	Berat: 0.2 kg,
	Lebar: 17 cm,
	Panjang: 24 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/pijat-refleksi-untuk-wanita,
	title: Pijat Refleksi Untuk Wanita,
	author: Indarniati,
	Jumlah Halaman: 113,
	Tanggal Terbit: 6 Feb 2018,
	ISBN: 9786027602502,
	Bahasa: Indonesia,
	Penerbit: Dunia Sehat,
	Berat: 0.155 kg,
	Lebar: 17 cm,
	Panjang: 24 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/danielle-walker-eat-what-you-love-hc,
	title: Danielle Walker Eat What You Love,
	author: Danielle Walker,
	Jumlah Halaman: 336,
	Tanggal Terbit: 1 Nov 2018,
	ISBN: 9781607749448,
	Bahasa: English,
	Penerbit: Mitra Pelita Internusa,
	Berat: 1.2 kg,
	Lebar: 20.0 cm,
	Panjang: 25.0 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/home-barista-sc,
	title: Home Barista,
	author: Simone Egger,
	Jumlah Halaman: 192,
	Tanggal Terbit: 25 Agt 2015,
	ISBN: 9781615192922,
	Bahasa: Indonesia,
	Penerbit: Workman,
	Berat: 0.06 kg,
	Lebar: 12 cm,
	Panjang: 12 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/yakin-dengan-vaksin-dan-imunisasi,
	title: Yakin Dengan Vaksin Dan Imunisasi?,
	author: dr. Arifianto, Sp.A,
	Jumlah Halaman: 340.0,
	Tanggal Terbit: 5 Des 2019,
	ISBN: 9786237567042,
	Bahasa: Indonesia,
	Penerbit: Penerbit Katadepan,
	Berat: 0.286 kg,
	Lebar: 14.0 cm,
	Panjang: 20.0 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/ilmiah-populer-pangan-gizi-kesehatan,
	title: Ilmiah Populer : Pangan, Gizi & Kesehatan,
	author: Prof. Dr. Ir. Deddy Muchtadi, M.S.,
	Jumlah Halaman: 222,
	Tanggal Terbit: 7 Des 2015,
	ISBN: 9786022891727,
	Bahasa: Indonesia,
	Penerbit: Alfabeta,
	Berat: 0.3000 kg,
	Lebar: 16 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/olahan-brokoli-untuk-bayi-anak2,
	title: Olahan Brokoli Untuk Bayi & Anak2,
	author: Inti Krisnawati,
	Jumlah Halaman: 88,
	Tanggal Terbit: 25 Jul 2016,
	ISBN: 9786020332628,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.1000 kg,
	Lebar: 14 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/mainan-rajut-edukatif,
	title: Mainan Rajut Edukatif,
	author: Denik Ristya Rini,
	Jumlah Halaman: 64,
	Tanggal Terbit: 17 Nov 2015,
	ISBN: 9786021656327,
	Penerbit: Tiara Aksa,
	Berat: 0.1500 kg,
	Lebar: 17 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/21-resep-kue-tradisonal-lezat-populer,
	title: 21 Resep Kue Tradisonal Lezat Populer,
	author: Saji,
	Jumlah Halaman: 50,
	Tanggal Terbit: 5 Feb 2016,
	ISBN: 9789792364996,
	Bahasa: Indonesia,
	Penerbit: Samindra Utama,
	Berat: 0.1 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/nutrient-power-memulihkan-kesehatan-mental,
	title: Nutrient Power: Memulihkan Kesehatan Mental,
	author: William J Walsh,
	Jumlah Halaman: 259,
	Tanggal Terbit: 1 Agt 2016,
	ISBN: 9780000009784,
	Bahasa: Indonesia,
	Penerbit: Jejak Benang Emas,
	Berat: 0.3 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/rahasia-kedahsyatan-fungsi-seksual,
	title: Rahasia Kedahsyatan Fungsi Seksual,
	author: Adi D. Tilong,
	Jumlah Halaman: 172,
	Tanggal Terbit: 10 Agt 2015,
	ISBN: 9786022558859,
	Bahasa: Indonesia,
	Penerbit: Flash Books,
	Berat: 0.2000 kg,
	Lebar: 14 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/primarasa-50-resep-andalan-puasa-lebaran,
	title: Primarasa 50+ Resep Andalan: Puasa & Lebaran,
	author: Femina Media,
	Jumlah Halaman: 95,
	Tanggal Terbit: 22 Mei 2017,
	ISBN: 9789795156635,
	Bahasa: Indonesia,
	Penerbit: Gaya Favorit Press,
	Berat: 0.17 kg,
	Lebar: 17 cm,
	Panjang: 20 cm,
},
{
	page: 93,
	kategori 1: non-fiksi,
	kategori 2: gaya-hidup,
	book_url: https://www.gramedia.com/products/resep-olahan-keju-terlengkap,
	title: Resep Olahan Keju Terlengkap,
	author: Ratu Hani,
	Jumlah Halaman: 112,
	Tanggal Terbit: 18 Des 2017,
	ISBN: 9789790822955,
	Penerbit: Demedia,
	Berat: 0.215 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
