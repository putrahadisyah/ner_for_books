{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/steve-jobs-super-trik-sukses-dan-kaya-raya,
	title: Steve Jobs Super Trik Sukses Dan Kaya Raya,
	author: Damar Kencana,
	Jumlah Halaman: 280.0,
	Tanggal Terbit: 5 Des 2019,
	ISBN: 9786025945366,
	Bahasa: Indonesia,
	Penerbit: Scritto Books,
	Berat: 0.236 kg,
	Lebar: 14.0 cm,
	Panjang: 20.0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/belajar-goblok-dari-bob-sadino,
	title: Belajar Goblok Dari Bob Sadino,
	author: Dodi Mawardi,
	Tanggal Terbit: 8 Mar 2010,
	ISBN: 9789799646360,
	Bahasa: Indonesia,
	Penerbit: Kintamani,
	Berat: 0.2500 kg,
	Lebar: 14 cm,
	Panjang: 0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/milikilah-mental-pemenang-mengenal-mengembangkan-diri,
	title: Milikilah Mental Pemenang - Mengenal & Mengembangkan Diri,
	author: Irwan Amrun,
	Jumlah Halaman: 208,
	Tanggal Terbit: 18 Mar 2019,
	ISBN: 9786020495644,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.560 kg,
	Lebar: 23 cm,
	Panjang: 15 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/paris-my-sweet,
	title: Paris, My Sweet,
	author: Amy Thomas,
	Jumlah Halaman: 292,
	Tanggal Terbit: 10 Jul 2017,
	ISBN: 9786020301679,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.2000 kg,
	Lebar: 13.5 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/penuh-andalah-yang-memutuskan-gelas-itu-setengah-kosong-ata,
	title: Penuh: Andalah Yang Memutuskan Gelas Itu Setengah Kosong Ata,
	author: Sony Adams,
	Jumlah Halaman: 168.0,
	Tanggal Terbit: 9 Okt 2019,
	ISBN: 9786237115809,
	Bahasa: Indonesia,
	Penerbit: PSIKOLOGI CORNER,
	Berat: 0.126 kg,
	Lebar: 13.0 cm,
	Panjang: 19.2 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/i-believe-i-will-succeed,
	title: I Believe I Will Succeed,
	author: Alfon Elnath Octaura,
	Jumlah Halaman: 196.0,
	Tanggal Terbit: 23 Des 2019,
	ISBN: 9786230002311,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.3 kg,
	Lebar: 14.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/organogram,
	title: Organogram,
	author: DR. Ing. H. Rachmantio,
	Jumlah Halaman: 80.0,
	Tanggal Terbit: 12 Des 2019,
	ISBN: 9786239037628,
	Bahasa: Indonesia,
	Penerbit: Rainbook Publishing,
	Berat: 0.064 kg,
	Lebar: 13.0 cm,
	Panjang: 19.0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/conf-trainerpreneur-edisi-revisi,
	title: Trainerpreneur - Edisi Revisi,
	author: Ongky Hojanto,
	Jumlah Halaman: 200,
	Tanggal Terbit: 26 Agt 2016,
	ISBN: 9786020333557,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.9119 kg,
	Lebar: 1 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/inspiring-headmaster,
	title: Inspiring Headmaster,
	author: Jamaluddin El-banjary,
	Jumlah Halaman: 512.0,
	Tanggal Terbit: 4 Nov 2019,
	ISBN: 9786230007927,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.55 kg,
	Lebar: 14.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/asyik-ngobrol-dengan-siapa-pun,
	title: Asyik Ngobrol Dengan Siapa Pun,
	author: Ian Dimas,
	Jumlah Halaman: 204,
	Tanggal Terbit: 20 Jan 2017,
	ISBN: 9786023913428,
	Bahasa: Indonesia,
	Penerbit: Saufa,
	Berat: 0.2350 kg,
	Lebar: 14 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/tina-metamorfosis-yang-menakjubkan-seorang-tina-toon,
	title: Tina: Metamorfosis Yang Menakjubkan Seorang Tina Toon,
	author: Alia Fathiyah,
	Jumlah Halaman: 100,
	Tanggal Terbit: 22 Agt 2014,
	ISBN: 9786020305059,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3000 kg,
	Lebar: 20 cm,
	Panjang: 0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/conf-la-taias-for-ummahat-kekuatan-itu-bernama-ibu,
	title: La Taias for Ummahat: Kekuatan Itu Bernama Ibu,
	author: Naqiyyah Syam,
	Jumlah Halaman: 158,
	Tanggal Terbit: 26 Agt 2014,
	ISBN: 9786020307398,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.300 kg,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/conf-dahsyatnya-kemauan,
	title: Dahsyatnya Kemauan,
	author: Ainy Fauziyah,
	Jumlah Halaman: 184,
	Tanggal Terbit: 1 Jan 1970,
	ISBN: 9786020285184,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/tetap-waras-walau-patah-hati,
	title: Tetap Waras, Walau Patah Hati,
	author: Mawila Pertiwi & Kang Sam,
	Jumlah Halaman: 160.0,
	Tanggal Terbit: 6 Mar 2020,
	ISBN: 9786237333326,
	Bahasa: Indonesia,
	Penerbit: Cemerlang Publishing,
	Berat: 0.145 kg,
	Lebar: 14.0 cm,
	Panjang: 20.0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/the-art-of-giving-back,
	title: The Art Of Giving Back,
	author: Nila Tanzil,
	Jumlah Halaman: 128,
	Tanggal Terbit: 6 Nov 2018,
	ISBN: 9786024261030,
	Bahasa: Indonesia,
	Penerbit: B First,
	Berat: 0.14 kg,
	Lebar: 13 cm,
	Panjang: 20 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/allah-swt-is-my-goal,
	title: Allah SWT Is My Goal,
	author: Mhd. Rois Almaududy,
	Jumlah Halaman: 192,
	Tanggal Terbit: 15 Nov 2017,
	ISBN: 9786020894812,
	Penerbit: Tiga Serangkai,
	Berat: 0.175 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/dream-and-destiny,
	title: Dream And Destiny,
	author: Kennedy Jenniffer Dhillon,
	Jumlah Halaman: 98,
	Tanggal Terbit: 1 Jan 2001,
	ISBN: 9789792959178,
	Bahasa: Indonesia,
	Penerbit: No Publisher,
	Berat: 0.4000 kg,
	Lebar: 19 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/discipline-like-japanese,
	title: Discipline Like Japanese,
	author: Istifatun Zaka,
	Jumlah Halaman: 292.0,
	Tanggal Terbit: 23 Des 2019,
	ISBN: 9786025964138,
	Bahasa: Indonesia,
	Penerbit: Caesar Media Pustaka,
	Berat: 0.244 kg,
	Lebar: 13.5 cm,
	Panjang: 20.0 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/conf-sokola-rimba-revisi,
	title: Sokola Rimba (Revisi),
	author: Butet Manurung,
	Jumlah Halaman: 376,
	Tanggal Terbit: 15 Mar 2016,
	ISBN: 9789797099961,
	Penerbit: Kompas Penerbit Buku,
	Berat: 0.9119 kg,
	Lebar: 1 cm,
},
{
	page: 69,
	kategori 1: non-fiksi,
	kategori 2: pengembangan-diri,
	book_url: https://www.gramedia.com/products/leadership-principles-for-graduates,
	title: Leadership Principles For Graduates,
	author: John C. Maxwell,
	Jumlah Halaman: 120,
	Tanggal Terbit: 15 Nov 2016,
	ISBN: 9786020956435,
	Bahasa: Indonesia,
	Penerbit: Mic Publishing,
	Berat: 0.25 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
