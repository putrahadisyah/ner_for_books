{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/peruntungan-di-tahun-kambing-kayu-2566,
	title: Peruntungan Di Tahun Kambing Kayu 2566,
	author: Kang Hong Kian,
	Jumlah Halaman: -,
	Tanggal Terbit: 9 Jan 2015,
	ISBN: 9786022498902,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.3000 kg,
	Lebar: 19 cm,
	Panjang: 0 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/peruntungan-shio-berdasarkan-jam-kelahiran,
	title: Peruntungan Shio Berdasarkan Jam Kelahiran,
	author: Tjia Hong Nio,
	Jumlah Halaman: 108,
	Tanggal Terbit: 8 Sep 2014,
	ISBN: 9786020000000,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.3000 kg,
	Lebar: 18 cm,
	Panjang: 0 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/formulasi-matematika-fisika,
	title: Formulasi Matematika & Fisika,
	author: R Tao R H,
	Jumlah Halaman: 108,
	Tanggal Terbit: 4 Okt 2016,
	ISBN: 9786027284685,
	Bahasa: Indonesia,
	Penerbit: Graha Ilmu,
	Berat: 0.2500 kg,
	Lebar: 18 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/filogenetika-molekuler-teori-aplikasi,
	title: Filogenetika Molekuler : Teori & Aplikasi,
	author: Victor Aprilyanto,
	Jumlah Halaman: 212,
	Tanggal Terbit: 29 Apr 2016,
	ISBN: 9786027299962,
	Bahasa: Indonesia,
	Penerbit: Innosain,
	Berat: 0.4700 kg,
	Lebar: 21 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/seri-ilmu-pengetahuan-anatomifisioliogi,
	title: Seri Ilmu Pengetahuan Anatomi & Fisiologi,
	author: Naomi E Balaban & James E Bobick,
	Jumlah Halaman: 396,
	Tanggal Terbit: 22 Sep 2014,
	ISBN: 9789790624719,
	Bahasa: Indonesia,
	Penerbit: Penerbit Indeks,
	Berat: 0.5 kg,
	Lebar: 17 cm,
	Panjang: 25 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/dna-barcode-fauna-indonesia,
	title: Dna Barcode Fauna Indonesia,
	author: M Syamsul Arifin,
	Jumlah Halaman: 262,
	Tanggal Terbit: 9 Des 2013,
	ISBN: 9786027985261,
	Bahasa: Indonesia,
	Penerbit: Kencana,
	Berat: 0.25 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/prinsip2-stratigrafi,
	title: Prinsip-prinsip Stratigrafi,
	author: Djauhari Noor,
	Jumlah Halaman: 136,
	Tanggal Terbit: 15 Feb 2016,
	ISBN: 9786027854437,
	Bahasa: Indonesia,
	Penerbit: Khalifah Mediatama,
	Berat: 0.2000 kg,
	Lebar: 15 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/penelitian-geografi-terapan,
	title: Penelitian Geografi Terapan,
	author: Khairani,
	Jumlah Halaman: 0,
	Tanggal Terbit: 1 Nov 2016,
	ISBN: 9786024220877,
	Penerbit: Prenadamedia Group,
	Berat: 2 kg,
	Lebar: 0 cm,
	Panjang: 1 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/statistik-nonparametris-untuk-penelitian,
	title: Statistik Nonparametris Untuk Penelitian,
	author: Sugiyono,
	Jumlah Halaman: 374,
	Tanggal Terbit: 28 Okt 2015,
	ISBN: 9786022891796,
	Bahasa: Indonesia,
	Penerbit: Alfabeta,
	Berat: 0.5000 kg,
	Lebar: 16 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/statistik-psikologi-solusi-masalah-psikologi-dengan-statist,
	title: Statistik Psikologi, Solusi Masalah Psikologi Dengan Statistik,
	author: Setiasih,
	Jumlah Halaman: 179,
	Tanggal Terbit: 2 Nov 2018,
	ISBN: 9786022628071,
	Bahasa: Indonesia,
	Penerbit: Graha Ilmu,
	Berat: 0.300 kg,
	Lebar: 17 cm,
	Panjang: 24 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/metode-analisis-vegetasi-dan-komunitas-hewan,
	title: Metode Analisis Vegetasi dan Komunitas Hewan,
	author: Ir. Indriyanto, M.P.,
	Jumlah Halaman: 253,
	Tanggal Terbit: 23 Feb 2019,
	ISBN: 9786022628453,
	Bahasa: Indonesia,
	Penerbit: Graha Ilmu,
	Berat: 0.35 kg,
	Lebar: 17 cm,
	Panjang: 24 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/kalkulus-diferensial-teori-aplikasi,
	title: Kalkulus Diferensial Teori&Aplikasi,
	author: Sudaryono,
	Tanggal Terbit: 22 Agt 2013,
	Penerbit: Kencana,
	Berat: 0.3000 kg,
	Lebar: 0 cm,
	Panjang: 0 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/karakterisasi-gen-antioksidan-untuk-pengembangan-panax-ginseng,
	title: Karakterisasi Gen Antioksidan Untuk Pengembangan Panax Ginseng,
	author: Johan Sukweenadhi,
	Jumlah Halaman: 67,
	Tanggal Terbit: 18 Okt 2017,
	ISBN: 9786022628033,
	Bahasa: Indonesia,
	Penerbit: Graha Ilmu,
	Berat: 0.15 kg,
	Lebar: 17 cm,
	Panjang: 24 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/analisis-data-longitudinal,
	title: Analisis Data Longitudinal,
	author: Danardono,
	Jumlah Halaman: 174,
	Tanggal Terbit: 3 Nov 2015,
	ISBN: 9789794209950,
	Bahasa: Indonesia,
	Penerbit: Ugm Gadjah Mada University Press,
	Berat: 0.2500 kg,
	Lebar: 16 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/matematika-diskrit-1,
	title: Matematika Diskrit,
	author: Gede Suweken,
	Jumlah Halaman: 93,
	Tanggal Terbit: 8 Jul 2017,
	ISBN: 9786024251970,
	Bahasa: Indonesia,
	Penerbit: Rajagrafindo Persada,
	Berat: 0.15 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/daftar-jenis-tumbuhan-di-pulau-wawanii-sulteng,
	title: Daftar Jenis Tumbuhan Di Pulau Wawanii, Sulteng,
	author: Rugayah Dkk,
	Jumlah Halaman: 364,
	Tanggal Terbit: 26 Feb 2016,
	ISBN: 9789797998455,
	Bahasa: Indonesia,
	Penerbit: Lipi Press,
	Berat: 0.4000 kg,
	Lebar: 15 cm,
	Panjang: 21 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/mengenal-integral-lebih-dekat-untuk-sma-perguruan-tinggi,
	title: Mengenal Integral Lebih Dekat untuk SMA & Perguruan Tinggi,
	author: Joko Ade Nursiyono Dan Jamik Safitri,
	Jumlah Halaman: 208,
	Tanggal Terbit: 21 Apr 2016,
	ISBN: 9786020946795,
	Bahasa: Indonesia,
	Penerbit: In Media,
	Berat: 0.2300 kg,
	Lebar: 14 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/desain-eksperimen-untuk-penelitian-ilmiah,
	title: Desain Eksperimen Untuk Penelitian Ilmiah,
	author: Suwanda,
	Jumlah Halaman: 356,
	Tanggal Terbit: 28 Mar 2011,
	ISBN: 9786028800648,
	Bahasa: Indonesia,
	Penerbit: Alfabeta,
	Berat: 0.4500 kg,
	Lebar: 16 cm,
	Panjang: 24 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/statistik-teori-aplikasi-ed-8-jl-2,
	title: Statistik Teori&Aplikasi Ed 8 Jl 2,
	author: J Supranto,
	Jumlah Halaman: 406,
	Tanggal Terbit: 2 Sep 2016,
	ISBN: 9786022985662,
	Bahasa: Indonesia,
	Penerbit: Erlangga,
	Berat: 0.6500 kg,
	Lebar: 18 cm,
},
{
	page: 24,
	kategori 1: non-fiksi,
	kategori 2: sains,
	book_url: https://www.gramedia.com/products/pedoman-aplikasi-metode-penelitiuan-dalam-penyusunan-karya-i,
	title: Pedoman Aplikasi Metode Penelitiuan dalam Penyusunan Karya Ilmiah, Skripsi, Tesis, dan Disertasi,
	author: Beni Ahmad Saebani,
	Jumlah Halaman: 448,
	Tanggal Terbit: 8 Jan 2018,
	ISBN: 9789790766617,
	Bahasa: Indonesia,
	Penerbit: Pustaka Setia,
	Berat: 0.535 kg,
	Lebar: 16 cm,
	Panjang: 24 cm,
},
