{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/a-piece-of-the-moon,
	title: A Piece Of The Moon,
	author: Ha Hyun,
	Jumlah Halaman: 194,
	Tanggal Terbit: 8 Mar 2019,
	ISBN: 9786025297274,
	Bahasa: Indonesia,
	Penerbit: Haru,
	Berat: 0.2 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/komik-touche,
	title: Komik: Touche,
	author: Windhy Puspitadewi,
	Jumlah Halaman: 192,
	Tanggal Terbit: 8 Okt 2018,
	ISBN: 9786020618395,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.15 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/mengungkit-pembunuhan-five-little-pigs-cover-baru,
	title: Mengungkit Pembunuhan (Five Little Pigs) - Cover Baru,
	author: Agatha Christie,
	Jumlah Halaman: 472,
	Tanggal Terbit: 27 Nov 2017,
	ISBN: 9789792283655,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.400 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/my-promise,
	title: My Promise,
	author: Nina Rosmina,
	Jumlah Halaman: 240,
	Tanggal Terbit: 16 Apr 2018,
	ISBN: 9786020502212,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.3 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/mereja-datang-ke-baghdad-they-came-to-baghdad-cover-baru,
	title: Mereja Datang Ke Baghdad (They Came To Baghdad) - Cover Baru,
	author: Agatha Christie,
	Jumlah Halaman: 352,
	Tanggal Terbit: 6 Mei 2019,
	ISBN: 9789792293760,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.120 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/my-ice-boy,
	title: My Ice Boy,
	author: Pit Sansi,
	Jumlah Halaman: 396,
	Tanggal Terbit: 9 Jul 2018,
	ISBN: 9786024303464,
	Bahasa: Indonesia,
	Penerbit: Mizan Media Utama Pt,
	Berat: 0.3 kg,
	Lebar: 15 cm,
	Panjang: 21 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/hijrah-cinta,
	title: Hijrah Cinta,
	author: Dini Fitria,
	Jumlah Halaman: 368,
	Tanggal Terbit: 19 Mei 2017,
	ISBN: 9786026051462,
	Bahasa: Indonesia,
	Penerbit: Falcon Publishing,
	Berat: 0.35 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/harlequin-koleksi-istimewa-ellie-dan-taipan-yunani-carrying-the-greeks-heir,
	title: Harlequin Koleksi Istimewa: Ellie dan Taipan Yunani (Carrying the Greek's Heir),
	author: Sharon Kendrick,
	Jumlah Halaman: 208,
	Tanggal Terbit: 7 Jan 2019,
	ISBN: 9786020620220,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.125 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/friendzone-lempar-kode-sembunyi-hati,
	title: Friendzone: Lempar Kode Sembunyi Hati,
	author: Alnira,
	Jumlah Halaman: 310,
	Tanggal Terbit: 5 Feb 2018,
	ISBN: 9786024528423,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.200 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/wedding-dress-the-gaun-pengantin,
	title: Wedding Dress,The: Gaun Pengantin,
	author: Rachel Hauck,
	Jumlah Halaman: 405,
	Tanggal Terbit: 6 Jul 2015,
	ISBN: 9786020268644,
	Bahasa: Indonesia,
	Penerbit: Thomas Nelson,
	Berat: 0.3820 kg,
	Lebar: 12.5 cm,
	Panjang: 19.5 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/the-chaos-walking-trilogy1_the-knife-of-never-letting-go-pisau-luka,
	title: The Chaos Walking Trilogy#1_The Knife of Never Letting Go (Pisau Luka),
	author: Patrick Ness,
	Jumlah Halaman: 540,
	Tanggal Terbit: 26 Mar 2018,
	ISBN: 9786020379289,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.800 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/pada-senja-yang-membawamu-pergi-new-cover,
	title: Pada Senja yang Membawamu Pergi (New Cover),
	author: Boy Candra,
	Jumlah Halaman: 256,
	Tanggal Terbit: 11 Jun 2018,
	ISBN: 9789797809232,
	Bahasa: Indonesia,
	Penerbit: Gagas Media,
	Berat: 0.175 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/lit-tentangdiaku,
	title: LiT: #Tentangdiaku,
	author: Hani Widiani,
	Jumlah Halaman: 176,
	Tanggal Terbit: 14 Mei 2018,
	ISBN: 9786020460000,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.12 kg,
	Lebar: 10.5 cm,
	Panjang: 16 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/harlequin-koleksi-istimewa-pernikahan-allegra-wedding-night-with-her-enemy,
	title: Harlequin Koleksi Istimewa: Pernikahan Allegra (Wedding Night with Her Enemy),
	author: Melanie Milburne,
	Jumlah Halaman: 224,
	Tanggal Terbit: 9 Jul 2018,
	ISBN: 9786020387994,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.12 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/the-age-of-reason,
	title: The Age Of Reason,
	author: Jean Paul Sartre,
	Jumlah Halaman: 468,
	Tanggal Terbit: 17 Mei 2018,
	ISBN: 9786026657763,
	Bahasa: Indonesia,
	Penerbit: Immortal,
	Berat: 0.4 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/the-da-vinci-code-new,
	title: The Da Vinci Code-New,
	author: Daniel Gerhard Brown,
	Jumlah Halaman: 680,
	Tanggal Terbit: 20 Jul 2016,
	ISBN: 9786022911845,
	Bahasa: Indonesia,
	Penerbit: Bentang,
	Berat: 0.3600 kg,
	Lebar: 18 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/conf-hq-blush-her-irresistible-protector,
	title: HQ Blush: Her Irresistible Protector,
	author: Michelle Douglas,
	Jumlah Halaman: 276,
	Tanggal Terbit: 3 Sep 2016,
	ISBN: 9786020265551,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/hr-the-perfect-stranger,
	title: HR: The Perfect Stranger,
	author: Anne Gracie,
	Jumlah Halaman: 536,
	Tanggal Terbit: 14 Mei 2018,
	ISBN: 9786020460123,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.28 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/ppm-love-02-tamat,
	title: PPM Love 02 - Tamat,
	author: Baek Myo,
	Jumlah Halaman: 304,
	Tanggal Terbit: 1 Feb 2016,
	ISBN: 9786023397426,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.2500 kg,
	Lebar: 13.2 cm,
	Panjang: 20 cm,
},
{
	page: 19,
	kategori 1: fiksi-sastra,
	kategori 2: fiksi-populer,
	book_url: https://www.gramedia.com/products/teenlit-mawar-merah3-matahari-cover-baru,
	title: Teenlit: Mawar Merah#3: Matahari - Cover Baru,
	author: Luna Torashyngu,
	Jumlah Halaman: 384,
	Tanggal Terbit: 24 Jun 2019,
	ISBN: 9786020310756,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.130 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
