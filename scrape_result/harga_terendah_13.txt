{
	page: 13,
	book_url: https://www.gramedia.com/products/countdown-7-days-01,
	title: Countdown 7 Days 01,
	author: Karakara Kemuri,
	Jumlah Halaman: 184,
	Tanggal Terbit: 7 Okt 2015,
	ISBN: 9786023395170,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/dot-to-dot-coloring-cars-sahabat-sejati,
	title: Dot To Dot & Coloring Cars Sahabat Sejati,
	author: Disney,
	Jumlah Halaman: 32,
	Tanggal Terbit: 28 Sep 2015,
	ISBN: 9786020273341,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2000 kg,
	Lebar: 21 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/conf-waikiki-restaurant-in-enoshima-05,
	title: Waikiki Restaurant in Enoshima 05,
	author: Shonen-gahosha / Okai Haruko,
	Tanggal Terbit: 18 Jan 2016,
	ISBN: 9786020250960,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1500 kg,
	Lebar: 0 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/attack-on-titan-2-lc-cu-nomor-baru,
	title: LC: Attack On Titan 2,
	author: Hajime Isayama,
	Tanggal Terbit: 19 Mei 2015,
	ISBN: 9786020264868,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/maruhan-the-mercenary-04,
	title: Maruhan The Mercenary 04,
	author: Kim Sung-jae / Kim Byung-jin,
	Jumlah Halaman: 184,
	Tanggal Terbit: 7 Okt 2015,
	ISBN: 9786023395118,
	Bahasa: Indonesia,
	Penerbit: Daewon C.i.,
	Berat: 0.2000 kg,
	Lebar: 13 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/blue-leadwort-02,
	title: Blue Leadwort 02,
	author: Selena Lin,
	Jumlah Halaman: 184,
	Tanggal Terbit: 3 Agt 2016,
	ISBN: 9786020290898,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 11.4 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/midnight-lolita-terbit-ulang,
	title: Midnight Lolita (Terbit Ulang),
	author: Saki Siumi,
	Jumlah Halaman: 192,
	Tanggal Terbit: 8 Jun 2016,
	ISBN: 9786023399659,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.2000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/doctor-s-naughty-kiss-terbit-ulang,
	title: Doctor`S Naughty Kiss (Terbit Ulang),
	author: Shin Kawamaru,
	Jumlah Halaman: 192,
	Tanggal Terbit: 10 Jun 2015,
	ISBN: 9786023393565,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/nice-guy-syndrome-08,
	title: Nice Guy Syndrome 08,
	author: Hwang Mi Ree,
	Jumlah Halaman: 160,
	Tanggal Terbit: 30 Sep 2015,
	ISBN: 9786023395101,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/yotsuba-02-terbit-ulang,
	title: Yotsuba&! 02 (Terbit Ulang),
	author: Kiyohiko Azuma,
	Jumlah Halaman: 196,
	Tanggal Terbit: 10 Jun 2015,
	ISBN: 9786020266817,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/heavenly-dogs-stories-last-letter,
	title: Heavenly Dogs Stories - Last Letter,
	author: Atsuko Hotta,
	Jumlah Halaman: 187,
	Tanggal Terbit: 23 Sep 2015,
	ISBN: 9786023394937,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/dot-to-dot-coloring-cars-juara-sejati,
	title: Dot To Dot & Coloring Cars Juara Sejati,
	author: Disney,
	Jumlah Halaman: 32,
	Tanggal Terbit: 28 Sep 2015,
	ISBN: 9786020273334,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2000 kg,
	Lebar: 21 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/chocolate-girl-extra-lc,
	title: Chocolate Girl Extra: Lc,
	author: Yoshihara Yuki,
	Jumlah Halaman: 192,
	Tanggal Terbit: 13 Jan 2016,
	ISBN: 9786020279053,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 12 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/sunny-doll-02-tamat,
	title: Sunny Doll 02 (Tamat),
	author: Honey Chen,
	Jumlah Halaman: 188,
	Tanggal Terbit: 25 Nov 2015,
	ISBN: 9786020274645,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 11.4 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/sticker-puzzle-barbie-rock-n-royals-my-bff-rocks,
	title: Sticker Puzzle Barbie Rock N Royals: My Bff Rocks!,
	author: Mattel East Asia Limited,
	Jumlah Halaman: 16,
	Tanggal Terbit: 2 Nov 2015,
	ISBN: 9786020275642,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2500 kg,
	Lebar: 25 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/bride-of-the-fox,
	title: Bride Of The Fox,
	author: Chiyori,
	Jumlah Halaman: 192,
	Tanggal Terbit: 6 Jan 2016,
	ISBN: 9786023396955,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/swordsman-the-06,
	title: The Swordsman 06,
	author: Lee Jae Heon / Hong Ki Woo,
	Jumlah Halaman: 168,
	Tanggal Terbit: 13 Jan 2016,
	ISBN: 9786023397006,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.2000 kg,
	Lebar: 13 cm,
	Panjang: 18 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/utsuho-20,
	title: Utsuho 20,
	author: Yukii Iinuma,
	Jumlah Halaman: 200,
	Tanggal Terbit: 13 Jan 2016,
	ISBN: 9786020277714,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 11.4 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/you-are-my-world-03,
	title: You Are My World 03,
	author: Keiko Notoyama,
	Jumlah Halaman: 192,
	Tanggal Terbit: 11 Nov 2015,
	ISBN: 9786023395804,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.1000 kg,
	Lebar: 11 cm,
},
{
	page: 13,
	book_url: https://www.gramedia.com/products/ouroboros-09-lc,
	title: Ouroboros 09: Lc,
	author: Yuya Kanzaki,
	Jumlah Halaman: 176,
	Tanggal Terbit: 11 Nov 2015,
	ISBN: 9786020276137,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2000 kg,
	Lebar: 13 cm,
},
