{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/pesan-cinta-mbah-moen,
	title: Pesan Cinta Mbah Moen,
	author: Tim Rene Islam,
	Jumlah Halaman: 244.0,
	Tanggal Terbit: 17 Okt 2019,
	ISBN: 9786021201732,
	Bahasa: Indonesia,
	Penerbit: RENE ISLAM,
	Berat: 0.5 kg,
	Lebar: 14.0 cm,
	Panjang: 18.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/from-this-day-forward-sejak-hari-ini-sampai-selamanya,
	title: From This Day Forward (Sejak Hari Ini Sampai Selamanya),
	author: Craig & Amy Groeschel,
	Jumlah Halaman: 168.0,
	Tanggal Terbit: 4 Okt 2019,
	ISBN: 9786024191474,
	Bahasa: Indonesia,
	Penerbit: Light Publishing,
	Berat: 0.159 kg,
	Lebar: 14.5 cm,
	Panjang: 21.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/jejak-jejak-mengagumkan,
	title: Jejak-Jejak Mengagumkan,
	author: Nemo (Purnomo),
	Jumlah Halaman: 206.0,
	Tanggal Terbit: 1 Nov 2019,
	ISBN: 9786237394082,
	Bahasa: Indonesia,
	Penerbit: Tiga Serangkai,
	Berat: 0.195 kg,
	Lebar: 15.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/menjadi-muslimah-hebat,
	title: Menjadi Muslimah Hebat,
	author: Asrifin An Nakhrawie,
	Jumlah Halaman: 200.0,
	Tanggal Terbit: 28 Agt 2019,
	ISBN: 9786025372896,
	Bahasa: Indonesia,
	Penerbit: Syalmahat Publishing,
	Berat: 0.245 kg,
	Lebar: 14.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/rabiah-al-adawiyah-perjalanan-cinta-wanita-sufi,
	title: Rabiah Al-Adawiyah: Perjalanan & Cinta Wanita Sufi,
	author: Azeez Naviel M.,
	Jumlah Halaman: 196.0,
	Tanggal Terbit: 23 Sep 2019,
	ISBN: 9786025992612,
	Bahasa: Indonesia,
	Penerbit: Solusi Distribusi Buku Cv,
	Berat: 0.147 kg,
	Lebar: 12.4 cm,
	Panjang: 18.9 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/taman-pencinta-raudhah-al-muhibbin,
	title: Taman Pencinta (Raudhah Al-Muhibbin),
	author: Ibnu Qayyim Al-Jauzy,
	Jumlah Halaman: 503.0,
	Tanggal Terbit: 13 Sep 2019,
	ISBN: 208036190,
	Bahasa: Indonesia,
	Penerbit: Mizan Media Utama Pt,
	Berat: 0.76 kg,
	Lebar: 23.0 cm,
	Panjang: 15.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/be-a-winner-in-spiritual-warfare,
	title: Be A Winner In Spiritual Warfare,
	author: Neil T. Anderson,
	Jumlah Halaman: 208.0,
	Tanggal Terbit: 9 Des 2019,
	ISBN: 9786237519263,
	Bahasa: Indonesia,
	Penerbit: Andi Offset,
	Berat: 0.229 kg,
	Lebar: 14.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/matan-abi-syuja,
	title: Matan Abi Syuja`,
	author: IMAM AHMAD BIN HUSEN,
	Jumlah Halaman: 60.0,
	Tanggal Terbit: 11 Okt 2019,
	ISBN: 9786025145254,
	Bahasa: Indonesia,
	Penerbit: MAKTABAH TURMUSY,
	Berat: 0.1 kg,
	Lebar: 17.7 cm,
	Panjang: 25.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/muhammad-sang-guru,
	title: Muhammad Sang Guru,
	author: ,
	Jumlah Halaman: 239.0,
	Tanggal Terbit: 3 Okt 2019,
	ISBN: 9786237409076,
	Bahasa: Indonesia,
	Penerbit: Serambi Semesta Distribusi,
	Berat: 0.201 kg,
	Lebar: 13.0 cm,
	Panjang: 20.5 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/the-three-hats-of-leadership,
	title: The Three Hats Of Leadership,
	author: HERDY HUTABARAT,
	Jumlah Halaman: 208.0,
	Tanggal Terbit: 18 Des 2019,
	ISBN: 9786237519461,
	Bahasa: Indonesia,
	Penerbit: Andi Offset,
	Berat: 0.274 kg,
	Lebar: 15.0 cm,
	Panjang: 23.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/kompilasi-hadis-sahih-populer,
	title: Kompilasi Hadis Sahih Populer,
	author: Ismail H.M.,S.PD.I,
	Jumlah Halaman: 336.0,
	Tanggal Terbit: 10 Okt 2019,
	ISBN: 9786239126025,
	Bahasa: Indonesia,
	Penerbit: Pustaka al Uswah,
	Berat: 0.318 kg,
	Lebar: 15.0 cm,
	Panjang: 23.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/al-quran-al-qayyim-a5-terjemah-tajwid-resl-15x215-ziyad-1,
	title: Al Quran Al Qayyim A5 Terjemah Tajwid Resl 15X21.5 Ziyad,
	author: ZIYAD QURAN,
	Jumlah Halaman: 630.0,
	Tanggal Terbit: 10 Feb 2020,
	ISBN: 9786023173778,
	Bahasa: Indonesia,
	Penerbit: ZIYAD QURAN,
	Berat: 0.56 kg,
	Lebar: 16.0 cm,
	Panjang: 21.5 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/paulus-perjalanan-seorang-rasul,
	title: Paulus Perjalanan Seorang Rasul,
	author: Douglas A. Campbell,
	Jumlah Halaman: 298.0,
	Tanggal Terbit: 4 Feb 2020,
	ISBN: 9786022316961,
	Bahasa: Indonesia,
	Penerbit: BPKGM,
	Berat: 0.345 kg,
	Lebar: 14.5 cm,
	Panjang: 21.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/bingung-bingung,
	title: Bingung Bingung,
	author: Fadel Ilahi Eldimisky,
	Jumlah Halaman: 166,
	Tanggal Terbit: 5 Agt 2019,
	ISBN: 9786020630472,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.130 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/tadabur-juz-amma,
	title: Tadabur Juz Amma,
	author: DR SAIFUL BAHRI,
	Jumlah Halaman: 384.0,
	Tanggal Terbit: 2 Nov 2019,
	ISBN: 9789795928430,
	Bahasa: Indonesia,
	Penerbit: PUSTAKA AL KAUTSAR,
	Berat: 0.6 kg,
	Lebar: 15.5 cm,
	Panjang: 24.5 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/malaikat-agen-supernatural-allah,
	title: Malaikat Agen Supernatural Allah,
	author: ED ROCHA,
	Jumlah Halaman: 208.0,
	Tanggal Terbit: 20 Okt 2016,
	ISBN: 9786237519065,
	Bahasa: Indonesia,
	Penerbit: Andi Offset,
	Berat: 0.23 kg,
	Lebar: 14.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/ensiklopedi-sejarah-islam,
	title: Ensiklopedi Sejarah Islam,
	author: Dr Raghib As-Sirjani,
	Jumlah Halaman: 494.0,
	Tanggal Terbit: 17 Sep 2019,
	ISBN: 9789795928355,
	Bahasa: Indonesia,
	Penerbit: Pustaka Al-Kautsar,
	Berat: 1.286 kg,
	Lebar: 16.1 cm,
	Panjang: 24.6 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/pendidikan-islam-mengupas-aspek-aspek-dalam-dunia-pendidikan,
	title: Pendidikan Islam Mengupas Aspek-Aspek Dalam Dunia Pendidikan,
	author: Moh. Abdullah, Moch. Faizin Muflich, Lailil Zumroti, Muhamad Basyrul Muvid,
	Jumlah Halaman: 300.0,
	Tanggal Terbit: 3 Des 2019,
	ISBN: 9786237593041,
	Bahasa: Indonesia,
	Penerbit: Aswaja Pressindo,
	Berat: 0.33 kg,
	Lebar: 14.5 cm,
	Panjang: 20.5 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/sejarah-otentik-politik-nabi-muhammad-saw,
	title: Sejarah Otentik Politik Nabi Muhammad Saw,
	author: Prof. Dr. Husain Munis,
	Jumlah Halaman: 294.0,
	Tanggal Terbit: 1 Agt 2019,
	ISBN: 9786028648301,
	Penerbit: IIman Real,
	Berat: 0.35 kg,
	Lebar: 14.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 89,
	kategori 1: non-fiksi,
	kategori 2: agama,
	book_url: https://www.gramedia.com/products/kitab-para-pencari-kebenaran,
	title: Kitab Para Pencari Kebenaran,
	author: Imam Al Ghazali,
	Jumlah Halaman: 324.0,
	Tanggal Terbit: 28 Agt 2019,
	ISBN: 9786237327219,
	Penerbit: TUROS KHAZANAH PUSTAKA ISLAM,
	Berat: 0.5 kg,
	Lebar: 15.0 cm,
	Panjang: 23.0 cm,
},
