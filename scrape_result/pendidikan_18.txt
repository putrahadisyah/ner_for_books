{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/update-paling-lengkap-drilling-semua-jenis-soal-tes-cpns-sis,
	title: Update Paling Lengkap Drilling Semua Jenis Soal Tes CPNS Sistem CAT dan PBT,
	author: -,
	Jumlah Halaman: 2888,
	Tanggal Terbit: 20 Jun 2016,
	ISBN: 9786023755417,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 1.61 kg,
	Lebar: 19 cm,
	Panjang: 26 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/mi-kli-cinta-alquranhadis-jl1-k13,
	title: MI Kelas 1 Cinta Alquran & Hadis Jilid 1 (Kurikulum 2013),
	author: Choirul Fata,
	Jumlah Halaman: 146,
	Tanggal Terbit: 26 Feb 2018,
	ISBN: 9786022577478,
	Bahasa: Indonesia,
	Penerbit: Tiga Serangkai,
	Berat: 0.27 kg,
	Lebar: 17 cm,
	Panjang: 25 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/smama-kl-x-xi-xii-king-master-biologi-cara-bimbel,
	title: SMA/MA Kelas X, XI, XII, King Master Biologi Cara Bimbel,
	author: Forum Tentor Indonesia,
	Jumlah Halaman: 536,
	Tanggal Terbit: 12 Mar 2018,
	ISBN: 9786025454158,
	Bahasa: Indonesia,
	Penerbit: Forum Edukasi,
	Berat: 0.35 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/atlas-superlengkap-indonesia-dunia,
	title: Atlas Superlengkap Indonesia & Dunia,
	author: Redaksi Cemerlang,
	Jumlah Halaman: 142,
	Tanggal Terbit: 10 Jun 2017,
	ISBN: 9786021348758,
	Bahasa: Indonesia,
	Penerbit: Cemerlang Publishing,
	Berat: 0.50 kg,
	Lebar: 21 cm,
	Panjang: 29 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/kamus-lengkap-bahasa-jerman-sc-idx,
	title: Kamus Lengkap Bahasa Jerman,
	author: Risa Agustin,
	Jumlah Halaman: 576,
	Tanggal Terbit: 4 Sep 2015,
	ISBN: 9786022332015,
	Bahasa: Indonesia,
	Penerbit: Serba Jaya,
	Berat: 0.55 kg,
	Lebar: 14.5 cm,
	Panjang: 20 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sd-kelas-v-tematik-udara-bersih-bagi-kesehatan-k13-revisi-2016,
	title: SD Kelas V Tematik Udara Bersih bagi Kesehatan K13 Revisi 2016,
	author: Dhiah Saptorini,lili Nurlaili,
	Jumlah Halaman: None,
	Tanggal Terbit: 1 Apr 2018,
	ISBN: 9786022996712,
	Bahasa: Indonesia,
	Penerbit: Pt Yudhistira Ghalia Indonesia,
	Berat: 0.400 kg,
	Lebar: 20 cm,
	Panjang: 27 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sma-ma-kl-xi-sejarah-peminatan-jl-2-k13-n,
	title: SMA/MA Kelas XI Sejarah : Peminatan Jilid 2 Kurikulum 2013,
	author: Ratna Hapsari,
	Jumlah Halaman: 476,
	Tanggal Terbit: 4 Jun 2017,
	ISBN: 9786024341787,
	Bahasa: Indonesia,
	Penerbit: Penerbit Erlangga,
	Berat: 0.40 kg,
	Lebar: 18 cm,
	Panjang: 25 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/bahasa-korea-sehari-hari-cd-audio,
	title: Bahasa Korea Sehari-Hari + Cd Audio,
	author: Sri Endah Setia Lestari,
	Tanggal Terbit: 10 Mar 2011,
	Penerbit: Kesaint Blanc,
	Berat: 0.3000 kg,
	Lebar: 0 cm,
	Panjang: 0 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/kamus-kantong-mandarin-superkomplet,
	title: Kamus Kantong Mandarin Superkomplet,
	author: Aulid Atsani & Cicik Arista,
	Jumlah Halaman: 655,
	Tanggal Terbit: 19 Okt 2017,
	ISBN: 9786021659694,
	Bahasa: Indonesia,
	Penerbit: Charissa Publisher,
	Berat: 0.375 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/smart-fisika-smp,
	title: Smart Fisika SMP,
	author: Wijaya Kurnia Santoso,
	Jumlah Halaman: 200,
	Tanggal Terbit: 25 Sep 2017,
	ISBN: 9786024551674,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.3000 kg,
	Lebar: 14 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/kamus-kantong-jepang-superkomplet,
	title: Kamus Kantong Jepang Superkomplet,
	author: Oktavia Anggraeni,
	Jumlah Halaman: 652,
	Tanggal Terbit: 24 Okt 2017,
	ISBN: 9786021659731,
	Bahasa: Indonesia,
	Penerbit: Charissa Publisher,
	Berat: 0.36 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/smama-kl-12-ekonomi-3-buku-siswa-ilmu2-sosial,
	title: SMA/MA Kelas 12 Ekonomi 3 Buku Siswa Ilmu-Ilmu Sosial,
	author: Kinanti Geminastiti & Nella Nurlita,
	Jumlah Halaman: None,
	Tanggal Terbit: 16 Feb 2017,
	ISBN: 9786023742950,
	Bahasa: Indonesia,
	Penerbit: Yrama Widya,
	Berat: 0.566 kg,
	Lebar: 20 cm,
	Panjang: 26.5 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/jago-kuasai-bahasa-jerman,
	title: Jago Kuasai Bahasa Jerman,
	author: Dian Dwi Anisa & Cindhy Dwi Meidany,
	Jumlah Halaman: None,
	Tanggal Terbit: 4 Jan 2015,
	ISBN: 9786021674932,
	Bahasa: Indonesia,
	Penerbit: Pustaka Baru Press,
	Berat: 0.35 kg,
	Lebar: 15 cm,
	Panjang: 22 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/kamus-indonesia-inggris-edisi-ketiga-yang-diperbarui-sc,
	title: Kamus Indonesia - Inggris Edisi Ketiga Yang Diperbarui,
	author: John M. Echols & Hasan Shadily,
	Jumlah Halaman: None,
	Tanggal Terbit: 1 Sep 2014,
	ISBN: 9786020305653,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3000 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/mahir-3-bahasa-indonesia-inggris-jepang,
	title: Mahir 3 Bahasa: Indonesia Inggris Jepang,
	author: Gading Dani Bao,
	Jumlah Halaman: 224,
	Tanggal Terbit: 18 Jan 2018,
	ISBN: 9786023760862,
	Bahasa: Indonesia,
	Penerbit: Pustaka Baru Press,
	Berat: 0.23 kg,
	Lebar: 15 cm,
	Panjang: 22 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/simple-grammar-for-an-elementary-school,
	title: Simple Grammar For An Elementary School,
	author: Alzenia Putri,
	Jumlah Halaman: 172,
	Tanggal Terbit: 16 Des 2017,
	ISBN: 9786025015786,
	Penerbit: Brilliant Books,
	Berat: 0.155 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/smkmak-klx-aplikasi-pengolah-angkastreadsheet-kikd17,
	title: Smk/Mak Kl.X Aplikasi Pengolah Angka/Streadsheet /Kikd17,
	author: Deky Noviar,
	Jumlah Halaman: 192,
	Tanggal Terbit: 24 Nov 2017,
	ISBN: 9786024344474,
	Bahasa: Indonesia,
	Penerbit: Penerbit Erlangga,
	Berat: 0.33 kg,
	Lebar: 17.5 cm,
	Panjang: 25 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/sdmi-kl5-target-nilai-rapor-10-kupas-habis-semua-pelajaran,
	title: SD/MI Kelas 5 Target Nilai Rapor 10 Kupas Habis Semua Pelajaran,
	author: Tim Guru Indonesia,
	Jumlah Halaman: 450,
	Tanggal Terbit: 1 Jan 2009,
	ISBN: 9789797954109,
	Bahasa: Indonesia,
	Penerbit: Wahyu Media,
	Berat: 0.70 kg,
	Lebar: 19 cm,
	Panjang: 26 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/smp-mts-kl-1-2-3-new-edition-pocket-book-matematika,
	title: New Pocket Book Matematika SMP/MTs Kelas VII,VIII,& IX,
	author: Tim Math Eduka,
	Jumlah Halaman: 216,
	Tanggal Terbit: 19 Apr 2017,
	ISBN: 9786026992642,
	Bahasa: Indonesia,
	Penerbit: Cmedia,
	Berat: 0.1700 kg,
	Lebar: 12 cm,
	Panjang: 18 cm,
},
{
	page: 18,
	kategori 1: non-fiksi,
	kategori 2: pendidikan,
	book_url: https://www.gramedia.com/products/siap-jadi-juara-osn-matematika-olimpiade-sains-nasional,
	title: Siap Jadi Juara Osn Matematika (Olimpiade Sains Nasional),
	author: Mujiyati,
	Jumlah Halaman: 176,
	Tanggal Terbit: 14 Des 2015,
	ISBN: 9786020874265,
	Bahasa: Indonesia,
	Penerbit: Pustaka Baru,
	Berat: 0.3000 kg,
	Lebar: 19 cm,
	Panjang: 25 cm,
},
