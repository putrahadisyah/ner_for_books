{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/hr-do-you-want-to-start-a-scandal,
	title: HR: Do You Want to Start a Scandal,
	author: Tessa Dare,
	Jumlah Halaman: 416.0,
	Tanggal Terbit: 21 Okt 2019,
	ISBN: 9786230007392,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.12 kg,
	Lebar: 11.0 cm,
	Panjang: 18.0 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/modus,
	title: Modus,
	author: Agusta K,
	Jumlah Halaman: 268,
	Tanggal Terbit: 20 Jul 2018,
	ISBN: 9786024303051,
	Bahasa: Indonesia,
	Penerbit: Bentang Pustaka,
	Berat: 0.13 kg,
	Lebar: 13 cm,
	Panjang: 20 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/fall-in-love-with-senior,
	title: Fall In Love with Senior,
	author: SONYA NADILA,
	Jumlah Halaman: 200,
	Tanggal Terbit: 28 Jan 2019,
	ISBN: 9786025508301,
	Bahasa: Indonesia,
	Penerbit: Bintang Media,
	Berat: 0.165 kg,
	Lebar: 20.5 cm,
	Panjang: 14 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/hujan-dan-pawaka,
	title: Hujan Dan Pawaka,
	author: Tria Ayu K,
	Jumlah Halaman: 236,
	Tanggal Terbit: 11 Mar 2019,
	ISBN: 9786024838355,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.180 kg,
	Lebar: 19 cm,
	Panjang: 13 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/dia-yang-lebih-pantas-menjagamu,
	title: Dia Yang Lebih Pantas Menjagamu,
	author: Thulaib eth-Thulaib,
	Jumlah Halaman: 352,
	Tanggal Terbit: 3 Jan 2018,
	ISBN: 9786024072742,
	Bahasa: Indonesia,
	Penerbit: Laksana,
	Berat: 0.3 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/cr-tempting-the-player,
	title: CR: Tempting The Player,
	author: J. Lynn,
	Jumlah Halaman: 376.0,
	Tanggal Terbit: 21 Okt 2019,
	ISBN: 9786230007798,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.11 kg,
	Lebar: 11.0 cm,
	Panjang: 18.0 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/sahabat-dilarang-jatuh-cinta,
	title: Sahabat Dilarang Jatuh Cinta,
	author: Fataya Azzahra,
	Jumlah Halaman: 290,
	Tanggal Terbit: 27 Apr 2018,
	ISBN: 9786026714237,
	Bahasa: Indonesia,
	Penerbit: Falcon Publishing,
	Berat: 0.30 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/high-heels,
	title: High Heels,
	author: Inkarnadine,
	Jumlah Halaman: 150,
	Tanggal Terbit: 16 Jan 2019,
	ISBN: 9786022203001,
	Bahasa: Indonesia,
	Penerbit: Bukune,
	Berat: 0.16 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/jarak-menolak-temu,
	title: Jarak Menolak Temu,
	author: Euis N. Hayati,
	Jumlah Halaman: 185,
	Tanggal Terbit: 26 Mar 2018,
	ISBN: 9786021036785,
	Bahasa: Indonesia,
	Penerbit: Transmedia,
	Berat: 0.15 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/when-dimple-met-rishi,
	title: When Dimple Met Rishi,
	author: SANDHYA MENON,
	Jumlah Halaman: 399,
	Tanggal Terbit: 27 Nov 2018,
	ISBN: 9786026682352,
	Bahasa: Indonesia,
	Penerbit: Penerbit Spring,
	Berat: 0.300 kg,
	Lebar: 20 cm,
	Panjang: 14 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/setelah-pemakaman-after-the-funeral,
	title: After The Funeral (Setelah Pemakaman) - Cover Baru,
	author: Agatha Christie,
	Jumlah Halaman: 336,
	Tanggal Terbit: 27 Des 2017,
	ISBN: 9789792234343,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.130 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/keynan-jason,
	title: Keynan Jason,
	author: S. Andi,
	Jumlah Halaman: 374,
	Tanggal Terbit: 2 Mei 2018,
	ISBN: 9786021097984,
	Bahasa: Indonesia,
	Penerbit: Buku Pintar Indonesia,
	Berat: 0.35 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/hr-a-study-in-scarlet-woman,
	title: Hr: A Study In Scarlet Woman,
	author: Sherry Thomas,
	Jumlah Halaman: 432.0,
	Tanggal Terbit: 11 Nov 2019,
	ISBN: 9786230009112,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.15 kg,
	Lebar: 11.0 cm,
	Panjang: 18.0 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/little-love,
	title: Little Love,
	author: Honey Dee,
	Jumlah Halaman: 244,
	Tanggal Terbit: 26 Feb 2019,
	ISBN: 9786025373060,
	Bahasa: Indonesia,
	Penerbit: Andi Publisher,
	Berat: 0.2 kg,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/my-kampret-boyfriend,
	title: My Kampret Boyfriend,
	author: Ina Zakaria,
	Jumlah Halaman: 524,
	Tanggal Terbit: 9 Apr 2018,
	ISBN: 9786021097861,
	Bahasa: Indonesia,
	Penerbit: Buku Pintar Indonesia,
	Berat: 0.439 kg,
	Lebar: 20 cm,
	Panjang: 14 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/marrying-dear-teacher,
	title: Marrying Dear Teacher,
	author: Kevan Hanindra,
	Jumlah Halaman: 435,
	Tanggal Terbit: 4 Des 2018,
	ISBN: 9786021279762,
	Bahasa: Indonesia,
	Penerbit: Aksara Plus,
	Berat: 0.520 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/hr-the-dukes-quandary,
	title: HR: The Duke's Quandary,
	author: Callie Huitton,
	Jumlah Halaman: 356,
	Tanggal Terbit: 1 Apr 2019,
	ISBN: 9786020496436,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.180 kg,
	Lebar: 11 cm,
	Panjang: 18 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/berhenti-di-kamu,
	title: Berhenti di Kamu,
	author: Dewi Wulansari,
	Jumlah Halaman: 236.0,
	Tanggal Terbit: 23 Des 2019,
	ISBN: 9786232166813,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.16 kg,
	Lebar: 13.0 cm,
	Panjang: 19.0 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/falling-fast,
	title: Falling Fast,
	author: Chechyl Miliani,
	Jumlah Halaman: 322,
	Tanggal Terbit: 12 Jun 2019,
	ISBN: 9786025164644,
	Bahasa: Indonesia,
	Penerbit: Rdm Publishers,
	Berat: 0.290 kg,
	Lebar: 14 cm,
	Panjang: 20.5 cm,
},
{
	page: 35,
	kategori 1: non-fiksi,
	kategori 2: fiksi-sastra,
	kategori 3: fiksi-populer,
	kategori 4: romance-1,
	book_url: https://www.gramedia.com/products/merebah-riuh,
	title: Merebah Riuh,
	author: Sacessahci,
	Jumlah Halaman: 172,
	Tanggal Terbit: 29 Jan 2019,
	ISBN: 9786022203018,
	Bahasa: Indonesia,
	Penerbit: Bukune,
	Berat: 0.200 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
