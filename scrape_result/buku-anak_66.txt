{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/seri-aku-sudah-besar-kenapa-orang-dewasa-hanya-bermain-ponsel,
	title: Seri Aku sudah Besar-Kenapa Orang Dewasa Hanya Bermain Ponsel?,
	author: Ko Ja Hyun,
	Jumlah Halaman: 96,
	Tanggal Terbit: 14 Mei 2018,
	ISBN: 9786024288501,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.100 kg,
	Lebar: 18 cm,
	Panjang: 24 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/10-fakta-kisah-nyata-peninggalan-sejarah-kuno,
	title: 10 Fakta & Kisah Nyata : Peninggalan Sejarah Kuno,
	author: Anita Ganeri,
	Jumlah Halaman: 24,
	Tanggal Terbit: 9 Des 2014,
	ISBN: 9786022497172,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.15 kg,
	Lebar: 16.5 cm,
	Panjang: 20 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/kisah-favorit-anak-jepang,
	title: Kisah Favorit Anak Jepang,
	author: Dicky Stefanus,
	Jumlah Halaman: 116,
	Tanggal Terbit: 23 Apr 2018,
	ISBN: 9786020458540,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.250 kg,
	Lebar: 19 cm,
	Panjang: 23 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/arif-berprestasi-untuk-sekolah-dasar-kelas-1,
	title: Arif Berprestasi Untuk Sekolah Dasar Kelas 1,
	author: Christina Umi,
	Jumlah Halaman: 240,
	Tanggal Terbit: 9 Jul 2018,
	ISBN: 9786020504810,
	Bahasa: Indonesia,
	Penerbit: Gramedia Widiasarana Indonesia,
	Berat: 0.52 kg,
	Lebar: 20.5 cm,
	Panjang: 27.5 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/zona-sains-1-tubuh-manusia-fauna-penemuan-1,
	title: Zona Sains 1 : Tubuh Manusia, Fauna, Penemuan,
	author: David West,
	Jumlah Halaman: 92,
	Tanggal Terbit: 30 Apr 2014,
	ISBN: 9786022495826,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.2 kg,
	Lebar: 16 cm,
	Panjang: 20.5 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/titip-rindu-ke-tanah-suci,
	title: Titip Rindu Ke Tanah Suci,
	author: Aguk Irawan M.N.,
	Jumlah Halaman: 366,
	Tanggal Terbit: 4 Des 2017,
	ISBN: 9786020822877,
	Penerbit: Republika Penerbit,
	Berat: 0.31 kg,
	Lebar: 13.5 cm,
	Panjang: 20.5 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/c-sword-and-cornett-3,
	title: +C: Sword And Cornett 3,
	author: Yugyouji Tama,
	Jumlah Halaman: 200,
	Tanggal Terbit: 14 Jun 2017,
	ISBN: 9786020427607,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1300 kg,
	Lebar: 11.4 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/c-sword-and-cornett-4,
	title: +C: Sword And Cornett 4,
	author: Yugyouji Tama,
	Jumlah Halaman: 200,
	Tanggal Terbit: 2 Agt 2017,
	ISBN: 9786020440088,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1300 kg,
	Lebar: 11.4 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/small-puzzle-boboiboy-es-dan-blaze,
	title: Small Puzzle - Boboiboy Es Dan Blaze,
	author: Animonsta,
	Jumlah Halaman: 1,
	Tanggal Terbit: 27 Jun 2016,
	ISBN: 716121027,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 16 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/teenlit-teka-teki-terakhir-cover-baru,
	title: TeenLit: Teka-Teki terakhir - Cover Baru,
	author: Annisa Ihsani,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/kariage-kun-34-terbit-ulang,
	title: Kariage Kun 34 (Terbit Ulang),
	author: Masashi Ueda,
	Jumlah Halaman: 136,
	Tanggal Terbit: 5 Apr 2017,
	ISBN: 9786020403588,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1100 kg,
	Lebar: 11.4 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/matematika-seru,
	title: Matematika Seru,
	author: Agnes Dessyana,
	Jumlah Halaman: 96,
	Tanggal Terbit: 10 Jul 2017,
	ISBN: 9786023948208,
	Bahasa: Indonesia,
	Penerbit: Bhuana Ilmu Populer,
	Berat: 0.1000 kg,
	Lebar: 12 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/smart-cards-match-it,
	title: Smart Cards & Match It,
	author: Christine Lerin Adventary,
	Jumlah Halaman: 104,
	Tanggal Terbit: 7 Okt 2017,
	ISBN: 9786020421537,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 9 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/mini-coloring-books-mobil-retro-modern,
	title: Mini Coloring Books-Mobil Retro Modern,
	author: Yusup Somadinata,
	Jumlah Halaman: 16,
	Tanggal Terbit: 2 Okt 2017,
	ISBN: 9786020447032,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1000 kg,
	Lebar: 17 cm,
	Panjang: 17 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/big-hero-6-hiro-datang-menolong-sebuah-novel,
	title: Big Hero 6: Hiro Datang Menolong -Sebuah Novel,
	author: Disney,
	Jumlah Halaman: 88,
	Tanggal Terbit: 24 Okt 2014,
	ISBN: 9786020310053,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3000 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/science-adventure-motion-and-energy-vol-2,
	title: Science Adventure: Motion And Energy Vol 2,
	author: Go Yun Gon - Hyeon Jong Wo,
	Jumlah Halaman: None,
	Tanggal Terbit: 14 Sep 2015,
	ISBN: 9786020272368,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2500 kg,
	Lebar: 15 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/buku-aktivitas-kecerdasan-logika-cars,
	title: Buku Aktivitas Kecerdasan Logika Cars,
	author: Disney,
	Jumlah Halaman: 64,
	Tanggal Terbit: 19 Des 2016,
	ISBN: 9786020297965,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.3000 kg,
	Lebar: 21 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/girl-s-generation-alla-moda-girl-s,
	title: Girl`S Generation : Alla Moda Girl`S,
	author: Choi Bongsun,
	Jumlah Halaman: 200,
	Tanggal Terbit: 22 Agt 2016,
	ISBN: 9786020291871,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2500 kg,
	Lebar: 15 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/dragon-ball-vol-8,
	title: Dragon Ball Vol. 8,
	author: Akira Toriyama,
	Jumlah Halaman: 186,
	Tanggal Terbit: 18 Jan 2017,
	ISBN: 9786020299266,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1300 kg,
	Lebar: 11.4 cm,
},
{
	page: 66,
	kategori 1: buku-anak,
	book_url: https://www.gramedia.com/products/my-adventure-stories-comfy-car-2-bahasa,
	title: My Adventure Stories : Comfy Car - 2 Bahasa,
	author: Dini Capung Mungil,
},
