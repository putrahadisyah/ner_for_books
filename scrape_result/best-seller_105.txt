{
	page: 105,
	book_url: https://www.gramedia.com/products/haifa-cinta-dalam-diam-sn01015,
	title: Haifa Cinta Dalam Diam l Sn01015,
	author: Ariskakhurnia,
	Jumlah Halaman: 303,
	Tanggal Terbit: 21 Mar 2019,
	ISBN: 9786025884962,
	Bahasa: Indonesia,
	Penerbit: Aksara Plus,
	Berat: 0.260 kg,
	Lebar: 20 cm,
	Panjang: 14 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/kitab-lima-lingkaran,
	title: Kitab Lima Lingkaran,
	author: MIYAMOTO MUSASHI,
	Jumlah Halaman: 119,
	Tanggal Terbit: 28 Jan 2019,
	ISBN: 9786023520589,
	Bahasa: Indonesia,
	Penerbit: AZKA PRESSINDO,
	Berat: 0.250 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/sma-kl-1-2-3-new-edition-big-book-matematika,
	title: Sma Kl. 1,2,3 New Edition Big Book Matematika,
	author: Tim Bbm,
	Jumlah Halaman: 738,
	Tanggal Terbit: 4 Mei 2017,
	ISBN: 9786026992727,
	Bahasa: Indonesia,
	Penerbit: Cmedia,
	Berat: 0.64 kg,
	Lebar: 15 cm,
	Panjang: 22 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/sdmi-kl4-penjasorkes-kuri-2013,
	title: SD/MI Kelas 4 Penjasorkes Kurikulum 2013,
	author: Masrian,
	Jumlah Halaman: 150,
	Tanggal Terbit: 24 Agt 2016,
	ISBN: 9786022980940,
	Bahasa: Indonesia,
	Penerbit: Penerbit Erlangga,
	Berat: 0.14 kg,
	Lebar: 18 cm,
	Panjang: 25 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/4-bulan-di-amerika,
	title: 4 BULAN DI AMERIKA,
	author: Abdul Malik Karim Amrullah,
	Jumlah Halaman: 320,
	Tanggal Terbit: 1 Mei 2018,
	ISBN: 9786022505129,
	Bahasa: Indonesia,
	Penerbit: GEMA INSANI,
	Berat: 0.400 kg,
	Lebar: 13 cm,
	Panjang: 18 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/seni-hidup-bersahaja,
	title: Seni Hidup Bersahaja,
	author: Shunmyo Masuno,
	Jumlah Halaman: 224,
	Tanggal Terbit: 8 Jul 2019,
	ISBN: 9786020631950,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.176 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/senior,
	title: Senior,
	author: Eko Ivano Winata,
	Jumlah Halaman: 320,
	Tanggal Terbit: 2 Feb 2018,
	ISBN: 9786026716224,
	Bahasa: Indonesia,
	Penerbit: Mizan Publishing,
	Berat: 0.300 kg,
	Lebar: 14 cm,
	Panjang: 21 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/smpmts-kl-7-bright-an-english-course-grade-i-kur-2013,
	title: SMP/MTs Kelas 7 Bright An English Course Grade I Kurikulum 2013,
	author: Nur Zaida,
	Jumlah Halaman: 171,
	Tanggal Terbit: 26 Agt 2016,
	ISBN: 9786022987611,
	Bahasa: Indonesia,
	Penerbit: Penerbit Erlangga,
	Berat: 0.350 kg,
	Lebar: 17.5 cm,
	Panjang: 25 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/why-people-marie-curie,
	title: Why? People - Marie Curie,
	author: Hyeonjong Park,
	Jumlah Halaman: 184,
	Tanggal Terbit: 17 Jul 2017,
	ISBN: 9786020436272,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2500 kg,
	Lebar: 18 cm,
	Panjang: 24 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/find-your-why,
	title: Find Your Why,
	author: Simon Sinek,
	Jumlah Halaman: 324,
	Tanggal Terbit: 24 Jun 2019,
	ISBN: 9786020631370,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.210 kg,
	Lebar: 13.5 cm,
	Panjang: 20 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/conf-493-resep-ramuan-herbal-berkhasiat-untuk-cantik-alami-luar-dalam,
	title: 493 Resep Ramuan Herbal Berkhasiat untuk Cantik Alami Luar Dalam,
	author: Ami Wahyu & Gagas Ulung,
	Jumlah Halaman: 316,
	Tanggal Terbit: 7 Jul 2014,
	ISBN: 9786020306421,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.3000 kg,
	Lebar: 18 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/why-literature-sastra,
	title: Why? Literature - Sastra,
	author: Yearimdang,
	Jumlah Halaman: 184,
	Tanggal Terbit: 17 Jul 2017,
	ISBN: 9786020431833,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.2600 kg,
	Lebar: 18 cm,
	Panjang: 24 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/hukum-pajak-indonesia,
	title: Hukum Pajak Indonesia,
	author: Dr. Bustamar Ayza, S.H., M.M.,
	Jumlah Halaman: 284,
	Tanggal Terbit: 31 Okt 2017,
	ISBN: 9786024221898,
	Bahasa: Indonesia,
	Penerbit: Prenadamedia Group,
	Berat: 0.34 kg,
	Lebar: 13.5 cm,
	Panjang: 20.5 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/legend-of-an-emperor-viii-cang-tian-ba-huang-07,
	title: Legend of an Emperor VIII Cang Tian Ba Huang 07,
	author: Tony Wong,
	Jumlah Halaman: 184,
	Tanggal Terbit: 11 Jul 2018,
	ISBN: 9786024289553,
	Bahasa: Indonesia,
	Penerbit: m&c!,
	Berat: 0.25 kg,
	Lebar: 17 cm,
	Panjang: 24.5 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/deradikalisasi-kebijakan-strategi-dan-program-penanggulangan-terorisme,
	title: Deradikalisasi Kebijakan, Strategi, dan Program Penanggulangan Terorisme,
	author: Prof. Dr. Irfan Idris, Ma,
	Jumlah Halaman: 292,
	Tanggal Terbit: 21 Jan 2019,
	ISBN: 9786025713279,
	Bahasa: Indonesia,
	Penerbit: Cahaya Insani,
	Berat: 0.700 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/the-jadugar-15-tahun-mengobrak-abrik-video-musik-indonesia,
	title: The Jadugar: 15 Tahun Mengobrak-Abrik Video Musik Indonesia,
	author: Tim Penulis Jadugar,
	Jumlah Halaman: 280.0,
	Tanggal Terbit: 20 Jan 2020,
	ISBN: 9786020635279,
	Bahasa: Indonesia,
	Penerbit: Gramedia Pustaka Utama,
	Berat: 0.5 kg,
	Lebar: 15.0 cm,
	Panjang: 23.0 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/smkmak-klx-fisika-bid-keahlian-teknkom-k13-rev,
	title: SMK/MAK Kelas X Fisika Bidang Keahlian Teknologi & Komunikasi Kurikulum 2013 Revisi,
	author: Yuni Supriyati,
	Jumlah Halaman: 218,
	Tanggal Terbit: 28 Feb 2018,
	ISBN: 9786024443443,
	Bahasa: Indonesia,
	Penerbit: Bumi Aksara Group,
	Berat: 0.37 kg,
	Lebar: 18 cm,
	Panjang: 25 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/country-girl-02,
	title: Country Girl 02,
	author: Tatsuhiko Hikagi,
	Jumlah Halaman: 232.0,
	Tanggal Terbit: 2 Okt 2019,
	ISBN: 9786230006821,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.12 kg,
	Lebar: 11.4 cm,
	Panjang: 17.2 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/handa-kun-6,
	title: Handa Kun 6,
	author: Satsuki Yoshino,
	Jumlah Halaman: 170,
	Tanggal Terbit: 18 Okt 2017,
	ISBN: 9786020448176,
	Bahasa: Indonesia,
	Penerbit: Elex Media Komputindo,
	Berat: 0.1200 kg,
	Lebar: 11.4 cm,
	Panjang: 17.2 cm,
},
{
	page: 105,
	book_url: https://www.gramedia.com/products/1-day-10-words-new-edition-best-seller,
	title: 1 Day 10 Words (New Edition Best Seller),
	author: Lala Isna Hasni,
	Jumlah Halaman: 560,
	Tanggal Terbit: 14 Jun 2019,
	ISBN: 9786025320088,
	Bahasa: Indonesia,
	Penerbit: Andaliman Books,
	Berat: 0.500 kg,
	Lebar: 13 cm,
	Panjang: 19 cm,
},
