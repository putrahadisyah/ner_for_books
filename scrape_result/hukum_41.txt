{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/anti-dumping-dalam-wto-penentuan-harga-normal-produk-seje,
	title: Anti Dumpling Dalam WTO,
	author: Paul Erwin R. Simanjuntak, S.H., L.L.M.,
	Jumlah Halaman: None,
	Tanggal Terbit: 23 Jan 2019,
	ISBN: 9786026176684,
	Bahasa: Indonesia,
	Penerbit: Permata Aksara,
	Berat: 0.30 kg,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-pembuktian-dalam-beracara-pidanaperdatadan-korupsi-d,
	title: Hukum Pembuktian Dalam Beracara Pidana, Perdata, Dan Korupsi Di Indonesia,
	author: Alfitra, Sh, Mh,
	Jumlah Halaman: 189,
	Tanggal Terbit: 16 Nov 2017,
	ISBN: 9789790131620,
	Bahasa: Indonesia,
	Penerbit: Raih Asa Sukses,
	Berat: 0.205 kg,
	Lebar: 14 cm,
	Panjang: 20 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-acara-pidana-memahami-perlindungan-ham-dlm-proses-pen,
	title: Hukum Acara Pidana: Memahami Perlindungan HAM Dalam Proses Penahanan di Indonesia,
	author: Dr. Ruslan Renggong, S.H.,
	Jumlah Halaman: 246,
	Tanggal Terbit: 19 Jan 2015,
	ISBN: 9786021186237,
	Bahasa: Indonesia,
	Penerbit: Kencana,
	Berat: 0.24 kg,
	Lebar: 13 cm,
	Panjang: 20.5 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/pemberantasan-korupsi-pidana-nasionalintl,
	title: Pemberantasan Korupsi Melalui Hukum Pidana Nasional dan Internasional,
	author: Jur Andi,
	Jumlah Halaman: 237.0,
	Tanggal Terbit: 1 Jan 2006,
	ISBN: 9789797694272,
	Bahasa: Indonesia,
	Penerbit: Rajawali Pers,
	Berat: 0.178 kg,
	Lebar: 13.0 cm,
	Panjang: 21.0 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-perbankan,
	title: Hukum Perbankan,
	author: Uswatun Hasanah,
	Jumlah Halaman: 192,
	Tanggal Terbit: 1 Des 2015,
	ISBN: 9786026344229,
	Penerbit: Setara Press,
	Berat: 0.2300 kg,
	Lebar: 15 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/uu-ri-no-10-th-2016-ttg-perubahan-kedua-atas-uu-no-1-th-2015-ttg-pilkada,
	title: Uu Ri No.10 Th.2016 Ttg Perubahan Kedua Atas Uu No.1 Th.2015 Ttg Pilkada,
	author: Tim Citra Umbara,
	Jumlah Halaman: 396,
	Tanggal Terbit: 14 Sep 2016,
	ISBN: 9786029440980,
	Bahasa: Indonesia,
	Penerbit: Citra Umbara,
	Berat: 0.3000 kg,
	Lebar: 21 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-administrasi-pemerintahan-daerah,
	title: Hukum Administrasi Pemerintahan Daerah,
	author: Sirajuddin Dkk,
	Jumlah Halaman: 388,
	Tanggal Terbit: 13 Jul 2016,
	ISBN: 9786021642931,
	Bahasa: Indonesia,
	Penerbit: Intrans Publishing,
	Berat: 0.4500 kg,
	Lebar: 15 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/pelaksanaan-hukuman-mati-perspektif-ham-hukum-pidana-di-indonesia,
	title: Pelaksanaan Hukuman Mati: Perspektif Ham & Hukum Pidana Di Indonesia,
	author: Nelvitia Purba & Sri Sulistyawati,
	Jumlah Halaman: 202,
	Tanggal Terbit: 17 Sep 2015,
	ISBN: 9786022624769,
	Bahasa: Indonesia,
	Penerbit: Graha Ilmu,
	Berat: 0.5000 kg,
	Lebar: 21 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-tata-negara-indonesia-edisi-revisi,
	title: Hukum Tata Negara Indonesia Edisi Revisi,
	author: Hestu Cipto Handoyo,
	Jumlah Halaman: 437,
	Tanggal Terbit: 28 Apr 2015,
	ISBN: 9786027821316,
	Bahasa: Indonesia,
	Penerbit: Cahaya Atmapustaka,
	Berat: 0.45 kg,
	Lebar: 15 cm,
	Panjang: 22.5 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-otonomi-daerah,
	title: Hukum Otonomi Daerah,
	author: Rusdianto Sesung,
	Jumlah Halaman: 198,
	Tanggal Terbit: 2 Jul 2014,
	ISBN: 9786027948204,
	Bahasa: Indonesia,
	Penerbit: Refika Aditama,
	Berat: 0.307 kg,
	Lebar: 16 cm,
	Panjang: 24 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/aspek-hukum-informed-consent-rekam-medis-dlm-transaksi-terapeutik,
	title: Aspek Hukum Informed Consent&Rekam Medis Dlm Transaksi Terapeutik,
	author: Desriza Ratman,
	Tanggal Terbit: 17 Jan 2014,
	Penerbit: Keni,
	Berat: 0.3000 kg,
	Lebar: 0 cm,
	Panjang: 0 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/peradilan-agama-di-indonesia-dinamika-pembentukan-hukum,
	title: Peradilan Agama Di Indonesia: Dinamika Pembentukan Hukum,
	author: Aden Rosadi,
	Jumlah Halaman: 324,
	Tanggal Terbit: 12 Agt 2015,
	ISBN: 9786027973213,
	Bahasa: Indonesia,
	Penerbit: Simbiosa Rekatama Media,
	Berat: 0.4000 kg,
	Lebar: 16 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-investasi-internasional-1,
	title: Hukum Investasi Internasional,
	author: Kusnowibowo,
	Jumlah Halaman: 210.0,
	Tanggal Terbit: 1 Sep 2019,
	ISBN: 9786021311448,
	Bahasa: Indonesia,
	Penerbit: Pustaka Reka Cipta,
	Berat: 0.197 kg,
	Lebar: 15.5 cm,
	Panjang: 23.5 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/filsafat-hukum-pidana-konsep-dimensiaplikasi,
	title: Filsafat Hukum Pidana: Konsep, Dimensi & Aplikasi,
	author: Siswanto Sunarso,
	Jumlah Halaman: 301,
	Tanggal Terbit: 2 Jun 2015,
	ISBN: 9789797697921,
	Bahasa: Indonesia,
	Penerbit: Rajagrafindo Persada,
	Berat: 0.2 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/teori-dan-praktik-hukum-acara-pidana-khusus,
	title: Teori Dan Praktik Hukum Acara Pidana Khusus,
	author: Harrys Ratama Teguh,
	Jumlah Halaman: 466,
	Tanggal Terbit: 24 Jan 2018,
	ISBN: 207967114,
	Bahasa: Indonesia,
	Penerbit: Pustaka Setia,
	Berat: 0.75 kg,
	Lebar: 16 cm,
	Panjang: 24 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/korupsi-dan-pembuktian-terbalik,
	title: Korupsi Dan Pembuktian Terbalik,
	author: Dr. Drs. Mansur Kartayasa, S.H., M.H.,
	Jumlah Halaman: 414,
	Tanggal Terbit: 12 Okt 2017,
	ISBN: 9786024221430,
	Bahasa: Indonesia,
	Penerbit: Prenadamedia Group,
	Berat: 0.41 kg,
	Lebar: 15 cm,
	Panjang: 23 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-konstitusi-pandangan-gagasan-modernisasi-negara-huku,
	title: Hukum Konstitusi Pandangan & Gagasan Modernisasi Negara Huku,
	author: Muhammad Junaidi,
	Jumlah Halaman: 242.0,
	Tanggal Terbit: 1 Jan 2018,
	ISBN: 9786024252762,
	Bahasa: Indonesia,
	Penerbit: Rajagrafindo Persada,
	Berat: 0.234 kg,
	Lebar: 15.5 cm,
	Panjang: 23.0 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/hukum-perseroan-terbatas-hc,
	title: Hukum Perseroan Terbatas,
	author: -,
	Jumlah Halaman: 600,
	Tanggal Terbit: 1 Jan 2009,
	ISBN: 9789790072671,
	Bahasa: Indonesia,
	Penerbit: Sinar Grafika,
	Berat: 0.95 kg,
	Lebar: 16 cm,
	Panjang: 23 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/legalisasi-aborsi,
	title: Legalisasi Aborsi,
	author: Aroma Elmina Martha, Dkk,
	Jumlah Halaman: 152.0,
	Tanggal Terbit: 26 Nov 2019,
	ISBN: 9786026215673,
	Bahasa: Indonesia,
	Penerbit: Uii Press,
	Berat: 0.215 kg,
	Lebar: 16.0 cm,
	Panjang: 23.0 cm,
},
{
	page: 41,
	kategori 1: non-fiksi,
	kategori 2: hukum,
	book_url: https://www.gramedia.com/products/praktik-hukum-acara-perdata-edisi-2,
	title: Praktik Hukum Acara Perdata Edisi 2,
	author: Elfrida R Gultom,
	Jumlah Halaman: 254,
	Tanggal Terbit: 9 Agt 2017,
	ISBN: 9786023182657,
	Bahasa: Indonesia,
	Penerbit: Mitra Wacana Media,
	Berat: 0.388 kg,
	Lebar: 17 cm,
	Panjang: 24 cm,
},
